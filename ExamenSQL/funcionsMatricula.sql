--() { :; }; exec psql matricula -f "$0"
--Carlos Esteban
--iaw47999217

/*Calcula la capacidad máxima de cada grupo*/
DROP function IF EXISTS calculaCapacidad(p_codgrupo int);
CREATE OR REPLACE function calculaCapacidad(p_codgrupo int)
RETURNS int
AS $$
    DECLARE
        v_capacidadGrupo int;
    BEGIN
        SELECT capacidad
        INTO STRICT v_capacidadGrupo
        FROM grupo
        WHERE codgrupo = p_codgrupo;

        RETURN v_capacidadGrupo;
    END;
$$ language plpgsql;

/*Retorna true en el caso que dado una asignatura y un grupo en el que se quiera matricular, se
encuentre el grupo completo dado un curso académico.*/
DROP function IF EXISTS grupoCompleto(p_codasig varchar, p_codgrupo int);
CREATE OR REPLACE function grupoCompleto(p_codasig varchar, p_codgrupo int)
RETURNS boolean
AS $$
    DECLARE
        v_asigCompleta boolean;
        v_capacidadGrupo int;
    BEGIN
        v_asigCompleta := false;
        v_capacidadGrupo := calculaCapacidad(p_codgrupo);

        SELECT true
        INTO STRICT v_asigCompleta
        FROM expediente e join grupo g
                ON e.codgrupo = g.codgrupo
            JOIN modulo m
                ON m.idmodulo = e.idmodulo
        WHERE g.capacidad >= v_capacidadGrupo
        LIMIT 1;
        RETURN v_asigCompleta;

        IF v_asigCompleta
        THEN
            RAISE NOTICE 'Grupo completo';
            RETURN true;
        ELSE
            RAISE NOTICE 'Quedan plazas en el grupo';
            RETURN false;
        END IF;

    END;
$$ language plpgsql;

/*Retorna si un DNI está matriculat a una assignatura*/
DROP FUNCTION IF EXISTS comprobaMatricula(p_codasig varchar, p_dni varchar);
CREATE OR REPLACE function comprobaMatricula(p_codasig varchar, p_dni varchar)
RETURNS boolean
AS $$
    DECLARE
        v_alumneMatriculat boolean;
    BEGIN
    v_alumneMatriculat := false;
    SELECT true
    INTO STRICT v_alumneMatriculat
    FROM expediente e join alumno a
            ON e.dni = a.dni
        join modulo m
            ON m.idmodulo = e.idmodulo
    WHERE e.dni = upper(p_dni) AND e.idmodulo = upper(p_codasig);
    RETURN true;

    EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        RETURN false;
    END;
$$ language plpgsql;

/*Retorna verdadero en el caso de que el alumno con dni p_dni tenga aprobada la
asignatura p_codasig*/
DROP FUNCTION IF EXISTS estaAprobada(p_codasig varchar, p_dni varchar);
CREATE OR REPLACE function estaAprobada(p_codasig varchar, p_dni varchar)
RETURNS boolean
AS $$
    DECLARE
        v_alumneMatriculat boolean;
        v_asigAprobada boolean;
    BEGIN
    v_asigAprobada := true;
    v_alumneMatriculat := comprobaMatricula(p_codasig, p_dni);

    IF v_alumneMatriculat
    THEN
        SELECT false
        INTO v_asigAprobada
        FROM expediente e join alumno a
                ON e.dni = a.dni
            join modulo m
                ON m.idmodulo = e.idmodulo
        WHERE e.dni = upper(p_dni) AND e.idmodulo = upper(p_codasig) AND nota < 5.00 OR nota is null;
        RETURN v_asigAprobada;

        IF found
        THEN
            RAISE EXCEPTION 'Asignatura suspendida o sin nota';
            RETURN false;
        END IF;
    ELSE
        RAISE NOTICE 'DNI no está matriculado en asignatura %', p_codasig;
        RETURN false;
    END IF;
    END;
$$ language plpgsql;

/*Retorna verdadero en el caso que un alumno que se quiera matricular de una asignatura y o no
tiene las asignaturas prerrequisito aprobadas (en el caso de estar matriculado) o directamente ni
tan siquiera está matriculado de ellas. Un ejemplo de prerrequisitos lo encontramos en DAW:
para poder matricularse de M02-UF4, es necesario haber estado matriculado de M02-UF2 y de
M02-UF3, y además tenerlas aprobadas. En la tabla Prerrequisitos habrian pues, dos filas con
M02-UF4: una para M02-UF2 y otra para M02-UF3.*/
DROP function IF EXISTS asignaturasPendientes(p_codasig varchar, p_dni varchar);
CREATE function asignaturasPendientes(p_codasig varchar, p_dni varchar)
RETURNS boolean
AS $$
    DECLARE
        v_asigPendientes boolean;
        v_prerrequisito varchar;
    BEGIN
        v_asigPendientes := true;

        SELECT idprerre
        INTO STRICT v_prerrequisito
        FROM prerrequisito
        WHERE idmodulo = p_codasig;

        CASE
            WHEN p_codasig = 'M05'
            THEN
                v_asigPendientes := estaAprobada(v_prerrequisito , p_dni);
                IF  v_asigPendientes
                THEN
                    RAISE NOTICE 'No se puede matricular, necesita aprobar %',v_prerrequisito ;
                    RETURN true;
                ELSE
                    RAISE NOTICE 'Prerrequisitos aprobados, puede hacer la matrícula de %', p_codasig;
                    RETURN false;
                END IF;
            WHEN p_codasig = 'M08'
            THEN
                v_asigPendientes := estaAprobada(v_prerrequisito , p_dni);
                IF  v_asigPendientes
                THEN
                    RAISE NOTICE 'No se puede matricular, necesita aprobar% para hacer la matrícula en %', v_prerrequisito,p_codasig;
                    RETURN true;
                ELSE
                    RAISE NOTICE 'Prerrequisitos aprobados, puede hacer la matrícula de %', p_codasig;
                    RETURN false;
                END IF;
        END CASE;
    END;
$$ language plpgsql;

/*Se da de alta en el expediente la matricula de un alumno, de una asignatura, en un
grupo en un curso académico si se dan las siguientes condiciones: el grupo que elige no está completo y si no
tiene prerrequisitos. El curso académico es el 2012. El resto de campos quedan vacíos.*/
DROP function IF EXISTS matriculaAsignatura(p_dni varchar, p_codasig varchar, p_codgrupo smallint, p_cursoacad varchar);
CREATE function matriculaAsignatura(p_dni varchar, p_codasig varchar, p_codgrupo smallint, p_cursoacad varchar)
RETURNS varchar
AS $$
    DECLARE
        v_asigPendientes boolean;
        v_grupoCompleto boolean;
        v_precioAssig int;
    BEGIN
        v_asigPendientes := asignaturasPendientes(p_codasig,p_dni);
        v_grupoCompleto := grupoCompleto(p_codasig, p_codgrupo );

        SELECT precio
        INTO STRICT v_precioAssig
        FROM modulo
        WHERE idmodulo = p_codasig;

        IF v_asigPendientes AND v_grupoCompleto
        THEN
            RAISE EXCEPTION 'No se puede hacer la matrícula, el grupo está completo o al alumno % le faltan asignaturas pendientes', p_dni;
        ELSE
            INSERT INTO matricula
            VALUES (nextval('seq_nummatri'), p_dni,p_cursoacad,v_precioAssig);
        END IF;
    END;
$$ language plpgsql;
