--iaw47999217--
--Carlos Esteban Casado--
--07/02/2019--

--1. Insereix (si s'escau) el/s lloguer/s necessari/s per validar les següents preguntes DML
INSERT INTO lloguer
VALUES	(1,3,TO_DATE('01-01-19','DD-MM-YY'),null,1.50);

INSERT INTO lloguer
VALUES	(2,21,TO_DATE('10-01-19','DD-MM-YY'),TO_DATE('07-02-19','DD-MM-YY'),2.50);

INSERT INTO lloguer (coddvd,codsoci,datapres)
VALUES (4, 3, current_timestamp::timestamp - interval '1 week');
--2. Esborra els lloguers de fa més d'una setmana
DELETE FROM lloguer
WHERE 	datapres < current_timestamp::timestamp - interval '7 days';

--3. Actualitza la reserva de la peli 'Juana la Loca' del soci Manel Capdevila, el seu import
--amb el seu import amb el seu preu corresponent.
UPDATE 	lloguer
SET		import = (	SELECT 	preu
					FROM	pelicula
					WHERE	lower(titol)='juana la loca')
WHERE	codsoci = (SELECT l.codsoci
					FROM 	lloguer l
						JOIN	soci s
							ON	l.codsoci = s.codsoci
						JOIN	dvd d
							ON l.coddvd = d.coddvd
						JOIN	pelicula p
							ON	d.codpeli = p.codpeli
					WHERE lower(s.nom)='manel'
						AND lower(s.cognoms)='capdevila'
						AND	lower(p.titol)='juana la loca');
-------
UPDATE lloguer l
SET import = (SELECT preu
					FROM pelicula p
					WHERE lower(titol) = 'juana la loca')
WHERE codsoci = (SELECT s.codsoci
					FROM soci s
					WHERE lower(nom) = 'manel' AND lower(cognoms) = 'capdevila')
			AND
			coddvd in (SELECT coddvd
					FROM dvd d JOIN pelicula p
						ON d.codpeli = p.codpeli
					WHERE lower(titol) = 'juana la loca');

--4. Mostra per cada peli el seu codi, títol, el número de vegades que s'ha prestada i els
--guanys derivats del seu prèstec (utilitzeu el camp preu, no import).
--El 3r i el 4t camp es dirán prestecs i Guanys respectivament. Ordena la llista de pelis
--de més vegades prestades a menys, i si hi ha coincidència per ordre alfabètic.
SELECT p.codpeli, titol, count(l.coddvd)"Prestecs", count(l.coddvd)*preu "Guanys"
FROM	pelicula p LEFT JOIN dvd d
				ON p.codpeli = d.codpeli
			LEFT JOIN lloguer l
				ON d.coddvd = l.coddvd
GROUP BY p.codpeli, titol
ORDER BY 3 DESC;

--5. Mostrar per a cada gènere, quina és la peli més cara. Mostreu el nom del gènere, el títol
--de la película i el preu. Ordeneu per gènere i títol.
SELECT 	g.genere,
				coalesce(p.titol,'**Sense peli associada**')"Títol",
				coalesce(p.preu,0) "Preu"
FROM genere g LEFT JOIN pelicula p
	ON g.codgen = p.codgen
WHERE (coalesce(preu,0),g.codgen) IN (SELECT max(preu),codgen
													FROM pelicula
													GROUP BY codgen)
ORDER BY g.genere, p.titol;


--6.Mostra els gèneres dels quals mai s'ha prestat una peli. Ordena'ls alfabèticament
SELECT 	genere
FROM 		genere
WHERE 	codgen NOT IN (SELECT p.codgen
                        FROM lloguer l JOIN dvd d
                        	ON l.coddvd = d.coddvd
                        JOIN pelicula p
                        	ON d.codpeli = p.codpeli)
ORDER BY 1;

--7.Mostra el títol de la película (o pel·lícules) que té mes gent esperant per ella.
--Ordena alfabèticament.
SELECT	l.codpeli, titol
FROM llistaespera l JOIN pelicula p
	ON l.codpeli = p.codpeli
GROUP BY l.codpeli, titol
HAVING count(*) =	(	SELECT max("numSocis")
										FROM	(SELECT	codpeli, count(codpeli) "numSocis"
												FROM		llistaespera
												GROUP BY codpeli) "taula");
