-- Nom: Jacint 
-- Cognoms: Iglesias Casanova
-- Codi: iaw14270791
-- Nom projecte: iglesiasjacint
-- Data: 13-02-2020
\c videoclub

-- 2. 
\echo 'Ex 2.'

select 	p.codpeli,
		titol,
		count(l.*) "Prestec",
		count(l.*) * preu "Guanys"
from pelicula p left join dvd d
	on d.codpeli = p.codpeli
left join lloguer l
	on l.coddvd = d.coddvd
group by p.codpeli,titol
order by 3 desc,2;



-- 3.
\echo 'Ex 3.'
select	ge.genere,
		coalesce(pelicula, '---') pelicula,
		coalesce((select max(preu)
		from pelicula
		where codgen = ge.codgen
		group by codgen)::text,'---') "preu"
from genere ge left join
	(select codgen,titol pelicula
	from pelicula pe
	where preu = ( 	select max(preu)
					from pelicula
					where codgen = pe.codgen))
	 peli on peli.codgen = ge.codgen;
	 
-- 4.
\echo 'Ex 4.'

select genere
from genere
intersect
select genere
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on p.codpeli = d.codpeli
except
select genere 
from genere ge join pelicula p
	on p.codgen = ge.codgen
join dvd d
	on d.codpeli = p.codpeli
join lloguer l
	on l.coddvd = d.coddvd;

-- 5.

select	nom,
		cognoms
from soci s join lloguer l
	on l.codsoci = s.codsoci
join dvd d
	on l.coddvd = d.coddvd
join pelicula p 
	on d.codpeli = p.codpeli
group by nom,cognoms
having count(distinct p.codpeli) > 2;


\c lloguercotxes

-- 1.
\echo 'Ex 1.'

create or replace view vehicle as
select matricula, marca, model, 'moto' tipus,
	case 
		when lower(tipus) = 'elèctric de bateria' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid endollable'
				then '0 emissions'
		when lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas  gas natural' or
			lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas gas liquat' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid no endollable' 
			then 'Eco'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2006' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2015' 
		then 'C'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2001' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2006' 
		then 'B'
		else
			'sense etiqueta'
		end etiqueta
from moto
union
select matricula, marca, model, 'cotxe' tipus,
	case 
		when lower(tipus) = 'elèctric de bateria' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid endollable'
				then '0 emissions'
		when lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas  gas natural' or
			lower(tipus)|| ' '|| lower(subtipus) = 'propulsió gas gas liquat' or
			lower(tipus)|| ' '|| lower(subtipus) = 'híbrid no endollable' 
			then 'Eco'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2006' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2015' 
		then 'C'
		when lower(subtipus) = 'benzina' and to_char(datamatriculacio,'yyyy') >= '2001' or
			lower(subtipus) = 'diesel' and to_char(datamatriculacio,'yyyy') >='2006' 
		then 'B'
		else
			'sense etiqueta'
		end etiqueta
from cotxe;




























