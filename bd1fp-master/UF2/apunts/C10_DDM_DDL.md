# psql con consulta SQL  
```
psql -d scott -c "select * from emp"
```
## batch/offline/desates  
Cuan llançem un process que no requireix la pressencia de l'usuari, sino que el pot deixar programat.
```
psql -d scott -c "select * from emp"
```
### online  
Process que per defecte requereix l'interacció de l'usuari per ser portat a carrec.
```
rm -rf
```
  
  
# ON UPDATE/DELETE  
Cuan borrem o actualitzem una clau primaria referenciada, les restriccions afectaran a les claus alienes que li fan referencia.  
   PK              FK  
    1        =>     N  
modificació      efecte  
  
# Operacións basiques  
* Per "borrar" el contingut  d'un camp o camps, he de fer un update  
```
update municipi
set idmunicipi = 7
where idmunicipi= 1
;
```
* Per eliminar un camp d'una taula he de fer un alter table  
```
ALTER TABLE Customers
DROP COLUMN ContactName
;
```
## Operació que no es pot exectuar.  
```
update emp
set deptno = 187
where deptno = 10
;
```
No es pot executar, per que el departament 187 no existeix, i per tant no se l'hi pot fer referencia. **Error d'integritat referencial**.  
  
  
# Practicar `DROP TABLE` en l'ordre que toca.  Practicar tambe `ALTER TABLE`, fer `DROP/ADD CONSTRAINT/COLUMN`.  
  
# TEMA NOU. SENTENCIA BÁSICA SQL.  
Las partes que lo forman (lineas) se llaman **clausulas**.  
Las clausulas **obligatorias** del **select** son `select` y `from`.  
  
## Ejemplos de clausulas (oficiales/standard SQL).  
* select        camps, expressió aritmètica
* from          taula/es
* where         condicio/ns
* group by
* having 
* order by
* limit
  
## Escritura de sentencias SQL.  
Oracle estableció que era **conveniente** poner las **palabras reservadas** en mayusculas.  
  
capçalera       camps       grau                    <=  
cos             files       cardinalitat            <= conjunt  
## Alies de camp i de taula.  
Alies de camp per informar/clarificar, fer mes clars els resultats. Els alies de taula per comoditat.
### Alies de camp  
* Funciona amb un sol bloc de texts sense espais
```
select 
    ename, 
    sal, 
    sal +300 salari_augmentat
from emp;
```
* No funciona amb espai i sense cometes.
```
select 
    ename, 
    sal, 
    sal +300 salari augmentat
from emp;
```
* Funciona y tolera espais amb cometes.
```
select 
    ename, 
    sal, 
    sal +300 "salari augmentat"
from emp;
```

## NULLS
Un `NULL` o es transforma o es descarta, per que si s'opera amb ell, el resultat sera NULL.  
**NULL = NULL => false**  
## Operador de concatenació `||`.  
```
select
        ename || ' treballa de ' || job "Empleados" /* las comillas dobles hacen que se vea la primera en mayusculas */
from emp
;
```
# PROVAR `DISTINCT` EN CASA  







