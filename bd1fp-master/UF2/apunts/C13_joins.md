# FK de reflexivas, crealrlas despues de la tabla con alter table.

# Joins.  
  
## Definicion de joins:  
**FK** de una tabla igualada a la **PK** de la otra tabla.  
  
## Tipos de joins:
* inner join
* outer join
    * left join
    * right join
* full outer join (extraño hacer un full join, caso excepcional)
* `*`producte cartesia amb restricció (caso especial)
* self join (en tablas con relaciones reflexivas)(caso especial)
  
**El join mas simple es el inner join, y el mas comun.**

## Ejemplos de join
```
-- forma normal (solo se puede hacer un tipo de joins con esta forma)

select table1.column, table2.column2
from table1, table2
where table1.FK = table2.PK
;

-- froma universal (recomendada)
select table1.column, table2.column2
from table1 join table2
    on table1.FK = table2.PK
;
```
  
A join B == B join A ==> propiedad conmutativa  
En el **from** por costumbre, declarar primero la tabla que contiene la **FK**, trazando un camino imaginario de **FK** a **PK**.  
**FK** ==> **PK**  

## En un join, en el select, el campo de FK y PK son intercambiables, ya que estan igualados `e.deptno = d.deptno`.

**Un error tipico de joins es el error de AMBIGUEDAD, se refiere a un campo presente en varias tablas, por lo cual no puede decidir cual es la tabla referenciada.**  

## Un join puede ser multiple, el primer join, se considera una tabla temporal que hace un join a la siguiente, y asi succesivamente.  

## JOIN CON CLAVES COMPUESTAS.  
Se igualan las 4 claves en pares de 2, **FK** con **PK**.

```
on  t1.a=t2.a AND t1.b=t2.b
```
## JOIN CON OTRA TABLA EXTRA (3).  
```
from    t1 join t2 on t1.ca = t2.cp
        join t3 on condicio join
        join t4 on condicio join
```

### ENUNCIADO: Muestra el nombre de los empleados y de sus departamentos (scott).  

```
-- no hay error de ambiguidad por que los campos son unicos, solo necesario en FK y PK
select 
    ename,
    dname,
    loc
from emp e join dept d
        on e.deptno = d.deptno
;

```
### Lo mismo pero con asterisco. 
```
select 
    *
from emp e join dept d
        on e.deptno = d.deptno
;
```
### Tots els camps de emp mas el nombre de deparrtabmento.
```
select e.*, dname
from emp e join dept d
    on e.deptno = d.deptno
;
```
### ENUNCIADO: Mostrar los empleados y el nombre de su jefe (outer left join).

```
select 
    e.ename "Empleado", 
    j.ename "Jefe"
from emp e left join emp j
    on e.mgr = j.empno
;
```



# A

## B

```
code
    code
```
`a`



* a
* b
* c
    * d
    * e
    * f