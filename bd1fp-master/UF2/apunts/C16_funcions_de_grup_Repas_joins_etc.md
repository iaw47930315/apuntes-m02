# Funcions de grup (aggregate functions/ funcions agregades).  
  
  
###  Definició:  
Son unes funcions que s'agrupen sobre un conjunt de files. (sum, avg, max, etc).  
  
### Quines son:  
  
1. COUNT    -Compta els elements.
2. MAX
3. MIN
4. SUM
5. AVG
  
  
### Example:  
```
select round(avg(sal),2), max(sal), min(sal), sum(sal), count(*)
from emp
;
-----------------------------------------------
  round  |   max   |  min   |   sum    | count 
---------+---------+--------+----------+-------
 2073.21 | 5000.00 | 800.00 | 29025.00 |    14
(1 row)

```
**Nota:** round redondea los decimales al numero seleccionado.  
  
```
select 
        round(avg(sal),2) "mitjana de salaris", 
        max(sal) "salari maxim", 
        min(sal) "salari minim",
        sum(sal) "total expensa en salaris", 
        count(*) "numero de treballadors de salesman"
from emp
where lower(job) = 'salesman'
;
```
###  
#### Exercici: Numero d'empleats que tenen commisió.
```
scott=> select count(comm) "numero d'empleats amb comissió"  
from emp;
 numero d'empleats amb comissió 
--------------------------------
                              4
(1 row)
-------select count(*) "numero d'empleats sense comissió"
from emp
where comm IS NULL
;
----------------------------

```
##### AVG operando con nulls:
```
select avg(coalesce(comm,0))
from emp;
----
select avg(comm)
from emp;
------------

```
**Nota:** 
## MIN/MAX  
Cuan s'apliquen dates a `MAX` i `MIN`, **MAX es la data mes recent** i **MIN es la data mes antiga**.  
  
## COUNT  
```
scott=> select count(mgr)
scott-> from emp;
 count 
-------
    13
(1 row)

scott=> select count(*)
from emp;
 count 
-------
    14
(1 row)

```
**Nota:** `COUNT` no compta les files que tenen el valor del camp com a `NULL` com a fila a l'hora de fer el compte total de files.  
  
#### COUNT(mgr) = COUNT if mgr IS NOT NULL;  

###  




##  
###  