# Subconsultas.  
  
  
in => = any  
>all => > max(field)  
>any => > min(field)  
  
#### Las consultas han de caber en un display de 80 columnas.  
### Where con fecha.  
```
select *
from cliente
where cliecod in (	select cliecod
					from pedido
					where lower(fabcod) = 'aci'
					and to_char(fecha, 'yyyy-mm') between'2003-01' and '2003-6');
```
**Nota:** Cuando se haga una equivalencia de fecha, poner primer el año antes del mes.  
```
mes     any  
7       2003 => este es considerado mas grande por el 7 de delante  
1       2007  
```
* subconsultes
    * monoregistre
    * multiregistre
    * multicolumna
  
## Errores tipicos.  
Cuando hay que hacer servir **NOT IN** <ins>no se puede sustituir</ins> por un **JOIN**.  
  
#### Cuando se pueda elegir hacer un JOIN en vez de una SUBCONSULTA.  
  
##### Joc de proves per a subconsulta amb mala leche.  
```
-- 9. Obtener una lista de las oficinas en donde haya algún representante cuya
-- cuota sea más del 55% del objetivo de la oficina.
                                                                        
select *
from oficina ofi
where ofinum in (       select r.ofinum
                        from oficina o join repventa r
                                on r.ofinum = o.ofinum
                        where r.cuota / o.objetivo > 0.55
                        and ofi.ofinum = r.ofinum);
```
        ofinum      cuota       objetivo  
pep     1           55          100  
maria   2           55          100  
carlos  1           55          100  
---

results oficina 1
