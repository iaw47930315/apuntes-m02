# Date  
  
```
to_char(cam,'patro')

```
## Lo que retorna to_char es text (tener en cuenta al introducir dias, por ejemplo, 01).  


* patrons
    * dd(numeric)        dy(avreujat)      day(text)
    * mm(numeric)        mon(avreujat)     month(text)
    * yy(dos xifres)     yyyy(quatre xifres)
    * hh24:mi:ss
    * hh12



* date
    * dia mes any
* timestamp
    * dia me ansy hores minuts segons
    * nom format: (epoch/unix time)

## Exemples:  
```
select to_char(current_date,'mm');
select to_char(current_date,'dy');
select to_char(current_timestamp,'hh24:mi:ss');
select to_char(current_timestamp,'dd/month/yyyy-hh24:mi:ss');
select to_char(current_timestamp,'dd/dy/day/mm/mon/month/yy/yyyy-hh24:mi:ss');
select to_char(current_timestamp,'dd/dy/day/mm/mon/month/yy/yyyy-hh12:mi:ss');

```

### Empleats contractat a partir de l'any 81 (scott).  
```
select *
from emp
where to_char(hiredate,'yy') >= '81'
--      ^expressió(de camp)  operador  
;
```
  
### Mostrar els empleats que van ser contractats en febrer, juny y octubre de cualsevol any (scott).  
```
select *
from emp
where to_char(hiredate,'mm') IN 
                ('02','05','10')
order by to_char(hiredate,'mm')
;

select *
from emp
where to_char(hiredate,'mm/yy') IN 
                ('02/81','05/81','10/81')
order by to_char(hiredate,'dd/mm/yy')
;

select *
from emp
where to_char(hiredate,'mm-yy') IN 
                ('02-81','05-81','10-81')
order by to_char(hiredate,'dd-mm-yy')
;

```


### Mostrar els empleats de mes recent a mes antic per data de contractació. De mes actual a mes antic.  

```
select *
from emp
order by hiredate desc
;
```
  
  
  
# Order by.  
## Ascendent es la ordre per defecte.  

### ejemplo order by lista de clase:
```
order by ap1,ap2,nom
; 
```
### ejemplo order by descencente por un campo y ascendente por otro campo.

```
select *
from emp
order by sal desc, ename asc  
;
```

### Versión pro:
  
```
select ename, sal
from emp
order by 2 desc, 1
;
```

# A

## B

```
code
    code
```
`a`



* a
* b
* c
    * d
    * e
    * f