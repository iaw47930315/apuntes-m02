# Operadors de càlcul  
  
## exists  
  
### Busca una fila relacionada.  
  
Si vui buscare el nom del **client** que ha **fet una comanda**:  
```
select nom
from cliente cl
where EXISTS (  select *
                from pedido p
                where p.cliecod = cl.cliecod);
```
  

## not exists  
  
## Exercici: clients que no han fet mai una comanda AMB EXISTS.  
  
```
select nombre
from cliente cl
where NOT EXISTS (  select *
                    from pedido p
                    where p.cliecod = cl.cliecod);
```
  
### NOTA: exists/ not exists mira la condición del where del select.  
  
### Exercici: clients que nomes demanen un sol producte.  
  
```
select nombre, pe.fabcod||pe.prodcod "producto"
from cliente cl join pedido pe
    on pe.cliecod = cl.cliecod
where not exists (  select *
                    from pedido p
                    where cl.cliecod = p.cliecod
                    and pe.prodcod||pe.fabcod != p.prodcod||p.fabcod)
group by 1,2
having count(pe.cliecod) > 1;

------------
-- condicion de insert
insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
values  (2,current_date,2117, 105,'rei','2a44l',1,10.32),
        (3,current_date,2101, 105,'rei','2a44l',1,10.32);


```

# VISTA: una finestra a la base de dades.  
  
## Vista simple: la que no tingui joins, i no tingui cap camp calculat.  
## Vista complexa: te camps calculats i/o joins.  

### Exemple de vistes:  
  
#### Creeu la vista que es dira emp30. Que ha de mostrar el codi de l'empleat, el nom i el salari, dels empleats del departament 30.  
  
```
create view emp30 as
select  empno, 
        ename, 
        sal
from emp
where deptno = 30;
```
  
#### El mateix pero amb codi nom i salari.  
```
----
drop view if exists emp30; -- es necesario si quieres cambiar el nombre del campo
----
create or replace view emp30 as -- create or replace view es util para laterar codigo de la tabla
select  empno "codi", 
        ename "nom", 
        sal "salari"
from emp
where deptno = 30
with check option; -- añadido despues, puedes quitarlo dependiendo
```
  
## Operaciones DML sobre VISTA. DEPENDE, si es tipus simple or complex. Vista simple si i no sempre.  
#### Despiden a ALLEN.  

```
delete from emp30
where lower(nom) = 'allen';
``` 
## DML in View:  
* simple    -Si, y no sempre (referirse al campe per l'alias)
* complexa  

```
insert into emp30 (codi,nom,salari)
values (5,'WOOBIE',3500);

--

update emp30
set codi = 2


``` 

## VISTAS WITH "CHECK OPTION"  
```
create or replace view emp30 as -- create or replace view es util para laterar codigo de la tabla
select  empno "codi", 
        ename "nom", 
        sal "salari"
from emp
where deptno = 30
with check option; -- añadido despues, puedes quitarlo dependiendo
---
ERROR:  new row violates check option for view "emp30"
DETAIL:  Failing row contains (5, WOOBIE, null, null, null, 3500.00, null, null).

```
### Check option mira que el insert sea visible, le falta el deptno = 30.  
```
create or replace view emp30 as -- create or replace view es util para laterar codigo de la tabla
select  empno "codi", 
        ename "nom", 
        sal "salari",
        deptno departament
from emp
where deptno = 30
with check option; -- añadido despues, puedes quitarlo dependiendo
```
```
insert into emp30 (codi,nom,salari, departament)
values (99,'WOOBIE',3500, 30);

insert into emp30 (codi,nom,salari)
values (9199,'WEEBER',3500); -- no deja con check options por que falta el dept.


``` 

#### modificar vista i treure primer camp i fer l'insert.  
```
--hay que hacer drop para que te deje sacar el campo

drop view emp30;

create or replace view emp30 as -- create or replace view es util para laterar codigo de la tabla
select  ename "nom", 
        sal "salari",
        deptno departament
from emp
where deptno = 30
with check option; -- añadido despues, puedes quitarlo dependiendo

insert into emp30 (nom,salari,departament)
values  ('ibidivi',3000,30);
---

ERROR:  null value in column "empno" violates not-null constraint
DETAIL:  Failing row contains (null, ibidivi, null, null, null, 3000.00, null, 30).

```
**Nota:** falta la **PK**, que toma un valor NULL y por lo tanto salta un error. Igualmente pasara lo mismo cuando haya un campo en la tabla origen con **NOT NULL**.  

## Link a comanda per veure info de comandes per veure el codi i info de la vista/taules  
[llistat de comandes](C2_DDL.md)  
\d nomvista informació basica  
\dv nomvista    informació basica mes propietari  
\sv nomvista    codi de la vista  
\st nomtaula    codi de la taula  
  
  
### CLI = Command Line Interface.  

## VISTA COMPLEXA.  
  
Mostrar la definició d'empleat.  
`\sv empleat`
Es vista complexa.  
# Exercici: vista representant, mostrara: nom, nom del seu cap, numero de comandes que ha fet, numero del client amb el que va contactar la primera vegada.  





