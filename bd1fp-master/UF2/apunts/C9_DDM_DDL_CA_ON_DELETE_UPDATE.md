
# Comportament de le CAS  

Una **FK** puede tener una restricción diferente en cada tabla.

* Comportament de les **CAS**.
    * restrict
    * cascade
    * set null


**restrict** es el comportament per defecte d'una **FK**, per tant no es necesari possarlo.  
  
En una operació en **cascade**, si modifiquem la **part 1** el resultat del **delete/update** **nomes ens mostra el numero de files modificades a la part 1, no la part N**.  
  
  
```
 empno | ename  |    job    | mgr  |  hiredate  |   sal   |  comm   | deptno 
-------+--------+-----------+------+------------+---------+---------+--------
  7369 | SMITH  | CLERK     | 7902 | 1980-10-17 |  800.00 |         |     20
  7499 | ALLEN  | SALESMAN  | 7698 | 1981-02-20 | 1600.00 |  300.00 |     30
  7521 | WARD   | SALESMAN  | 7698 | 1981-02-22 | 1250.00 |  500.00 |     30
  7566 | JONES  | MANAGER   | 7839 | 1981-04-02 | 2975.00 |         |     20
  7654 | MARTIN | SALESMAN  | 7698 | 1981-09-28 | 1250.00 | 1400.00 |     30
  7698 | BLAKE  | MANAGER   | 7839 | 1981-05-01 | 2850.00 |         |     30
  7782 | CLARK  | MANAGER   | 7839 | 1981-06-09 | 2450.00 |         |     10
  7788 | SCOTT  | ANALYST   | 7566 | 1982-12-09 | 3000.00 |         |     20
  7839 | KING   | PRESIDENT |      | 1981-11-17 | 5000.00 |         |     10
  7844 | TURNER | SALESMAN  | 7698 | 1981-09-08 | 1500.00 |    0.00 |     30
  7876 | ADAMS  | CLERK     | 7788 | 1983-01-12 | 1100.00 |         |     20
  7900 | JAMES  | CLERK     | 7698 | 1981-12-03 |  950.00 |         |     30
  7902 | FORD   | ANALYST   | 7566 | 1981-12-03 | 3000.00 |         |     20
  7934 | MILLER | CLERK     | 7782 | 1982-01-23 | 1300.00 |         |     10
(14 rows)

 deptno |   dname    |   loc    
--------+------------+----------
     10 | ACCOUNTING | NEW YORK
     20 | RESEARCH   | DALLAS
     30 | SALES      | CHICAGO
     40 | OPERATIONS | BOSTON
(4 rows)

update dept
set deptno=55
where deptno=20
;

UPDATE 1
 deptno |   dname    |   loc    
--------+------------+----------
     10 | ACCOUNTING | NEW YORK
     30 | SALES      | CHICAGO
     40 | OPERATIONS | BOSTON
     55 | RESEARCH   | DALLAS
(4 rows)

 empno | ename  |    job    | mgr  |  hiredate  |   sal   |  comm   | deptno 
-------+--------+-----------+------+------------+---------+---------+--------
  7499 | ALLEN  | SALESMAN  | 7698 | 1981-02-20 | 1600.00 |  300.00 |     30
  7521 | WARD   | SALESMAN  | 7698 | 1981-02-22 | 1250.00 |  500.00 |     30
  7654 | MARTIN | SALESMAN  | 7698 | 1981-09-28 | 1250.00 | 1400.00 |     30
  7698 | BLAKE  | MANAGER   | 7839 | 1981-05-01 | 2850.00 |         |     30
  7782 | CLARK  | MANAGER   | 7839 | 1981-06-09 | 2450.00 |         |     10
  7839 | KING   | PRESIDENT |      | 1981-11-17 | 5000.00 |         |     10
  7844 | TURNER | SALESMAN  | 7698 | 1981-09-08 | 1500.00 |    0.00 |     30
  7900 | JAMES  | CLERK     | 7698 | 1981-12-03 |  950.00 |         |     30
  7934 | MILLER | CLERK     | 7782 | 1982-01-23 | 1300.00 |         |     10
  7369 | SMITH  | CLERK     | 7902 | 1980-10-17 |  800.00 |         |     55
  7566 | JONES  | MANAGER   | 7839 | 1981-04-02 | 2975.00 |         |     55
  7788 | SCOTT  | ANALYST   | 7566 | 1982-12-09 | 3000.00 |         |     55
  7876 | ADAMS  | CLERK     | 7788 | 1983-01-12 | 1100.00 |         |     55
  7902 | FORD   | ANALYST   | 7566 | 1981-12-03 | 3000.00 |         |     55
(14 rows)

delete from
dept
where deptno=55
;

DELETE 1
 empno | ename  |    job    | mgr  |  hiredate  |   sal   |  comm   | deptno 
-------+--------+-----------+------+------------+---------+---------+--------
  7499 | ALLEN  | SALESMAN  | 7698 | 1981-02-20 | 1600.00 |  300.00 |     30
  7521 | WARD   | SALESMAN  | 7698 | 1981-02-22 | 1250.00 |  500.00 |     30
  7654 | MARTIN | SALESMAN  | 7698 | 1981-09-28 | 1250.00 | 1400.00 |     30
  7698 | BLAKE  | MANAGER   | 7839 | 1981-05-01 | 2850.00 |         |     30
  7782 | CLARK  | MANAGER   | 7839 | 1981-06-09 | 2450.00 |         |     10
  7839 | KING   | PRESIDENT |      | 1981-11-17 | 5000.00 |         |     10
  7844 | TURNER | SALESMAN  | 7698 | 1981-09-08 | 1500.00 |    0.00 |     30
  7900 | JAMES  | CLERK     | 7698 | 1981-12-03 |  950.00 |         |     30
  7934 | MILLER | CLERK     | 7782 | 1982-01-23 | 1300.00 |         |     10
  7369 | SMITH  | CLERK     | 7902 | 1980-10-17 |  800.00 |         |       
  7566 | JONES  | MANAGER   | 7839 | 1981-04-02 | 2975.00 |         |       
  7788 | SCOTT  | ANALYST   | 7566 | 1982-12-09 | 3000.00 |         |       
  7876 | ADAMS  | CLERK     | 7788 | 1983-01-12 | 1100.00 |         |       
  7902 | FORD   | ANALYST   | 7566 | 1981-12-03 | 3000.00 |         |       
(14 rows)

```  
  
En una taula **M:N** no podem possar una **CAS** que affecti a les **PK/FK** com a **set null**, ja que una **PK** no pot ser **NULL**.  
  
  cascade  restrict
A  `<>`  B   `<>`   C ====> vui fer un delete de A  
   1:N        1:N  
Q: Que passara si tinc dues 1:N, si la primera es cascade i la segona es restrict?  
A: La restricció mes restrictiva es la que predomina, per tant, no et deixara.  
  
  
## Joins.  
  
* inner
    * join
* outer
    * left
    * right

