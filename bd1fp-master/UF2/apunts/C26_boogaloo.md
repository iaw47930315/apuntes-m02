### Exercisi.  
#### Fer joc de proves per a cada exercici.  
  
* Exercicis DML  
* consultes
    * Nº de files que ha de tornar cada exercici
    * Consultes necessàries per validar exercici
    * consulta de l'exercici
  
* insert (a partir de un selec)
* update (a partir de una subconsulta)
* delete (a partir de una subconsulta)
  
### Consulta Scott: Per cada departament, qui cobra mes. Lloc de proves, nom departament y nom persona.
```
select  ename,
        dname,
        max(sal)
from emp e
    left join dept d
        on e.deptno = d.deptno
where sal = (  select max(sal)
                from emp
                where empno = e.empno
                group by deptno)
                
group by ename,dname
;
```