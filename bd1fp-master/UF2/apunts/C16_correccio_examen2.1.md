# Examen 2.1.  
  
  
### 1. A nivell conceptual, difernecies entre `delete` y `drop`.  
  
        `DELETE`      informació(DML)  
  
  
**Esborren**  
  
                                        usuaris  
        `DROP`        objectes(DDL)     taules  
                                        seqüencies  
                                        BDs  
  
  
### X. Una `FK` no puede estar vacia cuando:
1. el campo tiene definido un `not null`.
2. la `FK` forma parte de su propia `PK`.

