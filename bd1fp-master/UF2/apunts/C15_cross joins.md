# JOINS  
### En un join normal, el numero de filas deve ser determinado pro la tabla que contiene la **FK**.  
## CROSS JOIN/ PRODUCTE CARTESIA  
### Igualar dos camps que no son ni **PK** ni **FK**. - Correcte.  
  
```
select t1.column, t2.column
from table1 t1, table2 t2
where camp1 = camp2 (ni **FK** ni **PK**)
```
# Exercicis  
### Mostrar per cada empleat el seu nom salari i grau.  
**Producto cartesiano/ cross join V.**
```
select
        ename,
        sal,
        grade
from emp join salgrade
on sal between losal and hisal -- non-equijoin
;
```
**Producto cartesiano/ cross join^.**  

### 11. Obtener los pedidos tomados en los mismos días en que un nuevo representante fue contratado. Mostrar número de pedido, importe, fecha pedido.  

### Mostrar representant i ciutat de cadascú (mostrar "sense oficina" si no en té).  
```
select
    nombre,
    coalesce(ciudad, 'sin oficina')
from repventa r left join oficina o
    on r.ofinum = o.ofinum
;
```
### Mostrar representant i num oficina de cadascú (mostrar "sense oficina" si no en té).  
```
select
    nombre,
    coalesce(cast(o.ofinum as text),'sin oficina')
from repventa r left join oficina o
    on r.ofinum = o.ofinum
;
```
**otro casting diferente**

```
select
    nombre,
    coalesce(o.ofinum::text,'sin oficina')
from repventa r left join oficina o
    on r.ofinum = o.ofinum
;
```
