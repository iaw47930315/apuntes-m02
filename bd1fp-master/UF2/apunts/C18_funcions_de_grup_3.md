# Funcions de grup.  
  
## Group by  
* **No se pone group by** si nadamas salen funciones de grupo en el select y ningun campo libre.  
* **Se pone group by** si en el select salen funciones de grupo y select simple.
* **Se pone group by** cuando quiero n filas.
  
## Having  
* Cuando en el "where" hay una función de grupo.  
  
### Ejercicio: Cuantos empleados hay en el departamento 30?  
```
select count(*) "total empleats departament 30" from emp where deptno=30;
---
 total empleats departament 30 
-------------------------------
                             6
(1 row)

```
  
#### Diferencia entre un script que hace consultas y otro crea una base de datos.  
Uno afecta al estado de base de datos (modifica/crea la bd) y el otro no afecta (select no modifica información).  

#### Operacions.  
```
select 
	round(max(ventas/cuota*100),2) "maximo porcentaje de rendimiento" 
from repventa
;
```
`ventas/cuota*100),2`  
Si hay operaciones de igual jerarquia, se efectuan de izquierda a derecha  
  
## Diferencia entre count * y count campo.  
El `count(*)` cuenta sin condiciones y el `count(campo)` si hay un **NULL** no lo cuenta.  

## count(1)  
select 1 from repventa; = 10 filas con el valor 1.  
select * from repventa, = 10 filas.  
Por lo tanto count(1) from repventa; y count(*) from repventa; es lo mismo, y NO ES MAS RAPIDO UNO QUE OTRO.  


# COMO HACER QUE dos tablas sean 1:1.
## Haciendo que la FK tenga constraint UNIQUE.  
