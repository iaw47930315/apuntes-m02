# Apuntes Creació i Gestió de Taules DDL EF (Esquema Fiìsic).
[link a pdf tema](https://gitlab.com/jandugar/m02-bd/blob/master/UF2_SQL/1_DDL_Taules_PostgreSQL.pdf
)  
## 1. Ordres client `psql`

- **Editar ultima ordre**  
`\e`  

- **Connect**
`\c`

- **Display**  
`\d`  

- **Import** - executascript SQL  
`\i`  

- **Mostra el llistat de BD**  
`\l`  

- **Quit**  
`\q`

- **Help**  
`\h`  

- **Display all \ commands**  
`\?`  

- **Toggles expanded display**  
`\x`  

- **Shows the content of the query buffer**  
`\p`  

- **Mostra usuaris de BD**  
`\du`  

- **Mostrar historia de comandes disnts de Postgres**  
`\s`  

- **Mostrar nomes taules**  
`\dt`

- **Mostrar tots els objectes**  
`\d`  

- **Mostrar codi de tablas o vistes**  
`\st tablename`  
`\sv viewname`

- **toggle header and feet**  
`\t`

- **error code exit on error detected on psql**  
`-v ON_ERROR_STOP = 1`  

- **show schema (namespace)**  
`\dn`

- **show databases ()**  
`\l`

- **show query internal code**  
`psql -E`
  
- **show indexes**  
`\di`
  
- **show permissions on database per table**  
`\dp`  

- **show functions**  
`\df`
  
- **show code offunctions**  
`\sf`
      
- **show types**  
`\dT`
  
- **shows info on command**
`\h create trigger`

* Script: fitxer de text pla de codi informatic i es executable. Conte, o be sentencies d'un sistema operatiu (bash) o sentencies propies d'un llenguatge informatic interpretable (SQL).



## 1.2 PSQL  
`psql -d template1 -f '/home/users/inf/hiaw1/iaw14270791/Desktop/Clase/git/bd/iglesiasjacint/UF2/scripts/taulesScott.sql'`  

`psql` es util per automatizar tasques (p.e. bash scripting, cron), executem ordres psql sin necesidad de permanecer conectados a PostgreSQL.  

**Acostumarse a fer els nostres scripts desde `psql`, ens trovarem molts errors d'aquesta manera, pero ens acostumarem a l'excelencia.**  

**Exemple de `psql` amb el nostre script `habitatge.sql`**:
`psql -d template1 -f '/home/users/inf/hiaw1/iaw14270791/Desktop/Clase/git/bd/iglesiasjacint/UF2/scripts/habitatge.sql'`  

`vim nomfitxer numlinea`  

**Display line count**  
`set nu`  


### smallint/int/bigint

- smallint:
- - 2bytes  
- - 65536 (combinaciones), 65535 (rango)  

- int:  
- - 4bytes  
- - ...  


### A-B dos relaccions 1:N les dues sent part N


A(<ins>A</ins>, *B*)  
B(<ins>B</ins>, *A*)  

A l'hora de fer l'esquema físic, no possarem la clau aliena d'una de les dues (**la primera**), i l'introduirem despres amb un alter table.  


### Alter  
**Alter:** modifica una estructura.  

```
ALTER TABLE  emp
add constraint
    emp_deptno_fk foreign key (deptno) references dept;  
ALTER TABLE  emp
    add constraint emp_mgr_fk foreign key (mgr) references emp;
```  



### Errors tipics Model Fìsic.  

- **FK** i **PK** no comparteixen el mateix **tipus de dades**, per exemple `VARCHAR(20)`  
- **PK compuesta** declarada como fila, utilizando la frase reservada `PRIMARY KEY` **dos veces**  
-











**SQL:** Es un llenguatge informatic.  

**SQL**  

* DDL  
* DML  
* DCL  
* Select  

**DDL**  

* Taules  
* Vistes  
* Índex  

**Sentencies SQL**  

* Create  
* Drop  
* Alter  


DDL|DML
-|:-:
CREATE|INSERT
DROP|DELETE
ALTER|UPDATE
Objectes|Informació




**No es pot duplicar el nom d'un objecte (taula/vista) ni es pot possar un nom reservat a un objecte (create...)**  


El tipus de dades es el com s'enmagatzemen les dades i el tamany d'enmagatzematge.  

**string**, cadena, VARCHAR - bytes tipos de datos de cadena de caracteres **de longitud acotada**  



* VARCHAR  
* INT  
* NUMERIC(e,p) (escala, precisió) (longitud total, part decimal)  
* DATE, TIMESTAMP (epoch, unix time internally)  
⋅⋅* current_date  
⋅⋅* current_timestamp  

**Exemple:**
**ename VARCHAR(10),**  
nombre de campo, tipo de datos (cadena de caracteres), tamaño(10 bytes)  

INT, 4 bytes  
smallint, 2 bytes  
bigint, 8 bytes  

numeric, exemple  
NUMERIC(5,2) => 0 - 999.99  

hiredate date DEFAULT current_date,  
nom camp, tipus data  



**Reglas de PK**  
* No es pot repetir  
* No pot ser null  
* Ha de ser minima  


**Restriccions**  
* PRIMARY KEY  
* FOREIGN KEY  
* NOT NULL  
* UNIQUE  
* CHECK  


**Exemple constraint:**  
create table dept (
    deptno INTEGER CONSTRAINT dept_deptno_pk PRIMARY KEY,  
...);  
Explicació:  
nom camp, tipus de dades, constraint nom constraint, tipus de constraint  
- constraint a nivell de camp ^  

- contraint a nivell de taula v  

deptno INTEGER,  
...  
CONSTRAINT dept_deptno_pk PRIMARY KEY (deptno)  

**PK compuesta:**  
Se pone a nivel de tabla  
CONSTRAINT animal_codZooCodAnimal_pk PRIMARY KEY (codZoo,codAnimal)  


**FK**  
FK a nivell de camp no menciona FOREIGN KEY, sino REFERENCES  

CONSTRAINT emp_deptno_fk FOREIGN KEY (deptno) REFERENCES dept (deptno),  

**FK composta**  
CONSTRAINT pedido_fabprod_fk FOREIGN KEY (fabcod,prodcod REFERENCES producto (fabcod,prodcod)  



~Apunte:  
Un telefono, si contempla prefijos o numeros extranjeros, se almacena como varchar, por los mas y parentesis.  

Un nom minim 50 bytes de varchar.
