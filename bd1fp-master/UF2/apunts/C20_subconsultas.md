# Subconsultas.  
  
## Definición: cuando en una consulta nos falta información.  
  
## La subconsulta se ejecuta una vez y antes de la consulta principal.  
  
## Una subconsulta puede retornalr:  
* 1 fila y 1 columna
* n filas y 1 columna
* n filas y n columnas
  
## **No** hacer servir `ORDER BY` en una subconsulta. Si no muestro información no es necesario ordenar la información.  
  
## Subconsultas pueden ser:  
* mono registro
* multi registro
## Monoregistro.  
  
```
select ename, job
from emp
where sal < (   select sal
                from emp
                where lower(ename) = 'james'
             )
;
```
## Una subconsluta puede ir en el HAVING  
Muestra los departamentos con el salario mas pequeño que el salario mas pequeño del departamento 20.  
  
```
select deptno, min(sal)
from emp
group by deptno
having min(sal) >
    (select min(sal)
    from emp
    where deptno = 20)
;
```
## Operador mono registro vs consulta multi registro.  
```
select empno, ename
from emp
where sal=
        (select min(sal)
        from emp
        group by deptno)
;
ERROR:  more than one row returned by a subquery used as an expression
```
**Nota:** Esta consulta por que a un operador mono registro `=` le pasamos multiples registros.  
  
## Operadores multi registro:  
* in
* any - operador rel + any - not in
* all - operador rel + all  
  
[operadores multi registro](C11_DDM_DDL_like_not_in_!=_all .md)  
  
### Ejemplos.  
  
```
select ename, job
from emp
where job =
        (select job
        from emp
        where ename = 'asidusaid')
;
 ename | job 
-------+-----
(0 rows)

```
```
select empno, enmae
from emp
where sal > any
    (select sal
    from emp
    where upper(job)= 'CLERK'
    and upper(job) <> 'CLERK')
;
```
**Nota:** ^consulta multi registro. Hay consultas multi columna, esta no es una de ellas.  
**Nota2:** esta subconsulta se puede optimizar con un `sal > (... min(sal)...`  
  
### ANY y ALL en general tratar de no utilizar nunca.  


```
select empno, ename, job
from emp
where sal > any
    (select sal
    from emp
    where upper(job) = 'CLERK');
```

```
select empno, ename, job
from emp
where sal > 
    (select min(sal)
    from emp
    where upper(job) = 'CLERK');
```
**Nota:** > min(sal) esta optimizada. Se transforma una consulta **multiregistro** en una subconsulta **monoregistro**.  
  
```
select empno, ename, job
from emp
where sal >
    (select min(sal)
    from emp
    where upper(job) = 'CLERK')
and upper(job) != 'CLERK'
;

```
**Nota:** Igual que la anterior pero excluye a los job CLERK.  
  
### Exercici: mostrar els departaments que no tenen assignat cap empleat.  
```
select deptno
from dept
where deptno  not in (    select deptno
                        from emp
                        where deptno is not null);
```
**Nota:** En la tabla emp hay un deptno que es NULL, si no incluimos la clausula **where deptno is not null** no nos devolvera nada, por que operamos con NULL.  
  
**Nota 1.2:** No se puede comparar un **NULL**.  
Dept.deptno  NOT IN      emp.deptno  
10           != ALL      10         40 !=   TRUE  
20                       20         "" ""   TRUE  
30                       30         "" ""   TRUE  
40                       NULL       "" ""   FALSE  

**Nota 1.3:** CUALQUIER VALOR COMPARADO CON **NULL** ES FALSE, INCLUIDO OTRO **NULL**.  
**Nota2:**  
Dept.deptno  -      emp.deptno  
10                  10  
20                  20  
30                  30  
40  
  
**Nota 3:** Podriamos hacer un coalesce **pero** con cuidado que fuera un valor fuera de rango. Preferible poner un **IS NOT NULL**.  
### Exercici: mostrar el nom dels empleats que són cap.  
```
select ename
from emp
where empno  in ( select mgr
                    from emp
                    --where mgr is not null -- NO HACE FALTA EN UN IN
                    );

```
**Nota:** no hace falta IS NOT NULL en el IN por que:  
IN = true OR false OR ture OR...  
NOT IN = true AND false AND true...  
**Nota:** si que hace falta and **IS NOT NULL** en el **NOT IN**  
### Igual pero que no son cap.  
  
## .
* In     = =ANY  => accepta nulls  
* NOT IN = !=ALL => no acepta nulls  
```
select ename
from emp
where empno not in ( select mgr
                    from emp
                    where mgr is not null);

  
### Ejemplo: ¿Qué empleados tienen un salario superior al de Jones?  
```
select ename 
from emp
where sal > 
    (select sal
    from emp
    where lower(ename) = 'allen')
;
```
  
### Enunciado, quien es la persona mejor pagada y la peor pagada?
  
```
select ename
from emp
where sal = 
    (
    select min(sal)
    from emp
    )
;
```
### Apaño workarround erroneo a subconsultas.  

```
select ename
from emp
order by sal
limit 1
;

```
**Nota:** Funciona si solo hay 1 empleado con salario minimo, pero no si hay multiples con el mismo salario minimo (solo mostraria uno).Por lo tanto, **emplear subconsultas**.  

## Subconsultas pueden ir en el select from o where.  






#### Ejemplo subconsulta vs join.  
```

```
select ename
from emp
where empno = ( select mgr
                from emp
	            where ename = 'SMITH')
;
------
select j.ename
from emp e join emp j
	on e.mgr = j.empno
where e.ename = 'SMITH'
;
```



```

```

# HACER EGERCICIOS TRAINING SUBCONSULTAS. Separar scripts por apartados (de subconsultas, p.e. exerciccisTrainingSubqueries.sql)  
