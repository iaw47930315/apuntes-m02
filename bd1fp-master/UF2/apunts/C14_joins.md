## Consulta.  
#### Mostreu el nombre d'empleat, el salari, el salari incrementat un 26% i l'increment del 26%.  
```
select 
        empno empleat,
        sal salari, 
        sal * 0.26 "increment",
        sal + sal * 0.26 "salari incrementat"
from emp
;
```
# Si hi han errors sintactics a l'escript no es corretgira ($? = 0).  
  
# Examen    30' prova escrita (sense apunts) (S'ha d'aprovar) - 20%  
#           2h prova ordinador (amb apunts)                   - 80%  
  

# EN EL EXAMEN NO PONER ALIAS SI NO SE PIDE EN EL ENUNCIADO!  
# EL SELECT EN EL ORDEN QUE SE PIDE EN EL ENUNCIADO!  
# SOLO HACER LO QUE SE PIDE (P.E. SI NO SE PIDE ORDER BY, NO HACERLO)  
  
# Teoria de joins.  
### En un join, poner siempre la parte N que la parte 1 en la condición de join  
```
on pedido.fabcod = producto fabcod
```
 
### Cuantes files te el join?  
```
A   clau aliena x =>      B     join  
5flies                      10 files  
```  
### Resposta:  
**Mana** la taula que te la **clau aliena** (el mateix numero de files que la taula amb la **FK** o taula origen).  
## Producte cartesiá.  
Es un join sense restricció, es a dir la **FK** no esta igualada ala **PK**.  
  
## INNER JOIN vs OUTER JOIN  
**Inner Join**: juntar las filas de dos tablas donde coinciden **FK** con **PK**.

# EJERCICIO:
Mostreu nom i ciutat de tots els empleats.
- inner join
```
select 
    nombre,
    ciudad
from repventa r join oficina o
    on r.ofinum = o.ofinum
;
```
- outer join (**forçar** la **sortida** de files que tenen **null** a la **FK**)
```
select 
    nombre,
    ciudad
from repventa r left join oficina o
    on r.ofinum = o.ofinum
;
```
```
select e.ename, d.deptno, d.dname
from    emp e right join dept d
    on e.deptno= d.deptno
order by e.deptno
;
```

# EJERCICIOS.
### Mostrar el codi i el nom de l'empleat i el codi i el nom del seu cap.
- training
```
select 
    e.repcod "codigo empleado",
    e.nombre "nombre empleado",
    j.repcod "codigo jefe",
    j.nombre "nombre jefe"
from repventa e left join repventa j
    on e.jefe = j.repcod
;
```
- scott
```
select 
    e.empno "codigo empleado",
    e.ename "nombre empleado",
    j.empno "codigo jefe",
    j.ename "nombre jefe"
from emp e left join emp j
    on e.mgr = j.empno
;
```