# Funcions de grup.  
  
```
/*
	orden correcto es:
select
from
group by
having
order by
*/
```

## count(*) vs count(campo)

#### count(campo) no cuenta nulls, count (*) cuenta todos a pelo

```
scott=> select count(*) from emp;
 count 
-------
    14
(1 row)


select cound(comm) from emp;
 count 
-------
     4
(1 row)

```
Por que tiene nulls.

Numero d'empleats que estan asignats a una oficina;
```
select count(ofinum)
from repventa;
```
```
select count(*)
from repventa
where ofinum IS NOT NULL;
```

# Exercisi: Mostrar l'empleat (scott) que esta pitjor pagat. I el pitjor pagat.  
```
select ename 
from emp where sal = 
    (select min(sal) 
    from emp)
;
```
# Mostrar el nom de l'empleat que cobra 800;  
```
select ename 
from emp where sal = 800
;
```
