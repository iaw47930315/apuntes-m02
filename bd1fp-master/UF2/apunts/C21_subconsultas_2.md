# Subconsultas.  
  
#### Exercici: mostra els salaris mes baixos per departament que sugin mes baixos que el salari mes baix del departament 20.  
```
select  deptno, 
        min(sal) 
from emp 
group by deptno 
having min(sal) > (     select min(sal) 
                        from emp 
                        where deptno = 20);
```
  
## Comparativas multiregistro.  
* `>ANY`    => `>min`
* `>ALL`    => `>max`
* `NOT IN`  => `!all`

### NO USAR `>ANY` o `>ALL`, usar `>min` o `>max` respectivamente.  
  
#### Exercici (multicolumna). Mostrar els empleats que treballen al mateix departament i del mateix job.  
```
select ename, deptno, job
from emp e
where (deptno,job) in ( select deptno, job
                        from emp
                        where empno != e.empno );
```
**Nota:** S'ha d'afegir una condició per que no s'iguali amb si mateix `where empno != e.empno`.  
## REFERENCIES EXTERNES.  
Es comparar la consulta primaria amb la subconsulta.  
```
select ename, deptno, job
from emp e
where (deptno,job) in ( select deptno, job
                        from emp
                        where empno != e.empno )
order by 2,3;
```
## Subconsulta en FROM es diu taula temporal.  
  
#### Mostra el nom, salari i departaments empleats que cobren mes que la mitja del seu departament i la mitja del seu departament.  
```
select a.ename, a.sal, a.deptno, b.salavg
from emp a, ( select deptno, round(avg(sal),2) salavg
             from emp
             group by deptno) b
where a.deptno = b.deptno
and a.sal > b.salavg
order by 2,3;
```
** NOTISIMA:** esta es buena ^
**Nota:** con a.deptno = b.deptno, asociamos los campos. Se vinculan.  
**Nota2:** si no declaramos el campo deptno en la subconsulta, no se puede hacer el join.  
#### Exercici, fer lo mateix que l'anterior pero snese subconsulta.  
```
select ename, deptno, sal, round(avg(sal),2) "mitjana departament"
from emp
where sal > (select avg(sal)
            from emp e
            where e.deptno = emp.deptno)
group by 1,2,3
order by 2,3;

```
**Nota:** referencia externa + mono resultado.  
```
select   a.ename, a.sal, a.deptno, round(avg(b.sal),2) "mitja salari departament"
from emp a join emp b
        on a.deptno = b.deptno
group by 1,2,3
having a.sal >  avg(b.sal)
order by 2,3
;
-- funciona pero esta MAL por join de clave ajena que hace un producto cartesiano

```
  
```
select a.ename, a.sal, a.deptno, b.salavg
from emp a, ( select deptno, round(avg(sal),2) salavg
             from emp
             group by deptno) b
where a.deptno = b.deptno
order by 3,2;

```
  
### Inner join, manera.  
```
select *
from a,b
where a.ca = b.cp;
```
