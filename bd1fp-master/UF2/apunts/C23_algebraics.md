# Operadors algebraics o d'algebra  

### La capçalera ha de ser del mateix <ins>grau</ins>.  
  
* union (union all) - es conmutativa => B union A = A union B
* intersect         - es conmutativa => B intersect A = A intersect B (es como un AND, este en A AND B)
* except ("minus" en oracle) - NO ES CONMUTATIVA 
  
```
    A       B
   1 4     4  8
  11 9     9  2
            7
```
A union B = 4,11,9,8,7,2,1 (suma de todos los elementos, sin repeticiones)  
A union all B = 1, 2,4,4,8,9,9, 11 (suma, con repetidos)  
A intersect B = 4, 9 (comunes)  
A except B = 1, 11 (elementsdel PRIMER conjunt QUE NO ES TROVEN AL SEGON)  

## **Conjunt:** una serie d'elements que tenen les seguents propietaats:
* son del mateix tipus
* no hi han repetits
* no existeix ordre
  

  
# Llistes, DDL.  
