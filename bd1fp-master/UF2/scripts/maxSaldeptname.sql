-- Script: 			repventasFuncionsDeGrup.sql
-- Descripció: 		exercicis de subconsultes a training
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1


-- Salari maxim per departament, nom de departament iempleat

select 	dname,
		coalesce(ename,'no hi ha empleats')
from emp e 	right join dept d 	on e.deptno = d.deptno
where (d.deptno, coalesce(sal,0)) in (	select de.deptno, coalesce(max(sal),0)
							from emp em right join dept de 
								on em.deptno = de.deptno
							group by de.deptno);



