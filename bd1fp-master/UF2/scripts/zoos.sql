\c template1
drop database if exists zoos;
create database zoos;
\c zoos

--
-- Name: zoo; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE zoo (
    idzoo smallint,
    ciutat varchar(150),
    pais varchar(150),
    mida numeric(9,2),
    pressupost numeric(10,2),
    CONSTRAINT PK_ZOO_IDZOO PRIMARY KEY(idzoo)
);


--
-- Name: especie; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE especie (
    idespecie smallint,
    nomvulgar varchar(150),
    nomcientific varchar(150), 
    familia varchar(150),
    --perillextincio char(1), -- es preferible que sigui boolea
    perillextincio boolean,
    CONSTRAINT PK_ESPECIE_IDESPECIE PRIMARY KEY(idespecie),
    CONSTRAINT UN_ESPECIE_NOMCIENTIFIC UNIQUE(nomcientific)
    --CONSTRAINT CH_ESPECIE_PERILLEXTINCIO CHECK ( lower(perillextincio) in ('y', 'n') )
);


--
-- Name: animal; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE animal (
    zoo smallint,
    animal smallint,
    nom varchar(150),
    sexe char(1),
    anyneixement date NOT NULL,
    paisorigen varchar(150),
    continent varchar(7),
    especie smallint,
    CONSTRAINT PK_ANIMAL_ZA PRIMARY KEY(zoo,animal),
    CONSTRAINT FK_ANIMAL_ZOO FOREIGN KEY(zoo) REFERENCES zoo(idzoo),
    CONSTRAINT FK_ANIMAL_ESPECIE FOREIGN KEY(especie) REFERENCES especie(idespecie),
    CONSTRAINT CH_ESPECIE_SEXE CHECK ( lower(sexe) in ('m', 'f') ),
    CONSTRAINT CH_ESPECIE_ANYNEIXEMENT CHECK ( anyneixement >= current_date - interval '500 years' AND  anyneixement <= current_date + interval '100 years')
);


--
-- Inserts
--


--
-- Taula zoo
--
INSERT INTO zoo VALUES
        (1, 'Barcelona', 'Espanya', 3500.20, 6000000.54);
INSERT INTO zoo VALUES
        (2, 'Madrid', 'Espanya', 2500.50, 89000000.32);
INSERT INTO zoo VALUES
        (3, 'Conca', 'Espanya', 1500.60, 700000.25);
        
--
-- Taula taula especie
--
INSERT INTO especie VALUES
        (1, 'llop', 'Canis lupus', 'Canidae', false);
INSERT INTO especie VALUES
        (2, 'os', 'Ursidae', 'Ursidae', false);
INSERT INTO especie VALUES
        (3, 'linx iberic', 'Lynx pardinus', 'Felidae', true);


--
-- Taula taula animal
--
INSERT INTO animal VALUES
        (1, 1,'Wolverine', 'm', current_date - interval '10 years', 'Espanya', 'Europa', 1);
INSERT INTO animal VALUES
        (1, 2,'Ragnarok', 'f', current_date - interval '5 years', 'Espanya', 'Europa', 1);
INSERT INTO animal VALUES
        (2, 1, 'Yogi', 'm', current_date - interval '20 years', 'Russia', 'Asia', 2);
INSERT INTO animal VALUES
        (3, 1, 'Linx', 'f', current_date - interval '8 years', 'Espanya', 'Europa', 3);
        

       
--
-- Inserts prova d'error
--        
INSERT INTO especie VALUES
        (4, 'llop', 'Canis lupus', 'Canidae', 'n'); 
        -- insertamos especie repetida cuando esta en constraint UNIQUE
INSERT INTO especie VALUES
        (5, 'topillo', 'Arvicolinae', 'Cricetidae', 'z');
        -- insertamos 'z' como valor boolean
INSERT INTO animal VALUES
        (4, 1, 'Linx', 'f', current_date - interval '600 years', 'Espanya', 'Europa', 3);
        -- insertamos año fuera de rango definido en check

