/*			--PSQL--		*/

-- psql amb consulta final
psql -d training -c "select current_date;"

--psql amb import i consulta
psql -d template1 -f scott.sql -c "select * from emp;"

/*			--DROP TABLE IF EXISTS + DROP DATABASE IF EXISTS--		*/
\c habitatge
\echo "Borrando tablas."
drop table if exists vivendaxpersona ;
drop table if exists persona ;
drop table if exists vivenda ;
drop table if exists municipi;
\c template1
\echo "Borrando base de datos."
drop database if exists habitatge;
create database habitatge;
\c habitatge

/*			--CREATE TABLE--		*/

-- create table as
create table empleado as 
	select * 
	form emp
;

--
create table managers(id, nom, salari, data)
    as SELECT  empno, ename, sal, hiredate
    FROM    emp
;

-- with empty table
create table x
as
        select *
        from emp
        where 1=0
;

-- primary key PK
CREATE TABLE municipi (
    idMunicipi int,
    nom varchar(50),
    CONSTRAINT PK_MUNICIPI_IDMUNICIPI PRIMARY KEY(idMunicipi)
);

-- foreing key FK
CREATE TABLE vivenda (
    idCatastre int,
    direcio varchar(150),
    m2 numeric(6,2),
    municipi int,
    CONSTRAINT PK_VIVENDA_IDCATASTRE PRIMARY KEY(idCatastre),
    CONSTRAINT FK_VIVENDA_MUNICIPI FOREIGN KEY(municipi) REFERENCES municipi(idMunicipi)
		on delete set null
		on update cascade
);
-- unique
CREATE TABLE DEPT
       (DEPTNO smallint  constraint dept_pk primary key,
        DNAME character varying(14),
        LOC character varying(13),
        constraint emp_uk unique(dname)
	);
-- not null
CREATE TABLE producto (
    fabcod character(3),
    prodcod character(5),
    descrip character varying(20) NOT NULL,
    precio numeric(7,2) NOT NULL,
    exist integer,
    CONSTRAINT PK_PRODUCTO_FP PRIMARY KEY(FABCOD,PRODCOD)
	);
-- check
CREATE TABLE carretera (
    idCarretera smallint,
    tramsNum smallint,
    longitud numeric(8,3),
    tipus varchar(15),
    CONSTRAINT PK_CARRETERA_IDCARRETERA PRIMARY KEY(idCarretera),
    CONSTRAINT CH_CARRETERA_TIPUS CHECK ( lower(tipus) in ('comarcal', 'regional', 'nacional') )
);


/*			--ALTER TABLE--		*/

-- add constraint (FK)
alter table vivendaXpersona
	add 
		CONSTRAINT FK_VIVENDAXPERSONA_VIVENDA FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on update cascade
		on delete cascade
    ,
    add
		CONSTRAINT FK_VIVENDAXPERSONA_PERSONA FOREIGN KEY(persona) REFERENCES persona(dni)
		on update cascade
		on delete cascade
;

-- drop constraint
alter table vivendaXpersona
	drop constraint 
		FK_VIVENDAXPERSONA_PERSONA
;

--alter drop column
alter table vivendaXpersona
	drop column 
		vivenda
;

--alter table add column
alter table vivendaXpersona
	add 
		vivenda int
;
	
/*			--ON UPDATE/DELETE CASCADE/SET NULL--		*/

-- cascade
CREATE TABLE vivendaXpersona (
    vivenda int,
    persona varchar(9),
    CONSTRAINT PK_VIVENDAXPERSONA_VP PRIMARY KEY(vivenda,persona),
    CONSTRAINT FK_VIVENDAXPERSONA_VIVENDA FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on update cascade
		on delete cascade
    ,
    CONSTRAINT FK_VIVENDAXPERSONA_PERSONA FOREIGN KEY(persona) REFERENCES persona(dni)
		on update cascade
		on delete cascade
);

--set null
CREATE TABLE persona (
    dni varchar(9),
    nom varchar(50),
    cognom varchar(100),
    email varchar(125),
    telefon varchar(12),
    vivenda int,
    capFamilia varchar(9),
    CONSTRAINT PK_PERSONA_DNI PRIMARY KEY(dni),
    CONSTRAINT FK_PERSONA_CATASTRE FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on delete set null
		on update cascade
    ,
    CONSTRAINT FK_PERSONA_CAPFAMILIA FOREIGN KEY(capFamilia) REFERENCES persona(dni)
		on update cascade
		on delete set null
);

-- plus alter table
alter table vivendaXpersona
	add 
		CONSTRAINT FK_VIVENDAXPERSONA_VIVENDA FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on update cascade
		on delete cascade
    ,
    add
		CONSTRAINT FK_VIVENDAXPERSONA_PERSONA FOREIGN KEY(persona) REFERENCES persona(dni)
		on update cascade
		on delete cascade
;

/*			--SEQUENCE--		*/
-- create sequence
create sequence  if not exists deptno_seq
start with 50 increment by 10;

-- increment sequence
INSERT INTO vivenda 
VALUES(nextval('seq_idCatastre'), 'C/ Santiago nº 78 7º 3ª', 60.50, 1);

-- freeze sequence
INSERT INTO persona 
VALUES('13361682F', 'Jorge', 'Hurdler Durdler', 'hurlerdurler@gmail.com', '+34748334275', currval('seq_idCatastre'), NULL);

-- drop sequence
drop sequence seq_scott; 
 
/*			--INSERT--		*/

-- insert with specific fields "implicit insert" (always use this one since it's standard SQL)
insert into emp  
    (empno,ename,deptno)  
    values (9234, 'Francis', 10);

-- insert with all values "explicit insert"
insert into emp  
    values (9234,NULL, NULL, 'Francis', 10);

-- massive insert
INSERT INTO managers(id,name, salary,hiredate)
    SELECT  empno, ename, sal, hiredate
    FROM    emp
;  
--
INSERT INTO managers (id, nom, salari, data)
    SELECT  empno, ename, sal, hiredate
    FROM    emp
;

/*			--UPDATE--		*/

-- regular update with where
update  emp
set     deptno = 20
where   empno = 7782
;

-- update with multiple fields
update emp
set 
	sal=2000, 
	deptno = 20
where empno = 7782
;
