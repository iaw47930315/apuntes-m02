-- Script: 			exerciciMaxMesJoins.sql
-- Descripció: 		exercici de funcions de grup, joins i subconsultes extra de scott
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1

-- Avançats

-- 48. Mostreu els empleats que treballen en el mateix departament que Clark.
\echo '48. Mostreu els empleats que treballen en el mateix departament que Clark.'
select *
from emp 
where deptno = (	select deptno
					from emp
					where lower(ename) = 'clark');


-- 49. Calcular el nombre d’empleats per departament que tenen un salari superior a la mitjana.
\echo '49. Calcular el nombre d’empleats per departament que tenen un salari superior a la mitjana.'

select count(*), deptno
from emp e
where sal > (	select avg(sal)
				from emp
				where deptno = e.deptno)
group by deptno;
				



-- 50. Mostreu els empleats el salari sigui superior al d’Adams.
\echo '50. Mostreu els empleats el salari sigui superior al d’Adams.'

select *
from emp e
where sal > (	select sal
				from emp
				where lower(ename) = 'adams');


-- 51. Mostreu el nom i data d’ingrés de l’empleat que porta menys temps.
\echo '51. Mostreu el nom i data d’ingrés de l’empleat que porta menys temps.'

select 	ename,
		hiredate
from emp
where hiredate = (	select max(hiredate)
					from emp);


-- 52. Mostreu el nom dels empleats que guanyin més que qualsevol salesman (es a dir, que guanyi més que
-- tots els salesman).
\echo 'Mostreu el nom dels empleats que guanyin més que qualsevol salesman (es a dir, que guanyi més que'
\echo 'tots els salesman).'

select ename
from emp
where sal > (	select max(sal)
				from emp 
				where lower(job) = 'salesman');



-- 53. Mostreu els empleats que guanyin més que algun dels que treballen de salesman.
\echo '53. Mostreu els empleats que guanyin més que algun dels que treballen de salesman.'

select ename
from emp
where sal > (	select min(sal)
				from emp 
				where lower(job) = 'salesman');
				
				
-- MES EXERCICIS!


-- Més exercicis
-- 54. Mostreu el treball, el nom i el salari dels empleats ordenats pel 
-- tipus de treball i per salari descendent.
select	job,
		ename, 
		sal
from emp
order by 1, 3 desc;


-- 55. Mostreu el nom de cada empleat, i el nombre i nom del seu cap.

select	e.ename "Empleat",
		j.ename "Cap"
from emp e
	left join emp j
		on e.mgr = j.empno;


-- 56. Mostreu el nom de l’empleat i la seva data d’alta en l’empresa dels 
-- empleats que són analyst.

select	ename,
		hiredate
from emp
where lower(job) = 'salesman';

-- 57. Mostreu el nom de l’empleat i una columna que contingui el salari 
-- multiplicat per la comissió la capçalera sigui ‘BO’.

select	ename,
		sal * coalesce(comm, 1) "BO"
from emp;

-- 58. Trobeu el salari mitjà d’aquells empleats el treball sigui el 
-- d’analista (analyst).

select	round(avg(sal),2)
from emp
where lower(job) = 'analyst';

-- 59. Trobeu el salari més alt, el més baix i la diferència entre tots dos.

select	max(sal) "salari més alt",
		min(sal) "salari més baix",
		max(sal) - min(sal) "diferencia entre salaris mes alt i baix"
from emp;

-- 60. Trobeu el nombre de treballs diferents que hi ha al departament 30.

select count(distinct job) "numero de treballs diferents al departament 30"
from emp
where deptno = 30;

-- 61. Mostreu el nom de l’empleat, el seu treball, el nom i el codi de 
-- l’departament en el qual treballa.

select	ename,
		job,
		dname,
		d.deptno -- si pones e.deptno no muestra el 40, porqu esta en
				-- en d.deptno
from emp e
	full outer join dept d
		on e.deptno = d.deptno;

-- 62. Mostreu el nom, el treball i el salari de tots els empleats que 
-- tenen un salari superior a el salari més baix de el departament 30.

select	ename,
		job,
		sal
from emp
where sal < (	select min(sal)
				from emp
				where deptno = 30);

-- 63. Trobeu als empleats el cap és ‘BLAKE’.

select	e.*
from emp e
	left join emp j
		on e.mgr = j.empno
where lower(j.ename) = 'blake';

-- 64. Trobeu el nombre de treballadors diferents en el departament 30 per 
-- a aquells empleats el salari pertanyi a l’interval [1000, 1800].

select count(*) "numero de treballadors"
from emp
where deptno = 30 and
sal between 1000 and 1800;

-- 65. Trobeu el Ename, dname, job i sal dels empleats que treballin en el 
-- mateix departament que ‘TURNER’ i el seu salari sigui més gran que la 
-- mitjana de l’salari de el departament 10.

select	ename,
		dname,
		job,
		sal
from emp e
	join dept d
		on e.deptno = d.deptno
where e.deptno = (	select deptno
					from emp
					where lower(ename) = 'turner')
and sal > (	select avg(sal)
			from emp
			where deptno = 10);

-- 66. Indiqueu si les següents sentències són correctes, si no ho són, 
-- indiqueu en què consisteix l’error:

--A.

SELECT *
FROM EMP
WHERE MGR = NULL;
-- mgr is null

--B.
SELECT *
FROM DEPT
WHERE DEPTNO = 20 OR WHERE DEPTNO = 30;
-- or deptno = 30


--C.
SELECT *
FROM EMP
WHERE NOT ename LIKE ’R%’ AND sal BETWEEN 3000 AND 5000;

SELECT *
FROM emp
WHERE ename not LIKE 'R%' AND sal BETWEEN 3000 AND 5000;


D.
SELECT *
FROM EMP
WHERE sal < 4000 AND job NOT = ’ANALYST’;
-- !=
SELECT *
FROM EMP
WHERE sal < 4000 AND job != 'ANALYST';


E.
SELECT *
FROM DEPT
WHERE loc = ’DALLAS’ OR ’CHICAGO’;

lower(loc) = 'dallas' or lower(loc) = 'chicago'; 


-- 67. El salari mitjà i mínim de cada lloc, mostrant en el resultat 
-- aquells el salari mitjà estigui per sobre de 1500.

select	round(avg(sal),2) "salari mitja",
		round(min(sal),2) "salari mínim",
		job
from emp
group by job
having avg(sal) > 1500
;

-- 68. Què empleats treballen a ‘DALLAS’?

select e.*
from emp e
	join dept d
		on e.deptno = d.deptno
where lower(loc) = 'dallas';

-- 69. Quants emprat treballen a ‘CHICAGO’?

select count(*) "Nombre d'empleats a DALLAS"
from emp e
	join dept d
		on e.deptno = d.deptno
where lower(loc) = 'chicago';

-- 70. Llistar el nom dels empleats que guanyen menys que els seus s
-- upervisors.

select e.ename
from emp e
	join emp j
		on e.mgr = j.empno
where e.sal < j.sal;

-- 71. Llistar el nom, treball, departament, localitat i salari d’aquells 
-- empleats que tinguin un salari major de 2000 i treballin a ‘DALLAS’ o 
-- ‘NEW YORK’.

select	ename,
		job,
		e.deptno,
		loc,
		sal
from emp e
	join dept d
		on e.deptno = d.deptno
where sal > 2000 and
lower(loc) = 'dallas';

