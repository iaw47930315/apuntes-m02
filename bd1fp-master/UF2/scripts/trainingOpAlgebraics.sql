-- Script: 			repventasFuncionsDeGrup.sql
-- Descripció: 		exercicis de operadors algebraics a training
-- Autor: 			Jacint Iglesias Casanova
-- Codi: 			iaw14270791
-- Darrera revisió:	09/01/2020
-- Versió:			0.1

-- 1. Obtener una lista de todos los productos cuyo precio exceda de 20 
 -- euros y de los cuales hay algún pedido con un importe superior a 200 
 -- euros.
select distinct p.* 
from producto p join pedido pe
	on pe.prodcod||pe.fabcod = p.prodcod||p.fabcod
where precio > 20 and importe > 200;


-- con algebraicas

select prodcod, fabcod
from producto
where precio > 20 
intersect
select prodcod, fabcod
from pedido
where importe > 200
;

-- 2. Obtener una lista de todos los productos cuyo precio más IVA 
-- exceda de 20 euros o bien haya algún pedido cuyo importe más IVA 
-- exceda los 180 euros.

-- IVA 21%

select fabcod || prodcod "Codi Producte"
from producto
where precio + precio * 0.21 > 20 
union
select fabcod || prodcod
from pedido
where importe + importe * 0.21 > 180
;

select distinct p.*
from producto p
	join pedido pe
		on pe.prodcod||pe.fabcod = p.prodcod||p.fabcod
where precio * 1.21 > 20
or importe *1.21 > 180;

-- con union all muestra repetidos

select fabcod || prodcod "Codi Producte"
from producto
where precio + precio * 0.21 > 20 
union all
select fabcod || prodcod
from pedido
where importe + importe * 0.21 > 180
;



-- 3. Obtener los códigos de los representantes que son directores de 
-- oficina y que no han tomado ningún pedido.

select director
from oficina
except
select repcod
from pedido;

select director
from oficina 
where director not in (	select repcod
						from pedido);


-- 4. Mostrar el representante que vende más y el que vende menos.



select nombre, ventas
from repventa
where ventas = (	select max(ventas) -- es un igualen vez de in por 
					from repventa)	   -- que min/max devuelve un unico
union								   -- registro
select nombre, ventas
from repventa
where ventas = (	select min(ventas)
					from repventa);
					
-- con joins

select nombre, ventas
from repventa
where ventas = (	select max(ventas)
					from repventa)
or ventas = (	select min(ventas) 
				from repventa);


-- Cada director de cuantes oficines es director.

select director, nombre,  count(*) "Numero de Oficinas que dirige"
from oficina o join repventa r
	on o.director = r.repcod
group by director, nombre;



-- clients que no han fet mai una comanda
select cliecod
from cliente
except
select cliecod
from pedido;


-- F. Ejercicios Extra
-- 1. Obtener una lista con los nombres de las oficinas en las que ningún repre-
-- sentante haya tomado pedidos de productos del fabricante BIC. 



select ciudad
from oficina 
where ofinum not in (	select ofinum
						from repventa r
							join pedido p
								on p.repcod = r.repcod
						where lower(fabcod) = 'bic');


-- con algebraica

select ciudad
from oficina
except
select ciudad
from oficina o
	right join repventa r
		on r.ofinum = o.ofinum
	left join pedido p
		on r.repcod = p.repcod
where lower(fabcod) = 'bic';


-- joc de proves
select ciudad
from oficina
where ciudad not in (	select	ciudad
						from oficina);
					--
					
					select ciudad
from oficina
where ciudad not in ('Denver', 'New Yokr');

--

select ciudad
from oficina
where ciudad not in (   select ciudad
                        from oficina o
                                join repventa r
                                        on r.ofinum = o.ofinum
                                join pedido p
                                        on p.repcod = r.repcod
                        where lower(fabcod) = 'aci');

select ciudad
from oficina
except
select ciudad
from oficina o
		join repventa r
				on r.ofinum = o.ofinum
		join pedido p
				on p.repcod = r.repcod
where lower(fabcod) = 'aci';

-- 2. Obtener los nombre de los clientes que han solicitado pedidos a 
-- representantes de oficinas que venden a la región Oeste o que fueron 
-- contactados por primera vez por los directores de dichas oficinas. 

select distinct cl.nombre "Nombre cleientes"
from cliente cl join pedido p
		on p.cliecod = cl.cliecod
	join repventa r
		on p.repcod = r.repcod
where r.ofinum in (	select ofinum
					from oficina
					where lower(region) = 'oeste')
or cl.repcod in (	select director
					from oficina
					where lower(region) = 'oeste');
-- join

select distinct cl.nombre "Nombre cleientes"
from cliente cl join pedido p
		on p.cliecod = cl.cliecod
	join repventa r
		on p.repcod = r.repcod
	join oficina o
		on r.ofinum = o.ofinum
where lower(region) = 'oeste'
or cl.repcod = director and lower(o.region) = 'oeste';


-- ejemplo parte dos
select cl.nombre "Nombre cleientes"
from cliente cl
	join oficina o
		on cl.repcod = director
where lower(o.region) = 'oeste';


-- parte 1
select cl.nombre "nombre clientes"
from cliente cl 
	join repventa r
		on cl.repcod = r.repcod
	join oficina o 
		on r.ofinum = o.ofinum
where lower(region) = 'oeste';


-- union
select cl.nombre "Nombre cleientes"
from cliente cl
	join oficina o
		on cl.repcod = director
where lower(o.region) = 'oeste'
union
select cl.nombre "nombre clientes"   ---- Mira la diferencia
from pedido p 						--- este esta bien
	join repventa r					--- lo tuyo mal
		on p.repcod = r.repcod
	join oficina o
		on o.ofinum = r.ofinum
	join cliente cl 
		on cl.cliecod = p.cliecod
where lower(region) = 'oeste';



<<<<<<< HEAD

---



select cl.nombre "Nombre cleientes"
from cliente cl
	join oficina o
		on cl.repcod = director
where lower(o.region) = 'oeste'
union
select cl.nombre "nombre clientes"   
from pedido p 						
	join repventa r
		on p.repcod = r.repcod
	join oficina o
		on o.ofinum = r.ofinum
	join cliente cl 
		on cl.cliecod = p.cliecod
where lower(region) = 'oeste';



----

select cl.nombre
from cliente cl
	join oficina o
		on cl.repcod = o.director
where lower(region) = 'oeste'
union
select cl.nombre
from cliente cl
	join pedido p
		on p.cliecod = cl.cliecod
	join repventa r
		on p.repcod = r.repcod
	join oficina o
		on r.ofinum = o.ofinum
where lower(region) = 'oeste';



	

=======
select cl.nombre
from cliente cl
where cliecod in (	select cliecod
					from cliente cli
					
>>>>>>> 21781a1e2001534e7ffddb607f0a52d20b109fcc

-- 3. Obtener los nombres de los clientes que sólo han
-- hecho pedidos al representante que contactó con ellos la primera vez. 

select distinct cl.nombre, cliecod, repcod
from cliente cl
where cliecod not in (	select cliecod
						from pedido
						where repcod != cl.repcod
						and cliecod = cl.cliecod)
and cliecod in (	select cliecod
					from pedido);


-- not in
select nombre
from cliente 
where cliecod not in (	select cli.cliecod 
						from cliente cli
							join pedido pe
								on cli.cliecod = pe.cliecod
						where pe.repcod != cli.repcod)
and cliecod in (	select cliecod
					from pedido);

-- aljebraica
select cl.nombre
from cliente cl 
	join pedido p
		on p.cliecod = cl.cliecod
except
select cli.nombre 
from cliente cli
	join pedido pe
		on cli.cliecod = pe.cliecod
where pe.repcod != cli.repcod;

-- 4. Obtener
-- los nombres de los clientes que han solicitado todos sus pedidos a representantes
-- que pertenecen a la misma oficina. 


select distinct cl.nombre, cl.cliecod
from cliente cl 
	join pedido p
		on p.cliecod = cl.cliecod
	join repventa r
		on p.repcod = r.repcod
where r.ofinum not in (	select ofinum
						from repventa re
							join pedido pe
								on pe.repcod = re.repcod
						where pe.cliecod = p.cliecod
						and r.ofinum != re.ofinum); -- por que no chuta?
						
-- este chuta						
select distinct cl.nombre, cl.cliecod
from cliente cl 
	join pedido p
		on p.cliecod = cl.cliecod
	join repventa r
		on p.repcod = r.repcod
where cl.cliecod not in (	select cliecod
							from pedido pe
								join repventa re
									on pe.repcod = re.repcod
							where re.ofinum != r.ofinum
							and pe.cliecod = cl.cliecod);

-- condicion insert de prueva

insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
values (2,current_date,2102,105,'aci','4100z',1,30.50 )

-- reviewed
select distinct cl.nombre
from cliente cl
        join pedido p
                on p.cliecod = cl.cliecod
        join repventa r
                on p.repcod = r.repcod
where cl.cliecod not in (       select pe.cliecod
                                from pedido pe
                                        join repventa re
                                                on pe.repcod = re.repcod
                                where pe.cliecod = p.cliecod
                                and re.ofinum != r.ofinum) order by 1;
                           -- provar de hacer con not exist


-- 5. Obtener para cada oficina la cantidad de
-- unidades vendidas por sus representantes de productos del fabricante ACI ( de
-- las oficinas se muestra el nombre). 




select 	coalesce(ciudad, 'sin oficina'), 
	    sum(p.importe) "total ventas fabricante aci por oficina"
from oficina o
	right join repventa r
		on r.ofinum = o.ofinum
	join pedido p
		on p.repcod = r.repcod
where lower(fabcod) = 'aci'
group by ciudad;


-- si queremos sacar todas las oficinas, y sudare de lo de aci

select  coalesce(ciudad, 'sin oficina'),
            sum(p.importe) "total ventas fabricante aci por oficina"
from oficina o
        full outer join repventa r
                on r.ofinum = o.ofinum
        join pedido p
                on p.repcod = r.repcod
group by 1;


-- 6. Mostrar una lista con los nombres de los
-- representantes junto con los nombres se sus directores. Si un representante no
-- tiene director, también ha de aparecer en la lista (evidentemente, a su lado no
-- aparecerá ningún nombre).

select 	r.nombre "Vendedor",
		d.nombre "Director"
from repventa r
	left join oficina o
		on r.ofinum = o.ofinum
	left join repventa d
		on o.director = d.repcod
where r.nombre != d.nombre;
--^ que salga el null v

select  r.nombre empleado,
        coalesce(d.nombre, 'sin jefe') jefe
from repventa r
        left join oficina o
                on r.ofinum= o.ofinum
        left join repventa d
                on o.director = d.repcod
where r.nombre != coalesce(d.nombre,'');


-- si es jefe:

select  r.nombre empleado,
        coalesce(j.nombre, 'sin jefe') jefe
from repventa r
        left join repventa j
                on r.jefe = j.repcod;


-- Exercici: vista representant, mostrara: nom, nom del seu cap, numero 
-- de comandes que ha fet, numero del client amb el que va contactar la 
-- primera vegada.  

-- en teoria esta chuta
create or replace view empleat as
select 	e.nombre "Nombre empleado", 
		j.nombre "Nombre jefe de empleado", 
		count(*) "Numero de pedidos",
		(select count(*)
		from cliente
		where repcod = e.repcod) "clientes contactados"
from repventa e
	left join repventa j
		on e.jefe = j.repcod
	left join pedido p
		on p.repcod = e.repcod
group by e.nombre, j.nombre, e.repcod
 order by 1;
 

-- corrección:


create or replace view empleat as
select 	e.nombre "Nombre empleado", 
		coalesce(j.nombre, 'Sin jefe') "Nombre jefe de empleado",  -- atención al coalesce
		count(p.*) "Numero de pedidos",         -- atención count * filas pedidos mas pedidos vacios de representante por join, count p.* solo filas de pedidos sin null
		(select count(*)
		from cliente
		where repcod = e.repcod) "clientes contactados"
from repventa e
	left join repventa j
		on e.jefe = j.repcod
	left join pedido p
		on p.repcod = e.repcod
group by e.nombre, j.nombre, e.repcod
 order by 1;


---
-- tabla temporal

select 	e.nombre empleado,
		coalesce(j.nombre, 'Sin jefe') jefe,
		count(p.*) numPedidos,
        clients clientesContactados       
from repventa e
	left join repventa j
		on e.jefe = j.repcod
	left join pedido p
		on p.repcod = e.repcod
  left join (select repcod, count(*) clients
  from cliente
  group by repcod) cli  on cli.repcod = e.repcod
group by e.nombre, j.nombre, cli.clients
 order by 1;

 
 -- hacer ejercicios de scott
 
 
 -- scott
 -- mostrar el nomb de l'empleat i els anys que porta trevallant
 -- Ampliació, lo mateix pero mostrar anys mesos i dies (ps no es pot fer servir cap funció)
 
 select	ename, 
		(current_date - hiredate)::int / 365 "anys que porta trevallant" 
from emp;

-- ampliació  
 
select	ename "nom", 
		(current_date - hiredate)::int / 365  ||' anys ' ||
		(current_date - hiredate)::int % 365 / 30|| ' mesos ' ||
		 (current_date - hiredate)::int % 365 % 30 ||' dies' "Temps Trevallat"
from emp;



CASE
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
    WHEN conditionN THEN resultN
    ELSE result
END;



select	ename "nom", 
		(current_date - hiredate)::int / 365  ||' anys ' ||
		case
			when ((current_date - hiredate)::int % 365 / 30) = 0
				then '' 
			else (current_date - hiredate)::int % 365 / 30|| ' mesos ' 
		end ||
		(current_date - hiredate)::int % 365 % 30 ||' dies' "Temps Trevallat"
from emp;



--Vui el nom els anys els mesos i els dies.



