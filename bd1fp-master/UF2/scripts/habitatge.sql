\c habitatge
\echo "Borrando tablas."
drop table if exists vivendaxpersona ;
drop table if exists persona ;
drop table if exists vivenda ;
drop table if exists municipi;
\c template1
\echo "Borrando base de datos."
drop database if exists habitatge;
create database habitatge;
\c habitatge


\echo "Creando tablas."
--
-- Name: municipi; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--

CREATE TABLE municipi (
    idMunicipi int,
    nom varchar(50),
    CONSTRAINT PK_MUNICIPI_IDMUNICIPI PRIMARY KEY(idMunicipi)
);


--
-- Name: vivenda; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--


CREATE TABLE vivenda (
    idCatastre int,
    direcio varchar(150),
    m2 numeric(6,2),
    municipi int,
    CONSTRAINT PK_VIVENDA_IDCATASTRE PRIMARY KEY(idCatastre),
    CONSTRAINT FK_VIVENDA_MUNICIPI FOREIGN KEY(municipi) REFERENCES municipi(idMunicipi)
		on delete set null
		on update cascade
);
 
 
--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--


CREATE TABLE persona (
    dni varchar(9),
    nom varchar(50),
    cognom varchar(100),
    email varchar(125),
    telefon varchar(12),
    vivenda int,
    capFamilia varchar(9),
    CONSTRAINT PK_PERSONA_DNI PRIMARY KEY(dni),
    CONSTRAINT FK_PERSONA_CATASTRE FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on delete set null
		on update cascade
    ,
    CONSTRAINT FK_PERSONA_CAPFAMILIA FOREIGN KEY(capFamilia) REFERENCES persona(dni)
		on update cascade
		on delete set null
);
/* FK reflexiva puede dar error, pues referencias una clave que no esta creada, esto se puentea con un alter table, ejemplo:
ALTER TABLE  emp add constraint emp_deptno_fk foreign key (deptno) references dept;  
ALTER TABLE  emp add constraint emp_mgr_fk foreign key (mgr) references emp;
 */ 
 
--
-- Name: vivendaXpersona; Type: TABLE; Schema: public; Owner: postgres; Tablespace:
--


CREATE TABLE vivendaXpersona (
    vivenda int,
    persona varchar(9),
    CONSTRAINT PK_VIVENDAXPERSONA_VP PRIMARY KEY(vivenda,persona),
    CONSTRAINT FK_VIVENDAXPERSONA_VIVENDA FOREIGN KEY(vivenda) REFERENCES vivenda(idCatastre)
		on update cascade
		on delete cascade
    ,
    CONSTRAINT FK_VIVENDAXPERSONA_PERSONA FOREIGN KEY(persona) REFERENCES persona(dni)
		on update cascade
		on delete cascade
);

\echo "Creando sequence."

create sequence seq_idcatastre
start with 1 increment by 1;


\echo "Realizando inserts."
--
-- insert municipi
--
INSERT INTO municipi 
VALUES(1, 'Barcelona');
INSERT INTO municipi 
VALUES(2, 'Madrid');
--
-- insert vivenda
--

\echo "Realizando inserts con sequence."

--
-- inicializando sequence
--

INSERT INTO vivenda 
VALUES(nextval('seq_idCatastre'), 'C/ Santiago nº 78 7º 3ª', 60.50, 1);


--
-- aplicando valor actual de sequence
--
INSERT INTO persona 
VALUES('13361682F', 'Jorge', 'Hurdler Durdler', 'hurlerdurler@gmail.com', '+34748334275', currval('seq_idCatastre'), NULL);
INSERT INTO persona 
VALUES('83764926K', 'Maria', 'Mangla Hangla', 'mmangla@gmail.com', '+34775499375', currval('seq_idCatastre'), '13361682F');
INSERT INTO persona 
VALUES('18573963J', 'Fernando', 'Hurler Mangla', 'fhurler@gmail.com', '+34748465285', currval('seq_idCatastre'), '13361682F');

INSERT INTO vivendaXpersona 
VALUES(currval('seq_idCatastre'), '13361682F');
INSERT INTO vivendaXpersona 
VALUES(currval('seq_idCatastre'), '83764926K');
INSERT INTO vivendaXpersona 
VALUES(currval('seq_idCatastre'), '18573963J');

--
-- incrementando sequence
--
INSERT INTO vivenda 
VALUES(nextval('seq_idCatastre'), 'C/ San Jose nº 23 1º 2ª', 90, 2);

--
-- aplicando valor actual de sequence
--

INSERT INTO persona 
VALUES('23684728H', 'Manuel', 'Perez Mendez', 'mperez@gmail.com', '+34647337274', currval('seq_idCatastre'), NULL);


INSERT INTO vivendaXpersona 
VALUES(currval('seq_idCatastre'), '23684728H');


\echo "Comprovando resultados de sequence."

select * 
from vivenda;

select * 
from persona;

\echo "Mostrando tabla."

select * from vivendaXpersona;

\echo "Realizando delete."

delete from vivendaXpersona
where vivenda = 2;

\echo "Mostrando resultado delete."

select * from vivendaXpersona;

\echo "Restaurando valor."

INSERT INTO vivendaXpersona 
VALUES(2, '23684728H');

\echo "Realizando update."

insert into vivenda
values (nextval('seq_idCatastre'), 'C/ Jose Nuñez nº 23 1º 2ª', 150.35, 2);

select * from vivendaXpersona;

update vivendaxpersona
set vivenda = 3
where vivenda = 2
;

select * from vivendaXpersona;

/*
Juego de pruevas de update/delete CAS
*/
select * 
from municipi 
;
select *
from vivenda
;
select *
from persona 
;
select *
from vivendaxpersona 
;
------------------------------------------------------------
update municipi
set idmunicipi = 7
where idmunicipi= 1
;
select * 
from municipi 
;
select *
from vivenda
;
delete from municipi 
where idmunicipi = 7
;
select * 
from municipi 
;
select *
from vivenda
;

























