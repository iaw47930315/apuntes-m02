/*
HIAW1
iaw14270791
Jacint Iglesias
Base de Dades
UF2
exercicisScott.sql
DB: scott
*/
\i taulesScott.sql
\c scott
/*
1. Inserir en la taula DEPT la informació corresponent a un nou departament
 de consultoria, de codi 50 i que estigui ubicat a SANTANDER. Per a l'ocasió 
 creeu la seqncia deptno_seq (la qual començarà en 50 i s'incrementarà de 10
 en 10) i la utilitzeu per la sentència SQL. 
*/
create sequence deptno_seq
start with 50 increment by 10;

select * 
from dept
;
insert into dept
values 
		(nextval('deptno_seq'), 'MARKETING', 'SANTANDER')
;

select * 
from dept
;
/*
2. Donar d'alta a un nou empleat de n
om Andreu, que exercirà el lloc de
analyst en el departament 30 i el seu cap serà l'empleat 8200. De moment
es desconeixen els altres valors dels camps.
*/
select * 
from emp
;
insert into emp (empno, ename) 
values		
		(8200, 'FRANCISCO')
;
insert into emp (empno, ename, job, deptno, mgr)
values 
		(7935, upper('andreu'), upper('analyst'), 30, 8200)
;

select * 
from emp
;
-- 3. Canviar la date de l'empleat SCOTT per la d'avui. 

select empno "Codi d'empleat", ename "Nom", hiredate "Data de contractació" 
from emp
where lower(ename) = 'scott'
;
update emp
set		hiredate = current_date
where lower(ename) = 'scott'
;

select empno "Codi d'empleat", ename "Nom", hiredate "Data de contractació" 
from emp
where lower(ename) = 'scott'
;
/*
4. L'empleat MILLER, a cause de els seus èxits és ascendit al lloc de 
analys, augmentant-seu salari en un 20%, se li canvia al departament 30 
i el seu nou cap serà l'empleat 7566.
*/

update emp
set		
		job = 'ANALYST',
		sal = sal + sal * 20 / 100,
		deptno = 30,
		mgr = 7566				
where lower(ename) = 'miller'
;
/*
5. Arran de la signatura d'el conveni anual de l'empresa, s'ha determinat 
incrementar el salari de tots els empleat en un 6%.
*/

select *
from emp;
update emp
set		
		sal = sal + sal * 6 / 100
;
select *
from emp;

--6. L'empleat JAMES causa baixa a l'empresa.

select * 
from emp
;
delete 
from emp
where lower(ename) = 'james'
;
select * 
from emp
;
/*
7. Es contracta a SANZ, amb número 1657, per al departament 30 i amb sou 
3000.
*/

insert into emp (ename, empno, deptno,sal)
values 			('SANZ', 1657, 30, 3000)
;
select * 
from emp
where empno = 1657
;
--8. SANZ canvia al departament 40.

update emp
set deptno = 40
where ename = 'SANZ'
;
select * 
from emp
where ename = 'SANZ'
;
--9. SANZ treballa de venedor, amb una comissió de 4000.

update emp
set job = 'SALESMAN',
comm = 4000
where ename = 'SANZ'
;
/*
10. Es decideix augmentar les comissions. Augmenten totes les 
comissions en un 20% de l'salari.
*/

select * 
from emp
;
update emp
set comm = coalesce(comm, 0) + sal * 20 / 100 
;
select * 
from emp
order by ename
;
/*
10.bis. Es decideix augmentar el salari un 50% de la comissió.
*/
update emp
set sal = sal + coalesce(comm, 0) * .5 
;
select *
from emp
;
/*
11. Es decideix augmentar un 35% el salari als empleats que guanyin 
menys que SANZ
*/

update emp
set sal = sal + sal * 35 / 100
where sal < 
			(
			select sal
			from emp
			where ename = 'SANZ'
			)
;
select * 
from emp
order by ename
;
/*
12. S'acomiada a SANZ
*/

delete from
emp
where ename = 'SANZ'
;
select * 
from emp
order by ename
;
/*
13. El departament 30 desapareix. S'han d'acomiadar a tots els 
treballadors que hi treballen. Escriviu les sentències SQL encessàries 
per portar-lo a terme
*/
delete from
emp
where deptno = 30
;
select * 
from emp
order by deptno
;
delete from
dept
where deptno = 30
;
select *
from dept
;
/*
13.B. 
13. El departament 30 desapareix.Els treballadors de moment no 
s'assignen a cap departament. Escribim les sentencies necessaries.
*/
update emp
set deptno = NULL
where deptno = 30
;
select * 
from emp
order by deptno
;
delete from
dept
where deptno = 30
;
select *
from dept
;
/*
14. L'empresa està en crisi i ha d'acomiadar a tots els que treballen
al departament ACCOUNTING.
*/
select e.*
from
emp e
inner join emp j
on e.mgr = j.empno
where j.deptno = 10
;
/*
select e.*
from emp e
outer left join emp j
	on e.mgr = j.empno
inner join dept d
	on j.deptno = d.deptno
where lower(d.dname) = 'accounting'
;
*/
update emp
set 
	mgr = NULL
where empno in 
		(
		select e.empno
		from
		emp e
		inner join emp j
		on e.mgr = j.empno
		where j.deptno = 10
		)
;
select e.*
from emp e
 left join emp j
on e.mgr = j.empno
where j.deptno = 10
;
select * from 
emp;
delete from 
emp e
using dept d 
where e.deptno = d.deptno AND lower(dname) = 'accounting'
;
select * 
from emp;
/*
Consultes bàsiques
*/
\c template1
\i taulesScott.sql
/*
15. Mostreu el codi d'empleat, salari, comissió, nº de departament i 
data de la taula EMP.
*/
--
select empno "Exercici 15."
from emp
where 0 = 1
;
--
select 
	empno,
	sal,
	comm,
	deptno,
	hiredate
from emp
;
/*
16. Mostra tota la informació de la taula departaments
*/
--
select empno "Exercici 16."
from emp
where 0 = 1
;
--
select *
from dept
;
/*
17. Mostreu el nom i l'ocupació d'aquells que siguin salesman.
*/
--
select empno "Exercici 17."
from emp
where 0 = 1
;
--
select
	ename,
	job
from emp
where lower(job) = 'salesman'
;
/*
18. Mostreu el nom i el codi de departament d'aquells empleats que no 
treballen en el departament 30
*/
--
select empno "Exercici 18."
from emp
where 0 = 1
;
--
select
	ename,
	empno,
	deptno
from emp
where deptno != 30
order by 3
;
/*
19. Mostreu els empleats que treballen en els departaments 10 y 20 
(es vol les dues solucions).
*/
--
select empno "Exercici 19."
from emp
where 0 = 1
;
--
select 
	ename,
	deptno
from emp
where deptno = 10 or deptno = 20
order by 2
;
select 
	ename,
	deptno
from emp
where deptno in(10,20)
order by 2
;
/*
20. Mostreu el nom i salari d'aquells empleats que guanyin més de 2000
*/
--
select empno "Exercici 20."
from emp
where 0 = 1
;
--
select
	ename,
	sal
from emp
where sal > 2000
order by 2
;

--20.Mostreu el nom i salari d’aquells empleats que guanyin més de 2000.

select ename,sal
from emp
where sal > 2000;

--Exercicis Extra:
--Mostreu els empleats que cobren entree 2000 y 4000.

select * from emp where sal >= 2000 and sal <= 4000;

select * from emp where sal between 2000 and 4000;

--21.Mostreu el nom i la data de contractació d’aquells
--empleats que hagin entrat abans de l’1/1/82.

select ename, hiredate
from emp
where hiredate < date '1/1/82' -- NO ES ESTANDARD
;
select ename, hiredate
from emp
where to_char(hiredate, 'yyyy-mm-dd') < '1982-01-01'
;
--22.Mostreu el nom dels salesmans que guanyin més de 1500.
select ename, job, sal
from emp
where sal > 1500
;
--23.Mostreu el nom d’aquells que siguin ‘Clerk’ o treballin en el departament 30.
select 
	ename,
	job, 
	deptno
from emp
where lower(job) = 'clerk'	or deptno = 30
;
--24.Mostreu aquells que es diguin ‘SMITH’, ‘ALLEN’ o ‘SCOTT’.

select *
from emp
where lower(ename) = 'smith' or lower(ename) = 'allen' 
		or lower(ename) = 'scott'
;
select *
from emp
where lower(ename) IN ('smith' , 'allen', 'scott')
;
--25.Mostreu aquells que no es diguin ‘SMITH’, ‘ALLEN’ o ‘SCOTT’.
select 
	ename
from emp
where lower(ename) NOT IN ('smith', 'allen', 'scott')
order by 1
;
--26.Mostreu aquells el salari estigui entre 2000 i 3000.
select *
from emp
where sal between 2000 and 3000
;
--27.Mostreu aquells empleats que treballin en el departament 10 o 20.
select *
from emp
where deptno in (10,20)
order by deptno
;
--28.Mostreu aquells empleats el nom comenci per ‘A’.
select ename
from emp
where ename like 'A%'
;
--29.Mostreu aquells empleats el nom tingui com a segona lletra una “D”.
select ename
from emp
where ename like '_D%'
;
--30.Mostreu els diferents departaments que hi ha a la taula EMP.
select distinct deptno
from emp
order by 1
;
--31.Mostreu el departament i el treball dels empleats (evitant repeticions).
 
select distinct job, deptno
from emp
group by 1,2
order by 1
;
/*
    job    | deptno 
-----------+--------
 ANALYST   |     20
 CLERK     |     10
 CLERK     |     20
 CLERK     |     30
 MANAGER   |     10
 MANAGER   |     20
 MANAGER   |     30
 PRESIDENT |     10
 SALESMAN  |     30
(9 rows)

			????????
			
select distinct e.job, distinct d.deptno
from emp em
join emp de
group by 1,2
order by 1
;
ERROR:  syntax error at or near "distinct"
LINE 1: select distinct e.job, distinct d.deptno
                               ^
*/
 
--32.Mostreu aquells empleats que hagin entrat el 1981.
select 
	ename,
	hiredate
from emp
where to_char(hiredate, 'yyyy') = '1981'
;

--33.Mostreu aquells empleats que tenen comissió, mostrant nom i comissió.
select 
	ename,
	comm
from emp
where comm IS NOT NULL
;

--34.Mostreu aquells empleats que guanyin més de 1500, ordenats per ocupació.
select * 
from emp
where sal > 1500
order by job
;

--35.Calcular el salari anual a percebre per cada empleat (tingueu en compte 14 pagues).
select ename, sal * 14 "Salari anual"
from emp
order by 2 desc
;


--36.Mostreu el nom de l’empleat, el salari i l’increment de l’15% de l’salari.
select ename, sal,  sal * 0.15 "Increment del 15%"
from emp
order by 3 desc
;
--37.Mostreu el nom de l’empleat, el salari, l’increment de l’15% de l’salari i el salari augmentat un 15%.
select ename, sal,  sal * 0.15 "Increment del 15%", sal + sal * 0.15 "Salari amb bonus 15%"
from emp
order by 4 desc
;
