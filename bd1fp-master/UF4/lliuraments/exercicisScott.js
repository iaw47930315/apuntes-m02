-- jacint iglesias casanova
-- iaw14270791
-- HIAW1
-- BASE DE DADES
-- UF4
-- Exercicis MongoDB

/*1. Mostreu el codi d'empleat, salari, comissió, nº de departament de cada empleat.
em*/

db.emp.find({},{empno:1,sal:1,"dept.deptno":1}).pretty()



/*2. Mostreu el nom i l'ocupació d'aquells que siguin salesman.*/

db.emp.find({job:"SALESMAN"},{ename:1,job:1}).pretty()



/*--3. Mostreu el nom i el codi de departament d'aquells empleats que no treballen en el departament 30.*/

db.emp.find({"dept.deptno": { $not:{$eq:30} }},{ename:1,"dept.deptno":1}).pretty()



/*--4. Mostreu els empleats que treballen en els departaments 10 y 20*/

db.emp.find({"dept.deptno":{$in:[10,20]}}).pretty()



/*--4. Mostreu el nom i salari d'aquells empleats que guanyin més de 2000.*/

db.emp.find({sal: {$gt:2000}},{ename:1,sal:1}).pretty()




/*--5. Mostreu el nom dels salesmans que guanyin més de 1500.*/

db.emp.find({sal: {$gt:1500},job:"SALESMAN"},{ename:1,job:1,sal:1}).pretty()





/*--6. Mostreu el nom d'aquells que siguin 'Clerk' o treballin en el departament 30.*/

db.emp.find({$or: [ { job:"CLERK"}, {"dept.deptno": 30 } ]} ,{ename:1}).pretty()


/*--7. Mostreu aquells que es diguin 'SMITH', 'ALLEN' o 'SCOTT'.*/


db.emp.find({ename: {$in:["SMITH","ALLEN","SCOTT"]}} ,{ename:1}).pretty()



/*--8. Mostreu aquells el salari estigui entre 2000 i 3000.*/

db.emp.find( {sal: { $gt: 1999, $lt:3001} } ,{}).pretty()



/*--9. Mostreu aquells que no es diguin 'SMITH', 'ALLEN' o 'SCOTT'.*/

db.emp.find({ename: {$not : {$in:["SMITH","ALLEN","SCOTT"] } }} ,{}).pretty()



/*--10. Mostreu aquells empleats el nom comenci per 'A'.*/

db.emp.find({ename: /^A/} ).pretty()


/*--11. Mostreu aquells empleats que treballin en el departament 10 o 20.*/
db.emp.find({"dept.deptno": {$in:[20,10] } } ).pretty()




/*--12. Mostreu aquells empleats el nom tingui com a segona lletra una "D".*/

db.emp.find({ename: /^.D.*/},{ename:1}).pretty()



/*--13. Llisteu (sense repetits) els departaments on treballen els empleats.*/

db.emp.distinct("dept.deptno");



/*--14. Mostreu aquells empleats que hagin entrat el 1981.*/

db.emp.find({ $year: hiredate} ,{hiredate:1}).pretty()

db.emp.find( {
	hiredate:
		{
		$gt: new Date("1980-30-12T00:00:00.000Z"),
		$lt : new Date("1982-01-01T00:00:00.000Z")
		}

},{hiredate:1}).pretty()




/*--15. Mostreu aquells empleats que tenen comissió, mostrant nom i comissió.*/

db.emp.find( { comm : {$ne:null} },{ename:1,comm:1} ).pretty()




/*--16. Mostreu aquells empleats que guanyin més de 1500, ordenats per ocupació.*/


db.emp.find( { sal : {$gt:1500} },{} ).pretty().sort({job:1})




/*--17. Calcular el salari anual a percebre per cada empleat (tingueu en compte 14 pagues).*/

db.emp.aggregate(    [      { "$project": 
	{ename:1,sal:1,"Salari anual": { "$multiply": ["$sal", 14 ] } } 
	}    ] ).pretty()






/*--18. Mostreu el nom de l'empleat, el salari i l'increment de l'15% de l'salari.*/

db.emp.aggregate(    [      { "$project": 
	{ename:1,sal:1,"Increment salarial": { "$multiply": ["$sal", 0.15] } } 
	}    ] ).pretty()




/*--19. Mostreu el nom de l'empleat, el salari, l'increment de l'15% de l'salari i el salari augmentat un 15%.*/



db.emp.aggregate(    [      { "$project": 
	{ename:1,sal:1,"Increment salarial": { "$multiply": ["$sal", 0.15] }
	,"Salari incrementat": { "$multiply": ["$sal", 1.15] }
	 } 
	}    ] ).pretty()




/*--20. Mostreu el nom i la data de contractació d'aquells empleats que hagin entrat abans de l'1/1/82.*/

db.emp.find(
			{hiredate: {$lt: new Date("1982-01-01T00:00:00.000Z")}},
			{ename:1,hiredate:1}
			).pretty()

