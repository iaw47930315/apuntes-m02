# Enunciat.

## 1.CINÈFIL

Un cinèfil aficionat a la informàtica vol crear una Base de Dades que reculli informació diversa sobre el món cinematogràfic, des dels orígens del cinema fins a avui mateix, amb el contingut que es descriu a continuació.
Lògicament, vol tenir classificades moltes pel.lícules, que vindran identificades per un codi. També vol de cadascuna el nom, l’any de l’estrena, el pressupost, el director, etc. A més, de cada pel.lícula vol conéixer també quins actors van intervenir, així com el paper que hi representàven (actor principal, secundari, etc.) i el possible premi que va rebre per la seva interpretació.
Les pel.lícules són d’un tema determinat. Es ben sabut que hi ha actors especialitzats en un tema, encara que un actor és capaç d’interpretar varis temes amb diferent “habilitat”.
Com que el nostre cinèfil és una mica curiós, vol emmagatzemar també dades personals dels actors, que ha anat recollint al llegir revistes del món artístic. Per exemple, quins actors són en certa manera substitutius d’altres, amb un grau de possible substitució que pot anar de 1 a 10. També quins actors són “incompatibles”, o sigui, que mai han treballat ni treballaran junts amb una mateixa pel.lícula o escena.
Els actors estan contractats, en un moment donat per una companyia, però poden canviar si tenen una oferta millor. També poden retornar a una companyia en la que ja hi  havien treballat. Les companyies produeixen pel.lícules, però cap pel.lícula és coproduïda per dues o més companyies.
Com que el nostre amic fa molt de turisme, vol saber, per a cada ciutat, quines companyies hi tenen representació i a quina adreça. Evidentment, les companyies solen tenir representació a quasi totes les ciutats importants. Al mateix temps, vol també informació de quines pel.lícules s’estan rodant a cada ciutat i en quin moment, tenint en compte que una pel.lícula es pot rodar a vàries ciutats i també a una mateixa ciutat en diferents fases del seu rodatge.

Proposar un esquema entitat-relació adient (identificant clarament entitats, atributs i interrelacions) i un esquema lògic amb el seu DR.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas cinefil](https://drive.google.com/file/d/1lB6-8zfiSbJoUa_EbhIFTLXnSbCoN5UM/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas biblioteca](../img/cinefil.png)
  ![cas biblioteca correcció](../img/cinefilCorrecio.png)
  
  
# 3. Model lògic relacional
## 3.1. Esquema lògic
### Original  

  **Director**(<ins>IdDirector</ins>, nom)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **Tema**(<ins>IdTema</ins>, nom)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **Pelicula**(<ins>IdPelicula</ins>, nom, anyEstrena, *tema*, *companyia*)  
  **Actor**(<ins>IdActor</ins>, nom)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  
  **nivellEspecialitat**(<ins>*actor, tema*</ins>, habilitat, especialitat)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **icnompatibilitat**(<ins>*actor, incompatibleAmb*</ins>,)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **DataOferta**(<ins>data</ins>)  
  **oferta**(<ins>*actor, dataOferta*</ins>, contractat, *companyia*)  
  **Ciutat**(<ins>IdCiutat</ins>, nom)  
  **representacio**(<ins>*companyia, ciutat*</ins>, adreca)  
  **DataRodatge**(<ins>data</ins>)  
  **rodatge**(<ins>*Pelicula, Ciutat, Data*</ins>)  


### Correcció

  **Tema**(<ins>IdTema</ins>, nom)  
  **Pelicula**(<ins>IdPelicula</ins>, nom, anyEstrena, pressupost, *tema*, *companyia*)  
  **Actor**(<ins>IdActor</ins>, nom)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **incompatibilitat**(<ins>*actor, incompatibleAmb*</ins>,)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **contracte**(<ins>*actor, dataContracte*</ins>, *companyia*, dataFi)  
  **Ciutat**(<ins>IdCiutat</ins>, nom)  
  **rodatge**(<ins>*Pelicula, Ciutat, dataInici*</ins>, fase, dataFi)  
  **Director**(<ins>IdDirector</ins>, nom)  
  **PeliculaxDirector**(<ins>*IdDirector, IdPelicula*</ins>)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  

## 3.2. Diagrama referencial

### Original
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Pelicula|tema|Tema
Pelicula|companyia|Companyia
Paper|actor|Actor
Paper|pelicula|Pelicula
nivellEspecialitat|actor|Actor
nivellEspecialitat|tema|Tema
substitutiu|actor|Actor
substitutiu|substitutiuDe|Actor
icnompatibilitat|actor|Actor
icnompatibilitat|incompatibleAmb|Actor
substitutiu|actor|Actor
substitutiu|substitutiuDe|Actor
oferta|actor|Actor
oferta|dataOferta|DataOferta
oferta|companyia|Companyia
representacio|companyia|Companyia
representacio|ciutat|Ciutat
rodatge|Pelicula|Actor
rodatge|Ciutat|Ciutat
rodatge|Data|DataRodatge

### Correcció


# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)



























