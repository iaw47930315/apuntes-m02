# Enunciat.

## 1.Publicitat

Es desitja confeccionar una base de dades que emmagatzemi informació sobre la publicitat actualment emesa en els principals mitjans de comunicació. Hi ha tres tipus de suports publicitaris: televisió, ràdio i premsa escrita. Dels anuncis es coneix l'eslògan que utilitzen. Si l'anunci és televisiu o és una falca radiofònica, es desitja conèixer quants minuts dura, el nom de les cadenes de televisió o emissores de ràdio respectivament que l'emeten i quantes vegades al dia és emès en cada un dels mitjans. Si l'anunci és imprès s'emmagatzemarà el nom i el tiratge de les publicacions. Dels anuncis impresos podran tenir imatge o no. Un anunci pertany a una campanya publicitària que pot incloure altres anuncis. Cada campanya publicitària té un tema (venda d'un producte, promoció del turisme en una determinada zona, ajuda a determinats països, prevenció de malalties, ...) i un pressupost total per a tots els anuncis que inclou la mateixa. Aquesta campanya publicitària la contracta a un anunciant del que coneixem el seu nom i si és institució o empresa.

Finalment es vol contemplar quins dels mitjans audiovisuals (és a dir cadenes de televisió i emissores de ràdio) considerats abans són al seu torn empreses que s'anuncien.

Nota: establiu les claus primàries que considereu oportunes.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas cinefil](https://drive.google.com/file/d/1lB6-8zfiSbJoUa_EbhIFTLXnSbCoN5UM/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas biblioteca](../img/cinefil.png)
  ![cas biblioteca correcció](../img/cinefilCorrecio.png)
  
  
# 3. Model lògic relacional
## 3.1. Esquema lògic
### Original  

  **Director**(<ins>IdDirector</ins>, nom)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **Tema**(<ins>IdTema</ins>, nom)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **Pelicula**(<ins>IdPelicula</ins>, nom, anyEstrena, *tema*, *companyia*)  
  **Actor**(<ins>IdActor</ins>, nom)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  
  **nivellEspecialitat**(<ins>*actor, tema*</ins>, habilitat, especialitat)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **icnompatibilitat**(<ins>*actor, incompatibleAmb*</ins>,)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **DataOferta**(<ins>data</ins>)  
  **oferta**(<ins>*actor, dataOferta*</ins>, contractat, *companyia*)  
  **Ciutat**(<ins>IdCiutat</ins>, nom)  
  **representacio**(<ins>*companyia, ciutat*</ins>, adreca)  
  **DataRodatge**(<ins>data</ins>)  
  **rodatge**(<ins>*Pelicula, Ciutat, Data*</ins>)  


### Correcció

  **Tema**(<ins>IdTema</ins>, nom)  
  **Pelicula**(<ins>IdPelicula</ins>, nom, anyEstrena, pressupost, *tema*, *companyia*)  
  **Actor**(<ins>IdActor</ins>, nom)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  
  **substitutiu**(<ins>*actor, substitutiuDe*</ins>, grauSubstitucio)  
  **incompatibilitat**(<ins>*actor, incompatibleAmb*</ins>,)  
  **Companyia**(<ins>IdCompanyia</ins>, nom)  
  **contracte**(<ins>*actor, dataContracte*</ins>, *companyia*, dataFi)  
  **Ciutat**(<ins>IdCiutat</ins>, nom)  
  **rodatge**(<ins>*Pelicula, Ciutat, dataInici*</ins>, fase, dataFi)  
  **Director**(<ins>IdDirector</ins>, nom)  
  **PeliculaxDirector**(<ins>*IdDirector, IdPelicula*</ins>)  
  **Paper**(<ins>*actor, pelicula*</ins>, paper, premi)  

## 3.2. Diagrama referencial

### Original
Relació referencial|Clau aliena|Relació referida
-|:-:|-
Pelicula|tema|Tema
Pelicula|companyia|Companyia
Paper|actor|Actor
Paper|pelicula|Pelicula
nivellEspecialitat|actor|Actor
nivellEspecialitat|tema|Tema
substitutiu|actor|Actor
substitutiu|substitutiuDe|Actor
icnompatibilitat|actor|Actor
icnompatibilitat|incompatibleAmb|Actor
substitutiu|actor|Actor
substitutiu|substitutiuDe|Actor
oferta|actor|Actor
oferta|dataOferta|DataOferta
oferta|companyia|Companyia
representacio|companyia|Companyia
representacio|ciutat|Ciutat
rodatge|Pelicula|Actor
rodatge|Ciutat|Ciutat
rodatge|Data|DataRodatge

### Correcció


# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)



























