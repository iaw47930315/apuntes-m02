# Enunciat.

## 1.Metro

Construir el model E/R que reflecteixi tota la informació necessària per a la gestió de les línies de metro d'una determinada ciutat. Els supòsits semàntics considerats són els següents:


* Una línia està composta per una sèrie d'estacions. De la línia volem saber el codi (L1, L2, etc) i el nom (Linia vermella, línia morada) i així respetimanet.


* Cada estació pertany almenys a una línia, podent pertànyer a diverses. Un exemple sería  l'estació La Sagrera, on la Linea 1 (vermella) i Linea 5 (línia blava) comparteien aquesta estació.


* De cada estació ens interessa saber segons els sentit de la línia, quina posició ocupa a dintre de la línia. Un exemple: L'estació de Sagrera de la L1 direcció Fondo ocupa el número 23 però la mateixa estació, L1 direcció Besos ocupa la 5.


* Una estació en un moment donat només pertany a una línia, però en el passat ha pogut formar part d'altra línia. Per exemple, donat un moment de la història recenta de Barcelona, va haver-hi un canvi d'estacions entre la lila i la groga. Interessarà saber doncs dataInici i dataFi de quan una estació ha format part d'una línia en concret.


* Cada estació pot tenir diversos accessos, però considerem que un accés només pot pertànyer a una estació.


* Un accés mai podrà canviar d'estació.


* Cada línia té assignats una sèrie de trens, no podent succeir que un tren estigui assignat a més d'una línia, però sí que no estigui assignat a cap (p. Ex., Si es troba en reparació).


* Algunes estacions tenen assignades cotxeres, i cada tren té assignada una cotxera.


* Interessa conèixer tots els accessos de cada línia. De cada accés, a part del codi i nom, voldrem saber si hi té ascensor.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas metro](https://drive.google.com/file/d/1sNgj0IYZui_Fbz-6zOx3lkKYtdp1sYxr/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas metro correció](../img/metrocorrecio.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Estacio**(<ins>IdEstacio</ins>, nomEstacio)  
  **Access**(<ins>idAccess</ins>, nomAccess, ascensor, *IdEstacio*)  
  **Cotxera**(<ins>IdCotxera</ins>, *IdEstacio*)
  **Tren**(<ins>IdTren</ins>, model, *IdCotxera*, *IdLinea*)  
  **Linea**(<ins>IdLinea</ins>, nomLinea)  
  **LineaxEstacio**(<ins>*Estacio, Sentit*</ins>, dataInici, dataFi, *Linea*)  
  
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Access|IdEstacio|Estacio
Cotxera|IdEstacio|Estacio
Tren|IdCotxera|Cotxera
Tren|IdLinea|Linea
LineaxEstacio|Estacio|Estacio
LineaxEstacio|Sentit|Estacio(sentit)
LineaxEstacio|Linea|Linea

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)