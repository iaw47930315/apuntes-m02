# Enunciat.

## 1.Enunciat

Una biblioteca que encara manté manualment la gestió dels prèstecs dels llibres vol informatitzar el procés. Actualment es tenen fitxes de dos tipus:

* Fitxes on es recullen les característiques dels llibres Títol, Autor@/@s, Editorial, Any de publicació,


* Fitxes relatives als préstecs que s'han efectuat, títol del llibre, la persona a la qual se li ha prestat, la data de préstec i la de devolució.

A més d'aquestes fitxes, hem recollit mitjaçant entrevistes amb els usuaris informació que ens caldrà analitzar:

Per als llibres interessa saber, a més del que apareix actualment a les fitxes, l'idioma en què estan escrits.

A més, es desitjarà fer cerques en funció de paraules clau, de manera que podrem trobar un llibre a través de diferents paraules clau. Per exemple, el llibre Sistemes i bases de dades seria un dels llibres que ens sortiria a la consulta per paraula clau "model semàntic".

De cada llibre poden haver-hi diferents exemplars, on cadascú tindrà un codi propi que serà únic i es voldrà també recollir característiques físiques propiciades per l'ús (té la tapa trencada, cal enquadernar, té trencada el gomet anti-robatori, etc.).

Cada llibre tractarà d'un o diversos temes, el que interessa reflectir per poder realitzar consultes del tipus "Llibres o articles que tenim sobre Bases de dades Multimèdia", "Articles que podem consultar sobre el llenguatge QUEL", etc. Els temes es poden dividir en subtemes i així successivament (no se sap en quants nivells). Per exemple, en el tema de DISSENY podem distingir una sèrie de subtemes, com DISSENY CONCEPTUAL, DISSENY LÒGIC, DISSENY FÍSIC, DISSENY GRÀFIC, DISSENY ASSISTIT PER ORDINADOR, etc. Cada tema tindrà un nom i una descripció.

Dels autors, a més del nom, ens interessa conèixer la seva nacionalitat. De les editorials emmagatzemarem el seu nom, adreça, ciutat i país.

A la biblioteca volem distingir tres tipus de socis: alumnes, als que com a màxim se'ls prestarà una obra durant tres dies, alumnes de doctorat o projectes de fi de carrera, que tindran accés com a màxim a dues obres durant la setmana i els professors i altres biblioteques, als quals se'ls deixarà tres obres durant un termini màxim d'un mes.

De cada un d'ells emmagatzemarem seu DNI, Nom, Adreça, Telèfon, Titulació en què estan estudiant (cas de ser estudiants) o impartint classes (cas de ser professors).

Els codis seran els propis del nostre sistema, excepte el DNI i l'ISBN. S'ha de contemplar la major quantitat d'informació possible (el més proper a la realitat).

Proposar un esquema entitat-relació adient, identificant clarament entitats, atributs i interrelacions.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas biblioteca](https://drive.google.com/file/d/1VlLufMXZfmsOjBcNBN9NljV5WGG0xDHj/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas biblioteca](../img/biblioteca2.png)
  
# 3. Model lògic relacional
## 3.1. Esquema lògic

  **Editorial**(<ins>IdEditorial</ins>, nom, adreca, ciutat, pais)  
  **Soci**(<ins>IdSoci</ins>, DNI, nom, adreca, telefon, tipus, maxim, titulacio, materiaImpartida)  
  **Llibre**(<ins>ISBN</ins>, titol, autor, editorial, anyPublicacio, idioma, *IdEditorial*)  
  **Autor**(<ins>IdAutor</ins>, nom, Nacionalitat)  
  **ParaulesClau**(<ins>IdParaula</ins>, paraula)  
  **AutorsxLlibre**(<ins>*IdAutor, ISBN*</ins>)  
  **ParaulesClauxLlibre**<ins>*ISBN, IdParaula*</ins>)  
  **TemaLlibre**(<ins>IdTema</ins>, tema, descripcio, *temaPrincipal*)  
  **TemaLlibrexLlibre**(<ins>*ISBN, IdTema*</ins>)  
  **ExemplarLlibre**(<ins>IdExemplar</ins>, caracteristiques, *ISBN*)  
  **Data**(<ins>dataInici</ins>)   
  **Prestec**(<ins>*idExemplar, dataInici, DNI*</ins>, dataDevolucio)  

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
LlibrexTema|ISBN|Llibre
LlibrexTema|idTema|Tema
Tema|temaPrincipal|Tema
ParaulaxLlibre|idParaula|ParaulaClau
ParaulaxLlibre|ISBN|Llibre
Prestec|idExemplar|Exemplar
Prestec|dataInici|Data
Prestec|DNI|Soci

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)