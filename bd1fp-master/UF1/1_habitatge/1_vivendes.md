# Enunciat.

## 1. Habitatges

Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (requeriments d'usuari) complementaris necessaris.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas habitatge](https://drive.google.com/file/d/182NL6aeMyKptMPutLeVzHJrwLtZ8II76/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas habitatge](../img/habitatge.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  **Municipi**(<ins>IdMunicipi</ins>, nom)  
  **Vivenda**(<ins>IdCatastre</ins>, direccio, m2, *IdMunicipi*)  
  **Persona**(<ins>DNI</ins>, nom, email, telefon, edad, *IdCatastre*, *capFamilia*)  
  **VidaxPersona**(<ins>IdCatastre, DNI</ins>)  
## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Vivenda|IdMunicipi|Municipi
Persona|IdCatastre|Vivenda
VidaxPersona|IdCatastre|Vivenda
VidaxPersona|DNI|Persona
Persona|capFamilia|Persona


# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script habitatge.sql](../../UF2/scripts/habitatge.sql)

