--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- funcions scottEmpleat
-- Jacint Iglesias
-- UF3

\c scott




-- cursor single row single value

drop function codiExemplarDisponible5(varchar);
create or replace function codiExemplarDisponible5(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			join prestec p on e.idexemplar = p.idexemplar
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and datadev is not null
			order by 1;
	v_exemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	close cur_exemplars;
	return coalesce(v_exemplar,0);
end;
$$ language plpgsql;





-- structure case return

v_mensage := 'Todo correcto.';
case
	when nocumpleA then
		...
		v_mensage := 'mi mensage A';
	when nocumpleB then
		...
		v_mensage := 'mi mensage B';
	else
		funcion principal (p.e. update);
end case;
return v_mensage;






-- select with sum and case

SELECT
	SUM (
		CASE rental_rate
		WHEN 0.99 THEN
			1
		ELSE
			0
		END
	) AS "Mass",
	SUM (
		CASE rental_rate
		WHEN 2.99 THEN
			1
		ELSE
			0
		END
	) AS "Economic",
	SUM (
		CASE rental_rate
		WHEN 4.99 THEN
			1
		ELSE
			0
		END
	) AS "Luxury"
FROM
	film;





-- void function
-- called
create or replace function prova()
returns void
as $$
begin
	raise info 'Hola';
end;
$$ language plpgsql;


-- caller
create or replace function prova2()
returns varchar
as $$
declare
	basura varchar(1);
begin
	basura := prova();
	return 'ok';
end;
$$ language plpgsql;





-- blocs anonims

do $$
begin
	raise info e'\nhola\nadeu';s
end; $$;



-- Sequencia a partir de maxim.
create sequence if not exists seq_emp
increment by 1 start with 1; -- can't start with 0

--set sequence default value 
select  setval('seq_emp', (select max(empno) FROM emp)::int); -- provar sin int cast
