# UF3 funcions.    
  

## Optimización de funciones, cursores, etc.  
  
* un solo return
* una sola variable si solo es un campo
* evitar bucles si es un solo registro/variable
* un solo campo, una sola variable simple (integer)
* coalesce para cuando el select no devuelve ningun valor
  
```
drop function codiExemplarDisponible5(varchar);
create or replace function codiExemplarDisponible5(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			join prestec p on e.idexemplar = p.idexemplar
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and datadev is not null
			order by 1;
	v_exemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	close cur_exemplars;
	return coalesce(v_exemplar,0);
end;
$$ language plpgsql;

```
  
## Reducir los parametros al minimo  
  
```
create or replace function retornarDocument(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
-- corrección (parametros minimos)
create or replace function retornarDocument(p_idexemplar prestec.idexemplar%type) -- parametre formal

```
  
 
## Corregir funciones, aplicar/añadir idexemplar o format segun convenga.  
  
## Reutilizar minifunciones.  
  
## Para renovar utilizar la tabla reserva
  
## Utilizar constantes (p.e. numero de dias de reserva)

## utilizar case con else para discriminar condición principal.  
  
```
v_mensage := 'Todo correcto.';
case
	when nocumpleA then
		...
		v_mensage := 'mi mensage A';
	when nocumpleB then
		...
		v_mensage := 'mi mensage B';
	else
		funcion principal (p.e. update);
end case;
return v_mensage;
	
```

## Nota!: un case => if else , es decir que una condición excluye la otra.  

```

```
