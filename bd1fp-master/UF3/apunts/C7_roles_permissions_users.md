# UF3. PostgreSQL permissions.  
  
```
escola=# create user profesor;
CREATE ROLE
escola=# create user tutor;
CREATE ROLE
escola=# create user alumne;
CREATE ROLE

/* ALUMNE */
escola=# create role alumner;
CREATE ROLE

grant select on faltes to alumner;
grant alumner to alumne;
set role alumne;

select * from faltes;
escola=> select * from modulxprofe ;
ERROR:  permission denied for relation modulxprofe

/* PROFE */

create role profesor_role;
grant select,update,insert on faltes to profesor_role;
grant select on modulxprofe,profe to profesor_role;

escola=# grant profesor_role to profesor;

escola=# set role profesor;


escola=> insert into profe values (4,'test');
ERROR:  permission denied for relation profe

escola=> update faltes set hora = current_time where codialumne='1' and idmodul=1;
UPDATE 1

escola=> select * from profe;
-- funciona

/* TUTOR */

set role postgres;
create role tutor_role;
grant select,update,insert,delete on faltes,modulxprofe,profe to tutor_role;
grant tutor_role to tutor;

set role tutor;
escola=> update profe set nom= 'Joan Bibes' where idprof = 1;
UPDATE 1


```