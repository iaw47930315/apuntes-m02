# UF3 repas.  
  
## Tipus de connexions:  
  
* Local
* Remota

## FItxers de configuracio.  
  
Chuleta resum: https://gitlab.com/jandugar/m02-bd/-/blob/master/UF3_PLpgSQL/resum_postgresql.pdf  

NOTA: es necesita usuari ROOT per accedir-hi
      
/var/lib/pgsql/data/postgresql.conf  <= vim  
```
=> listen_addresses = 'localhost' => '*'   <==== cambiar localhost por *
	* uncomment ^
	* port = 5432 => uncomment   
```
  
/var/lib/pgsql/data/pg_hba.conf <= vim  
```
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             postgres                                trust
local   all             all	                                	trust
local   all             all                                     peer
# IPv4 local connections:
host     all             all             127.0.0.1/32           ident
host     all             all             10.200.242.203/32      trust
host     all             all             10.200.242.202/32      trust 
host     all             all             10.200.242.201/32      md5
# IPv6 local connections:
host    all             all             ::1/128                 ident
# Allow replication connections from localhost, by a user with the
# replication privilege.
#local   replication     postgres                                peer
#host    replication     postgres        127.0.0.1/32            ident
#host    replication     postgres        ::1/128                 ident
```

```
ADDRESS: IP o Rang d'IPs que es poden conectar (/24...)
``` 

Nota:  
```
			TYPE			Description												PW
					
ident		host/remote		checkeja que el usuari de OS es igual a usuari BD		no
peer		local			checkeja que el usuari de OS es igual a usuari BD		no
trust																				no
md5																					si
password																			si
reject
```

trust: deixarlo per local, per que per remote confias cegament  
password: password enviat no xifrat	 
md5: password enviat xifrat		
reject: rechazar conexion		
  
Nota: mes restrictiu avans  
  
`# systemctl restart postgresql`
  
## Com veure els usuaris que hi ha a postgres sense saver la BD.  
  
```  
   psql -d template1 -c "select * from  pg_user;"

   psql -d template1 -c "select usename from  pg_user;"
   
   psql -d template1 -c "\du"
   
   psql -d template1 -c "select * from  pg_roles;"
```
    
`pg_user`  
`pg_...` => vistes de sistema de BD, dicionari  
no te que veure amb `tg_op` que es de trigger, variables especials dels 
triggers a nivell de fila  
  

## -U se lo traga todo  
  
## Create user:  

create user username createdb;

create role juan login createdb;
  
pg_roles => mes completa que pg_user
  
## Veure schema  
  
`\dn` 
  
create schema jordi; -- <= nom usuari
  
### Variable de sistema que mostra el ordre de cerca dels esquema  
  
show search_path;
search_path
"$user", public    
  
#### Truco si creas una tabla que ya existe en public, pero lo haces desde
tu usuario y existe un schema con tu nombre de usuario no se qujara
por que la guarda en tu esquema. 

#### Para crear tabla en un esquema determinado  
```
create table public.repventa (id,int); -- <= peta por que repventa ya existe en public
drop schema jordi cascade; -- <= per omitir borrat de taules
```
  
   
  
## Index  
  

  
```
create index if not exists repventa_repcod_index on repventa(repcod);
-- NOTA: un index per camp
create index if not exists repventa_repcod_index on repventa(job);
```           
## Capes ansi/x3/sparc - Arquitectura a tres nivells
* externa . vistes
* conceptual/ logico global .  taules
* interna logicoglobal . index
  
  
  
## Triggers. WHEN  
  
```
CREATE [ CONSTRAINT ] TRIGGER name { BEFORE | AFTER | INSTEAD OF } { event [ OR ... ] }
    ON table_name
    [ FROM referenced_table_name ]
    [ NOT DEFERRABLE | [ DEFERRABLE ] [ INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
    [ FOR [ EACH ] { ROW | STATEMENT } ]
    [ WHEN ( condition ) ]
    EXECUTE PROCEDURE function_name ( arguments )

```
  
### Exemple: bd scott,  
  
Volem un trigger quan un empleat d'un treball en concret fa una operació dml.  
Per asegurarnos que la modificació d'un salari mai sera a la baixa.  
  
```
create or replace function fControlSou()
returns trigger
as $$
begin
	raise exception 'No es permet baixar el salari.';
return new;
end;
$$ language plpgsql;

-- trigger (capçalera)
create trigger tControlSou
before update on emp
for each row 
when (new.sal < old.sal)
execute procedure fControlSou();


update emp set sal=1000 where empno=7902;

update emp set sal=sal *0.75 where deptno = 10;

```  




### Exemple trigger especificant el camp de la taula a chequejar  
  
```
create or replace function fProva()
returns trigger
as $$
begin
	raise info 'Aquest missatge només sortira si modifiquem el camp sal.';
return new;
end;
$$ language plpgsql;


create trigger tControlSou
before update of sal on emp
for each row 
execute procedure fProva();

update emp set deptno = 10 where deptno = 20; -- no s'executa el trigger
```
## Disable trigger:  
  
```
alter table emp disable trigger tcontrolsou;
```
