# UF3  Funcions.   
  
#### Retorno de carro. `E '\n' chr(10)` `E` o `e` de escape string constant. Osea:  
  
```
return e '\n Nueva linea'; -- escape string constant
//igual a bash
echo -e "Hola\nque\ntal"
```
  
#### Cosas nuevas de biblioteca:  
* serial
* inherits inherits (document);
* default
* md5
* `--() { :; }; exec psql template1 -f "$0"` shebang scripts sql

```
`--() { :; }; exec psql template1 -f "$0"`
^ ejecuta el script (a si mismo, $0)
se ejecuta como ./nombreScript.sql
chmod u+x nombreScript
--
chmod u+x taulesBiblioteca.sql
./taulesBiblioteca.sql
```
  
## A partir de ahora todas las entregas deven tener el shebang correcto.  
  
Como deveria ser el shebang para ejecutar las funciones de training (el archivo de funciones sql sobre la bd training)?.   
```
`--() { :; }; exec psql training -f "$0"`
^ el exec esta solo para ejecución como archivo
el \c se deja para ejecución via psql.
```
  
#### Curiosidad: en psql puedes poner `psql training -f nombrefichero.sql`, detecta que el primer parametro es la -d
  
#### Codi sense nom (sense funció): blocs anonims.  
  
```
do $$
--{declare}
begin
	raise notice 'Hola %', 'Jordi';
end $$;
-- respuesta por consola dentro de bd: 
-- NOTICE:  Hola Jordi
-- DO
```
  
### Tipos de raise.  
  
```
do $$
--{declare}
begin
raise info 'Hola %','Jordi';
end $$;
```
Raise info se ejecuta para dar una información como aviso. Ejemplo, (drop table if exist, si no existe te da un raise notice pero podria ser perfectamente un raise info).  
  
## Si en un codigo el profe se encuentra dos selects. Y tengo mi bloque de exception, esta excepción se asociara?  
  
La respuesta es los dos. Por que?  
### Se asocia al select que falle, cualquiera de ellos. No sabremos que select ha llamado a la excepción.  
   
 
  
```
exception
		when no_data_found then 
			return 0;
```

### Como se asocia un select a un exception concreto? Anidando, como? Asi:

```
begin
	begin
		select...
		exception
			...
			return A;
	end;
	begin
		select...
		exception
			...
			return B;
	end;
end;
``` 
  
Osea un `beginc` por cada `select` un `exception` por cada `select` y un `end` por cada `select`.  
  
## Crear .sh, Crear joc de proves EN EL .sh. Osea, en el .sh se llama a "jocDeProves.sql"  
  

## Correcció funcions biblioteca.  
  
```

-- corrección:  
=> titol no id, osea titol from document;
exemplar estat disponible AND not in (esta en prestec (datadev is null))

=> p_titol varchar
drop function codiExemplarDisponible(varchar);
create or replace function codiExemplarDisponible(p_titol varchar)
returns int
declare
	v_idExemplar varchar;
begin
	-- s'obte el codi de l'exemplar
	select idExemplar
	into strict v_idExemplar
	from exemplar e
	inner join document d
	on e.idDocument = d.idDocument
	where lower(titol) = lower (p_titol)
	and lower(estat) = 'disponible'
	and idExemplar not in (	select idExemplar
							from prestec
							where datadev is null)
	limit 1;
	-- ^if no encuentra nada salta a la excepción directamente saltandose todo el resto de codigo.
	-- es comunica que s'ha trobat l'exemplar
	raise notice 'document troba amb id %', v_idExemplar;
	-- es retorna l'exemplar
	return v_idExemplar;
	-- exception cuan no es troba un exemplar 
	exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;
```
^ Returns varchar y hace un casting implicito a int, esta mal, la variable ha de ser int.  
  
```
v_idExemplar varchar;
-- tendria que ser: 
v_idExemplar int;
-- o: 
v_idExemplar exemplar.idexemplar%type;
```  
### disponible no quiere decir que no esta en prestec, sino que puede estar prestado y esta abierto de cara al publico.  
 
#### Otra forma de no utilizar el limit1, estandard SQL.  
^  funcions de grup, agregate functions min(idExemplar).  
```
	select min(idExemplar) -- o max(idExemplar)
	into strict v_idExemplar
	from exemplar e
	inner join document d
	on e.idDocument = d.idDocument
	where lower(titol) = lower (p_titol)
	and lower(estat) = 'disponible'
	and idExemplar not in (	select idExemplar
							from prestec
							where datadev is null)
	;						
``` 
^question de performance, limit 1 mejor que max o min. limit no es standard habiras de adaptarlo a oracle.  
  
##### Autocritica, aprende de los selects curraos y no necesitaras mil funciones.    
    
## Too many rows. Select into (no excepciones => found o not found), into strict => exception
  
# Deveres, prestec de documents.  
  
=> postgresqltutorial.com/plpgsql-cursor/
