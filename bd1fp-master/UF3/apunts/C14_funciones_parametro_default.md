# UF3  Funcions.   
  
### Cuando trabajamos con funciones, si hacemos operaciones que no   
### dependan  de una BD, no hacer un select (6 * 2, no select 6 * 2;)  
  
### En oracle els select sense taula es fan `select 6 * 2 from dual;`, la taula `dual` es un dummy bank.  
  
```
taula dual:
dummy
X
```
  
###  Parametres per defecte en funcions.  
  
```
darrerdiames(p_mes integer default to_char(current_date,'mm')::int)
```
```
control d'errors:
if p_mes > 12 then
	return 'error';
```
  
### En PostgreSQL un `switch` se llama `case`.  
  
  
