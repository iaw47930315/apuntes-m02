# UF3 funcions.  
  
## CASE in plpgsql/sql.  

* https://www.postgresqltutorial.com/postgresql-case/ <= SQL
* https://www.postgresqltutorial.com/plpgsql-case-statement/ <= PL/pgSQL
  
```
-- SQL ------------------------------

CASE 
      WHEN condition_1  THEN result_1
      WHEN condition_2  THEN result_2
      [WHEN ...]
      [ELSE result_n]
END
	
	-- OR 
	
CASE expression
WHEN value_1 THEN
	result_1
WHEN value_2 THEN
	result_2 
[WHEN ...]
ELSE
	result_n
END;



-- PL/pgSQL ------------------------------

CASE search-expression
   WHEN expression_1 [, expression_2, ...] THEN
      when-statements;
  [ ... ]
  [ELSE
      else-statements; ]
END CASE;

```  
### Joc de proves funció prestec.  

* usar coalesce para data desbloqueig, etc.
   * hacer un casting al campo (es tipo date i una cadena de texto no entra por defecto)
```
(...)',coalesce(v_datadesbloqueig::varchar,'data desconeguda');
```
  
* Els passwords tenen encriptació amb md5.
  * `password = md5(p_pass)`
* si hi han 2 consultes, mirar si es poden simplificar en una
```
select idusuari,bloquejat
into strict v_idusuari,v_bloquejat
(...)
```

* si utilizamos mas de 2 campos
```
declare
	v_usuari usuari%rowtype;
(...)
select *
into strict v_usuari
```
  
* si hacemos salida del codigo, no hace falta un else
  
```
if exemplarDisponible() = 0 then
	...
end if;
-- executar resta de codi
```  
  
* usar constant
  
```
maxCDs constant integer := 1; 
``` 
  
* evitar returns (usar los minimos )
  
* hacer el calculo de documentos  de una (haciendo un case sum te ahorras hacer 3 counts distintos)

  
```    
select 
		sum(case when lower(format) = 'cd' then 1 else 0 end) numCds,
		sum(...
		into strict v_cds,v_revistes (...)
		(d'aquest usuari i datadev is null)
```
 
* case control max docs
  
```
case 
	when 
		v_message := 
```
  
* fer un return missatge en comptes de true or false;  

  
### Oracle select funcioNom(): => execute funcioNom();  




```
--
```

## Deveres, retornarDocument  [renovarDocument]


```

/*select format, p.idusuari
from document d
join exemplar e on d.iddocument = e.iddocument
join prestec p on p.idexemplar = e.idexemplar
where datadev is null;


case
	when a = 1 then
		v_message := 'usuari bloquejat'
	when b = 1 then
		v_message := v_message || ' usuari no existeix'
	
*/
```
