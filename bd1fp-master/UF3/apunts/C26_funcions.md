# UF3 funcions.  
    
## Tema de hoy. Triggers sobre vistas.  
  
### Orden ejecución triggers.  
  
1. Trigger Sentence Before
2. Trigger row Before
3. Sentencia DML
4. Trigger after row
5. Trigger after sentence   

##### Nota: postgres muestra DML como ejecutado el ultimo, pero no es el
##### orden real.  
  
## Los triggers a nivel row petan en insert o delete si tiene un new o  
old respectivamente.  

# tg_op => nos dice que operacion DML se ha ejecutado.  
  
## Variables especials/ pseudoregistres:  
https://www.postgresql.org/docs/9.1/plpgsql-trigger.html  
  
### Completar exercici triggers after before 
  
Fer una funció petita que accepta una cadena de text(old i new). Per evitar  
repetir el codi les dues funcions de row cridin a la mateixa funció, UTILITZAR  
CASE. Pasar tg_op com a parametre. Afegir "old/new  no te, dona error"    
  
## Triggers s'han de fer nomes QUAN ELS NECESITEM, si tens molts triggers  
## Es poden fer col·lisió i reacció en cadena entre ells.  
  
  
### Dia de la semana: to_char(current_date,'d'); = dia de la semana numerico.  
##### Nota: lunes = 2, domingo = 1.  
  
  
## Triggers a nivell de vista.  
  
```
-- scott
-- veure vista

scott=> \dv
        List of relations
 Schema |  Name   | Type | Owner 
--------+---------+------+-------
 public | empleat | view | yo


-- veure codi vista
   
scott=> \sv empleat 
CREATE OR REPLACE VIEW public.empleat AS
 SELECT emp.ename AS nomemp,
    emp.sal AS salari,
    emp.job AS ofici,
    dept.dname AS nomdept,
    dept.loc AS localitat
   FROM emp,
    dept
  WHERE emp.deptno = dept.deptno


-- vista dels camps de empleat

scott=> \d empleat 
             View "public.empleat"
  Column   |         Type          | Modifiers 
-----------+-----------------------+-----------
 nomemp    | character varying(10) | 
 salari    | numeric(7,2)          | 
 ofici     | character varying(9)  | 
 nomdept   | character varying(14) | 
 localitat | character varying(13) | 


-- insert que peta:

insert into empleat values ('jordi', 6000, 'profe','informatica','bcn');


-- opcions de create trigger

scott=> \h create trigger
Command:     CREATE TRIGGER
Description: define a new trigger
Syntax:
CREATE [ CONSTRAINT ] TRIGGER name { BEFORE | AFTER | INSTEAD OF } { event [ OR ... ] }
    ON table_name
    [ FROM referenced_table_name ]
    [ NOT DEFERRABLE | [ DEFERRABLE ] [ INITIALLY IMMEDIATE | INITIALLY DEFERRED ] ]
    [ FOR [ EACH ] { ROW | STATEMENT } ]
    [ WHEN ( condition ) ]
    EXECUTE PROCEDURE function_name ( arguments )

where event can be one of:

    INSERT
    UPDATE [ OF column_name [, ... ] ]
    DELETE
    TRUNCATE

```
  
### Tipus de vistes:  
  
* simple (d'una sola taula i que no hi hagin camps calculats(operacións aritmeticas or aggregate functions (count sum min....)))
* d'una o mes taules o que hi hagin camps calculats
  
No es poden fer peracions DML en vistes compostes.
El nostre deure es convertir un insert simple en una taula composta en una  
operació DML amb exit (realitzem les operacións DML sobre les taules arrel) (taules emp i dept)  
  
### Els triggers que s'executen cuan es fa una operació sobre vistes el que fan  
### es transformar la operació.  
  
### En trigger de vista, en comptes d' AFTER o BEFORE utilitzem  INSTEAD OF i el nom de la vista.  


#### Exercici. Crear un trigger per poder inserir a la vista empleat sense que doni cap error.  

```
-- funcio
create or replace function fEmpleat()
returns trigger
as $$
begin
	insert into emp (empno,ename,sal,job)
		values(nextval('empno_seq'),new.nomemp,new.salari,new.ofici);
return new;
end;
$$ language plpgsql;

-- trigger (capçalera)
create trigger tEmpleat
instead of insert on empleat
for each row -- sempre per triggers de vistes
execute procedure fEMpleat();

```
#### Nota: crear empno_seq.  

#### Nota desactivar los otros triggers.

##### Exercici recuperar el departament que falta i posarlo, altre cas
##### Que pasa si no existeix el departament? El trigger l'ha de crear.  

##### Disable triggers.  
alter table emp disable tfiladespres; <=  

## Examen programació 25 i 26.  UF2 i UF4.  
  
## Examen DB 27 i 28.  

## Mañana clase de BD  
  
  
