# UF3 schema.    

### schema = namespace  

**^ show schema** = `\dn`  

current_user : usuario asignado.    
session_user : usuario que se conecto inicialmente.          


crear un rol amb el nostre usuari, que tingui permis sobre una taula concreta  

```
create user pepito;  
  III
create role pepito login;
```

si hacemos:  
```
create role maria;
```
^no te puedes conectar como maira, necesita permiso de login o ser usuario por defecto.  

```
select * from pg_roles;
```

### diccionari de dades  

Contenen metadates, es a dir, dades sobre les dades.  
a postgres els ^ comencen per `pg_`  
`\dt` =>

```
select * from pg_(press tab = 144 possibilities)
```
`\d pg_roles`
```
select rolname from pg_roles ;
```

```
select rolname, rolcanlogin
from pg_roles
where rolcanlogin ='f';

-- sense l'igual

select rolname, rolcanlogin
from pg_roles
where not rolcanlogin ;
-- al ser boolean, true o false son valors directes, y no fa falta igualarlos


-- dba  

select rolname, rolcanlogin
from pg_roles
where not rolsuper;
```  

`pg_tables`
```
-- mostrar les taules del propi usuari
select tablename
from pg_tables
where tableowner='iaw14270791';
-- ^solo funciona en bd que has creado tu
```

### Mostra de la taula propietari, nom de l'esquema (namespace) y propietari(tableowner).
^ pg_tables (schemaname)  
^ pg_namespace (oid)  
^ s'ha de fer joins de taules  
pg_user (oid)  
