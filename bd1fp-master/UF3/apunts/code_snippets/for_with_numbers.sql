create or replace function printDaysOfWeek()
returns void
as $$
begin
   FOR counter_a IN 1..7 LOOP
	raise info '%',to_char(current_timestamp + (counter_a||' days')::interval,'dy');
   end loop;
end;
$$ language plpgsql;
