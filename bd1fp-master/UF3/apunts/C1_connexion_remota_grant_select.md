# UF3. DCL i llenguatge procedural.  

* Conexió remota & Transaccions				-	20%
* DCL (Data Control Language) (permisos)	-	^
* Programació amb BD (PPlpgSQL)				-	80%

### Hi hauran 2 examens, un de Conexió Remota & Transaccions i DCL (Data Control Language), i l'altre de Progrmació.  

## DCL, s'encarrega de donar permisos sobre objectes/usuaris.  

## Conexió remota.

## Una <ins>transacció</ins> es considera un conjunt d'operacions <ins>DML</ins> que s'ha d'executar de manera atomica.

### Atomic, que s'ha d'executar coma un tot, o s'executen totes o no s'executa cap. Si alguna falla es desfaran les que siguin fetes.
### Paraules reservades de transaccions:   

* begin
* commit
* rollback
* savepoint

## Programació:  

* funcions de sistema
	* coalesce
	* round
	* (...)
* funcions d'usuari
* triggers

### <ins>Trigger</ins>: es disparara cuan hi hagi una operació/escdeveniment <ins>DML</ins>.  

```
cd /var/lib/pgsql/data
(sense root = permission denied)
vim postgresql.conf
/listen_addresses
vim pg_hba.conf
```
```
cd /var/lib/pgsql/data
vim postgresql.conf
	listen_addresses = 'localhost' => '*'
	uncomment ^
port = 5432 => uncomment
systemctl restart postgresql

```

## Conexió via psql (sense editar l'altre fitxer conf).  

```
psql -d template1 (implicitament) -U iaw14270791

[root@h03 data]# psql template1
psql: FATAL:  role "root" does not exist

... create user iaw14270791
.... iaw54708991 - Josthyn => 10.200.242.202
mi ip 10.200.242.203

```    

```
psql -d template1 -U postgres -h ip -p 5432
```

## <ins>systemctl</ins>: gestiona serveis/dimonis (pot ser cualsevol servei).  
systemctl	start	postgresql  
			stop  
			restart  
 			enable  
			disable  
			status  
psql -d template1 -U postgres -h 10.200.242.202 -p 5432


# host    all             all             127.0.0.1/32            trust
host    all             all             10.200.242.202/24           trust

psql -d template1 -U postgres -h 10.200.242.202 -p 5432


### Explicació de la mascara: una IP v4 tiene 32 bits. Cada grup (x.x.x.x) es diu byte o octet. va de 0 a 255.  
Una mascara / 24 vol di que els 3 primers bytes estaran a 1 (11111111.11111111.11111111.--------), vol dir, coincideixen amb la ip especificada (10.200.242.cualsevos/24)   

# Ordenado:  
* su -l (como root)
* cd /var/lib/pgsql/data
* vim postgresql.conf
* listen_addresses = 'localhost' => '*'   <==== cambiar localhost por *
	* uncomment ^
	* port = 5432 => uncomment   
* vim pg_hba.conf
```
# host    all             all             127.0.0.1/32            trust
host    all             all             10.200.242.202/24           trust
```
* systemctl restart postgresql
* psql -d template1 -U postgres -h 10.200.242.202 -p 5432

READY.

### create user
* su -l postgres
* psql training (as postgres to db training)
* create user iaw54708991
* grant select on repventa to iaw54708991 ;
* psql -d training2 -U iaw14270791 -h 10.200.242.202 -p 5432 <= correcto

```
host                iaw
10.200.242.202      iaw54708991
10.200.242.201      iaw53336673
create user iaw47859591;

grant select,insert,update,delete on punts to user2,user3;
```
