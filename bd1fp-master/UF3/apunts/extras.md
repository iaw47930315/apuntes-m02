# array_agg

```
select d.deptno,
        array_agg (empno || ' separator ' || ename) "names"
from dept d join emp e on e.deptno = d.deptno
group by d.deptno
order by 1
;

```

# array_agg + json_build_object

```
select d.deptno,
        array_agg (json_build_object('empno',empno,'deptno',e.deptno,'name',ename)) "names"
from dept d join emp e on e.deptno = d.deptno
group by d.deptno
order by 1
;

```
