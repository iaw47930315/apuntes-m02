# UF3  Funcions.   
  
##### Funcion ejemplar disponible. Lo ideal seria pasarle el codigo del ejemplar. Le pasamos titulo por que cuando tu haces una reserva por internet, dices que quieres. 
  
## Cursors.  
  
Tipo de estructura que gestiona N filas (multiples filas).  
Un cursor es una variable especial que puede contener N valores.  
Primero se declara el cursor. El cursor no es mas que una consulta.  

#### exquema
```
-- cursor
declare => open => fetch => ? => close
							^ emptiy?
					^fetch	<= ^ NO						
```
  
#### sintaxis

```
DECLARE
    cur_films  CURSOR FOR SELECT * FROM film;
    cur_films2 CURSOR (year integer) FOR SELECT * FROM film WHERE release_year = year;
```
  
#### Ejemplo:  
  
```
create or replace function mostrarEmpleats()
returns varchar
as $$ 
	declare
		cur_empleats cursor for 
			select *
			from emp
			where deptno in (10,30);
	v_fila_empleat emp%rowtype;
begin
	open cur_empleats; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_empleats INTO v_fila_empleat;
	close cur_empleats;
	return format ('Empleat %s dept %s',v_fila_empleat.ename,v_fila_empleat.deptno);
end;
$$ language plpgsql;

```

#### Con columnas custom (ename, job, mgr, depnto, sal). 
  
* `record` es un contenedor que no especifica estructura
  
Se añade tambien bucle.
```
create or replace function mostrarEmpleats()
returns varchar
as $$ 
	declare
		cur_empleats cursor for 
			select ename, job, mgr, deptno, sal
			from emp
			where deptno in (10,30)
			order by deptno;
	v_empleat record;
begin
	open cur_empleats; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_empleats INTO v_empleat;
	-- found / not found
	while found -- en oracle => cur_empleats found
		loop
			raise info 'Empleat % dept %',v_empleat.ename,v_empleat.deptno;
			-- falta avanzar el cursor una posición V
			fetch cur_empleats INTO v_empleat;
		end loop;
	close cur_empleats;
	return 'ok';
end;
$$ language plpgsql;

-- select mostrarEmpleats();

```
##### ^Nota no puedo hacer un `found` sin un `fetch`.  
##### ^Nota no se puede avanzar en el loop sin incrementar el cursor, el cursor se incrementa con `fetch cur_empleats INTO v_empleat;`.   
  
### Exercisi, si l'empleat es del dept 10 increment del sou en un 15%, 20 en un 8% i 30 en un 14%.  
  
  
```
create or replace function incrementaSou()
returns varchar
as $$ 
	declare
		cur_empleats cursor for 
			select ename, job, mgr, deptno, sal
			from emp
			where deptno in (10,20,30)
			order by deptno;
	v_empleat record;
	v_salari_incrementat numeric;
begin
	open cur_empleats; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_empleats INTO v_empleat;
	-- found / not found
	while found -- en oracle => cur_empleats found
		loop
			--raise info 'Empleat % dept %',v_empleat.ename,v_empleat.deptno;
			case v_empleat.deptno
			WHEN 10 THEN
				--
				v_salari_incrementat :=  v_empleat.sal * 1.15;
			when 20 then
				--
				v_salari_incrementat :=  v_empleat.sal * 1.08;
			when 30 then
				--
				v_salari_incrementat :=  v_empleat.sal * 1.14;
			end case;
			-- falta avanzar el cursor una posición V
			raise info 'Departament %. Sou normal %, sou incrementat %.',v_empleat.deptno,v_empleat.sal,v_salari_incrementat;
			fetch cur_empleats INTO v_empleat;
		end loop;
	close cur_empleats;
	return 'ok';
end;
$$ language plpgsql;

 select incrementaSou();

``` 
##### Nota ahorrar codigo, es decir hacer un solo raise notice aprovechando la variable  `v_salari_incrementat`
##### Nota se puede hacer v_empleat.sal := v_empleat.sal * 1.15; si no usamos el v_empleat.sal no incrementado.  
    
### CASE.  
Dos formas de fer-ho, cuan l'operador no cambia:  
```
case v_empleat.deptno
	when 10 then
		--
	when 20 then
		--
```
	
Cuando el operador cmbia:  
```
case 
	when v_empleat.deptno in (10,20) then
		--
	when v_empleat.deptno = 13 then
		--
```
  
### LOOP.  
  
```
 LOOP
    -- fetch row into the film
      FETCH cur_films INTO rec_film;
    -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;

    -- build the output
      IF rec_film.title LIKE '%ful%' THEN 
         titles := titles || ',' || rec_film.title || ':' || rec_film.release_year;
      END IF;
   END LOOP;
```
#### Nota cuando haces un `found` primero has de haber hecho un `fetch`.  
  
#### Caracteristicas del `LOOP`, se puede ejecutar codigo minimo una vez. Similar al `DO WHILE` de java, se ejecuta minimo una vez.  


```
create or replace function mostrarEmpleats()
rreturn varchar
as $$
	declare
		cur_empleats cursor for 
			select ename, job, mgr, deptno, sal
			from emp
			where deptno in (10,20,30)
			order by deptno;
		v_empleat record;
	begin
		open cur_empleats;
		loop
			open cur_empleats;
			LOOP
				fetch cur_empleats into v_empleat;
			exit when NOT FOUND;
				raise info '...',v_empleat.job;
			end loop;
		close cur_empleats;
	return 'ok';
	end
$$ language plpgsql;	
		
```
  
### FOR LOOP.  
  
```
create or replace function mostrarEmpleats()
rreturn varchar
as $$
	declare
		cur_empleats cursor for 
			select ename, job, mgr, deptno, sal
			from emp
			where deptno in (10,20,30)
			order by deptno;
		v_empleat record;
	begin
		open cur_empleats;
		for v_empleat in cur_empleats -- no hace falta open, ni fetch ni close
			LOOP
			
			end loop;
	return 'ok';
	end
$$ language plpgsql;	
		
```
#### Nota no hace falta open, ni fetch ni close con FOR In.
  
### LOOPS con parametros  
  



* p_year
```
CREATE OR REPLACE FUNCTION get_film_titles(p_year INTEGER)
   RETURNS text AS $$
DECLARE 
	 titles TEXT DEFAULT '';
	 rec_film   RECORD;
	 cur_films CURSOR(p_year INTEGER) 
		 FOR SELECT title, release_year
		 FROM film
		 WHERE release_year = p_year;
BEGIN
   -- Open the cursor
   OPEN cur_films(p_year);
	
   LOOP
    -- fetch row into the film
      FETCH cur_films INTO rec_film;
    -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;

    -- build the output
      IF rec_film.title LIKE '%ful%' THEN 
         titles := titles || ',' || rec_film.title || ':' || rec_film.release_year;
      END IF;
   END LOOP;
  
   -- Close the cursor
   CLOSE cur_films;

   RETURN titles;
END; $$

LANGUAGE plpgsql;
```
   
### CURSOR con parametros.  
  
```
CREATE OR REPLACE FUNCTION get_film_titles(p_year INTEGER)
RETURNS varchar AS $$
DECLARE 
	declare
		cur_empleats cursor(p_deptno int) for
			select ename, job, mgr, deptno,sal
			from emp
			where deptno = p_deptno;
		v_empleat record;
	begin
		open cur_empleats(20);
		loop
			fetch cur_empleats into v_empleat;
		exit when not found;
			raise info 'Empleat % dept %',v_empleat.ename,v_empleat.deptno
END; $$

LANGUAGE plpgsql;
```
  
##### Segundo ejemplo cursor con parametros.  
  
```
CREATE OR REPLACE FUNCTION get_film_titles(p_year INTEGER)
RETURNS varchar AS $$
DECLARE 
	declare
		cur_empleats cursor(p_deptno int) for
			select ename, job, mgr, deptno,sal
			from emp
			where deptno = p_deptno;
		v_empleat record;
	begin
		open cur_empleats(20);
		loop
			fetch cur_empleats into v_empleat;
		exit when not found;
			raise info 'Empleat % dept %',v_empleat.ename,v_empleat.deptno
		end loop;
		open cur_empleats(10);
		loop
			fetch cur_empleats into v_empleat;
		exit when not found;
			raise info 'Empleat % dept %',v_empleat.ename,v_empleat.deptno
		end loop;
		close cur_empleats;
	return 'ok';
END; $$

LANGUAGE plpgsql;
``` 
 
  
#### Ejercicios. A la bd biblioteca. Mostrar id ejemplar, id documento, titulo, (nombre y apellidos en una unica columna). Provar while, loop y for. Un solo cursor, un solo codigo, 3 loops.  
^ cursors.sql
#### El codi de exemplarDisponible amb cursors, versio 2.  

prestec solo coje un documento.  Al no meter la iteración dentro de la funcion NO LA HACES DEPENDIENTE.
Una función superior controla la cantidad de veces que hacer prestec.  
Imaginar documentos en un formularios, uno a uno.  
Si un codigo lo puedes reutilizar no lo modifiques, haz una función superior que utilice esa otra función n veces.  
  


