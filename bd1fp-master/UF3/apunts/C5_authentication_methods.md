# UF3. PostgreSQL authentication methods.  
  
## Fitxers de configuració de connexió remota PostgreSQL.  
  
* su -l (como root)
* cd /var/lib/pgsql/data
* vim postgresql.conf
* listen_addresses = 'localhost' => '*'  <==== cambiar localhost por *
	* uncomment ^
	* port = 5432 => uncomment   
* vim pg_hba.conf
  
### Exemple connexió remota:  
`psql -d training2 -U iaw14270791 -h 10.200.242.202 -p 5432`  
  
#### # "local" is for Unix domain socket connections only  
Un **socket** es la combinació de la parella **IP** i **Port**.  
Un **port** es un numero en el qual hi ha un programa que esta **escoltant peticions i servintles**.  
  
`pg_hba.conf` es divideix en configuració connexions locals or remotes. `local` o `host`.  

### Possar password a les conexions remotes.  

`host     all             all             10.200.242.202/32      md5` 

`alter user iaw54708991 password 'jupiter';`
`systemctl restart postgresql`
`alter role rolename password 'password';` <= requires login privilege

`create role pepito login:`
||
`create user pepito;`

# Como entrar como postgres en una linea y poner permisos.  
```
psql -U postgres training -h 10.200.242.203

o
psql -U postgres training -h 10.200.242.203 -c "alter user iaw53336673 password 'jupiter';"

``` 
Otra manera:  
local   all             postgres            trust  
```
#### Resetejar password:  
`alter role iaw... reset password`  
+  
md5 => trust  
