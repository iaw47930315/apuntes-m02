# UF3. Transaccions multi usuari.  

Una operació normal es una transacció, per que te un commit implicit. Una operació despres d'un begin, si no te un commit no es una transacció per si sola.  

**Begin**, señala el inicio de una transacción.  
**Commit**, hace que los cambios sean definitivos. Borrael contingut dels fitxders de redolog. Es a dir no es guarda l'estat anterior a l'estat actual.   
**Rollback** todos los cambios hasta el `begin` desaparecen.  

```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
^ La transacció no ha acavat al ser un rollback to.  

Acaven transacció:  
* rollback (sense to)
* commit  			

```
lineas=> INSERT INTO punts (id, valor) VALUES (50,5);
INSERT 0 1
lineas=> BEGIN;
BEGIN
lineas=> SELECT id, valor WHERE punts;
ERROR:  no existe la columna «id»
LINE 1: SELECT id, valor WHERE punts;
               ^
lineas=> UPDATE punts SET valor = 4 WHERE id = 50;
ERROR:  transacción abortada, las órdenes serán ignoradas hasta el fin de bloque de transacción
lineas=> COMMIT;
ROLLBACK
lineas=> SELECT valor FROM punts WHERE id = 50;
 valor
-------
     5
```
^ Al haber orden erronea, todo el bloque de transacción se aborta.  

```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
Al haber un rollback to b permet recuperarse de l'error. Fent un rollback to b s'evita abortar tot el bloc de transacció.  

```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
COMMIT;
ROLLBACK -- fet automatic pel gestor quan fas commit
SELECT SUM(valor) FROM punts;
```
Si fas un commit amb bloc de transacció abortada s'efectua un `ROLLBACK` a secas, es a dir, aborta tota la transacció.  

Les operaciómns DML efectuades despres d'un error seran ignorades.  

### Transaccion pseudocodigo.  

```
delete  update  commit  commit
u1      u2      u2      u1
```
#### ^ explicació u2 es queda bloqujat a l'update, i al fer el commit fa un rollback per que ha modificat una fila NO EXISTENT!  
