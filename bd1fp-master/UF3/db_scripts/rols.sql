-- Nom: Jacint 
-- Cognoms: Iglesias Casanova
-- Codi: iaw14270791
-- Nom projecte: iglesiasjacint
-- Data: 13-02-2020
\c template1
drop database if exists rols;
create database rols;
\c rols

CREATE TABLE profe (
	idprof smallint  constraint profe_pk primary key,
    nom varchar(25)
);

CREATE TABLE modulxprofe (
    idprof smallint,
    idmodul smallint unique,
    CONSTRAINT PK_MODULXPROFE_PM PRIMARY KEY(idprof,idmodul),
    CONSTRAINT FK_MODULXPROFE_IDPROF FOREIGN KEY(idprof) REFERENCES profe(idprof)
);
CREATE TABLE faltes (
    idmodul smallint,
    dia date,
    hora time,
    codialumne varchar(22),
    justificada boolean,    
    CONSTRAINT PK_FALTES_MDC PRIMARY KEY(idmodul,dia,codialumne),
    CONSTRAINT FK_FALTES_IDMODUL FOREIGN KEY(idmodul) REFERENCES modulxprofe(idmodul)    
);

insert into profe (idprof,nom) (
values	(1,'Francesc Bibes'),
		(2,'Antoni Comins'),
		(3,'Alex Ferrer')
);

insert into modulxprofe (idprof,idmodul) (
values	(1,1),
		(2,2),
		(3,3)
);

insert into faltes (idmodul,dia,hora,codialumne,justificada) (
values	(1,'2020-03-03','08:00:00', 1,true),
		(2,'2020-03-03','08:00:00', 2,false),
		(3,'2020-03-03','08:00:00', 3,false),
		(1,'2020-03-03','08:00:00', 4,false)
);

/*
escola=# create user profesor;
CREATE ROLE
escola=# create user tutor;
CREATE ROLE
escola=# create user alumne;
CREATE ROLE
*/
/* ALUMNE */
/*
escola=# create role alumner;
CREATE ROLE

grant select on faltes to alumner;
grant alumner to alumne;
set role alumne;

select * from faltes;
escola=> select * from modulxprofe ;
ERROR:  permission denied for relation modulxprofe
*/
/* PROFE */
/*
create role profesor_role;
grant select,update,insert on faltes to profesor_role;
grant select on modulxprofe,profe to profesor_role;

escola=# grant profesor_role to profesor;

escola=# set role profesor;


escola=> insert into profe values (4,'test');
ERROR:  permission denied for relation profe

escola=> update faltes set hora = current_time where codialumne='1' and idmodul=1;
UPDATE 1

escola=> select * from profe;
-- funciona
*/
/* TUTOR */
/*
set role postgres;
create role tutor_role;
grant select,update,insert,delete on faltes,modulxprofe,profe to tutor_role;
grant tutor_role to tutor;

set role tutor;
escola=> update profe set nom= 'Joan Bibes' where idprof = 1;
UPDATE 1

-- añadir drop if exists
drop role myroleHere if exists;
*/


-- ^adaptar a script agnostico. Incluiendo grant privileges al user
