-- iaw14270791
-- Jacint Iglesias Casanova
-- H1IAW
-- DB name: lineas
-- filename: lineas.sql
-- connexion and database creation data
\c template1
drop database if exists lineas;
create database lineas;
\c lineas
-- creating tables
CREATE TABLE punts
       (id smallint  constraint id_punts_pk primary key,
        valor smallint
);
-- inserting values
INSERT INTO punts VALUES (0,0),
						(1,1),
						(2,2),
						(3,3),
						(4,0),			
						(5,1),	
						(6,2), 
						(7,3);
