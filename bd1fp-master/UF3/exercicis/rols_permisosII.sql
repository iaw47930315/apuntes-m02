-- Exercici: Agafar una taula, (faltes) dexifrar que vol dir cada cosa 
-- (a,r,d...).  Crear un rol i jugar amb ell, en conexió remota.  
  
-- psql -U postgres -d rols

 create user rol_tester;

create role rol_tester_role;

-- pslq rols

\dp

\du

-- Role name		Attributes													member of
-- rol_tester      |                                                            | {}
-- rol_tester_role | Cannot login 



-- as sys user, db creator and table creator (yo/ iaw14270791)
set role yo;

grant select on faltes to rol_tester_role;

-- rol_tester_role=r/yo


-- psql -U rol_tester_role -d rols
-- psql: FATAL:  al rol «rol_tester_role» no se le permite conectarse

grant rol_tester_role to rol_tester;

-- psql -U rol_tester -d rols
-- ^ connects right

select current_user, session_user;
--  current_user | session_user 
-- --------------+--------------
--  rol_tester   | rol_tester

-- rol_tester_role=r/yo |                   | 
-- r means read, which also means select privilege

select * from faltes;
-- ^success

-- psql rols 
-- ^ (as db creator)

grant update on faltes to rol_tester_role;


set role rol_tester; -- as postgres

update faltes set hora = current_time where idmodul=1 and codialumne='1';

-- ^successful

\dp
 
-- rol_tester_role=rw/yo

-- w means write which means to update



grant insert on faltes to rol_tester_role;
-- ^ as system user


set role rol_tester;
-- ^as postrgres

insert into faltes (idmodul,dia,codialumne)
values (1,current_date,'2');
-- ^successful

\dp

-- rol_tester_role=arw/yo

-- a means to append, which means to insert


grant delete on faltes to rol_tester_role;
-- ^ as system user


delete from faltes where idmodul=1 and dia = current_date and codialumne = '2';
-- ^as rol_tester user
-- ^is sucessful

-- rol_tester_role=arwd/yo 
-- d means to delete which is equivalent to delete privilege

revoke select on faltes from rol_tester_role;

-- \dp shows no entry for rol_tester_role


-- D -- TRUNCATE
-- x -- REFERENCES
-- t -- TRIGGER
