create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa repventa%rowtype;
begin
	select * -- es rowtype por lo tanto hay que incluir toda la fila
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
			return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;

select representant(109);

select representant(1099);

-- amb nomes dos camps V

-- create type (se crea fuera de la función)

create type tipus_repventa as(nombre varchar(50), puesto varchar(100));

create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa tipus_repventa;
begin
	select nombre, puesto -- create type
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
			return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;


-- raise exception  
  
create type tipus_repventa as(nombre varchar(50), puesto varchar(100)); -- variable composta

create or replace function representant(p_repcod integer) --  puentear smallint
returns varchar
as $$
declare 
	v_repventa tipus_repventa;
begin
	select nombre, puesto -- create type
	into strict v_repventa
	from repventa
	where repcod = p_repcod;
	raise notice 'El nom del representat amb codi % es %, el seu treball es %',p_repcod,v_repventa.nombre,v_repventa.puesto;
	return 'Consulta realizada con exito';
exception
		when no_data_found then 
		raise exception '% not found', p_repcod; -- $? daria un valor 
												 --diferente de 0, es decir se comunica error
			--return  'Error: No se encuentra el representante '|| p_repcod;
end;
$$ language plpgsql;

select representant(1099);

\c bdfuncions

-- multiple default parameters

create or replace function foo(	p_one integer default null,
								p_two integer default 43,
								p_three varchar default 'foo') -- parametre formal
returns varchar
as $$
begin
	return format('p_one=%s, p_two=%s, p_three=%s',p_one,p_two,p_three);
end;
$$ language plpgsql;
select foo(p_two=>22);
select foo(p_one=>11,p_two=>22);
select foo(p_three=>'hola');
select foo(p_three=>'hola',p_one=>1);
drop funciton foo(integer, integer, varchar); -- para cuando no puedes borrar normal
-- p default and required p

create or replace function foo2(p_zero int,
								p_one integer default null,
								p_two integer default 43,
								p_three varchar default 'foo') -- parametre formal
returns varchar
as $$
begin
	return format('p_zero=%s,p_one=%s, p_two=%s, p_three=%s',p_zero,p_one,p_two,p_three);
end;
$$ language plpgsql;

select foo2(11); --  asigna p_zero
select foo(11,p_one=>11,p_two=>22);
select foo2(11,p_three=>'hola');
select foo2(p_three=>'hola',p_zero=>1);
drop funciton foo2(integer,integer, integer, varchar); -- para cuando no puedes borrar normal
