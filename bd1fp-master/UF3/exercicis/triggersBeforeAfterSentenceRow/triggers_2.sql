--() { :; }; exec psql scott -f "$0"

-- iaw14270791
-- joc de proves funcions scott 
-- Jacint Iglesias
-- UF3
\c scott


-- sentence before


drop trigger updateChanges_sentence_before on emp;
create or replace function updateChanges_sentence_before()
returns trigger
as $$
begin
	raise info 'SENTENCE ABANS';
	return null; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;



drop trigger t_updateChanges_sentence_before on emp;
CREATE TRIGGER t_updateChanges_sentence_before
BEFORE UPDATE or insert or delete ON emp
FOR statement
EXECUTE PROCEDURE updateChanges_sentence_before();





-- sentence after
drop trigger updateChanges_sentence_after on emp;
create or replace function updateChanges_sentence_after()
returns trigger
as $$
begin
	raise info 'SENTENCE DESPRES';
	return null; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;



drop trigger t_updateChanges_sentence_after on emp;
CREATE TRIGGER t_updateChanges_sentence_after
after UPDATE or insert or delete ON emp
FOR statement
EXECUTE PROCEDURE updateChanges_sentence_after();





-- functions to call from row before / after

drop function fGetOldNew(dml_type varchar,emp, emp);
create or replace function fGetOldNew(dml_type varchar,p_new emp default null, p_old emp default null)
returns void
as $$
begin
	if dml_type = 'INSERT' then
		raise info '(new) nom:% , deptno: % , (old) <NULL>, <NULL>',p_new.ename,p_new.deptno;
	elsif dml_type = 'DELETE' then
		raise info ' (old) nom:% , deptno: %  (new) <NULL>, <NULL>',p_old.ename,p_old.deptno;
	else
		raise info 'Nom i dept (new) nom:% , deptno: % . (old) nom:%, deptno: %',p_new.ename,p_new.deptno,p_old.ename,p_old.deptno;
	end if;
end;
$$ language plpgsql;






-- row before



drop trigger f_updateChanges_row_before on emp;
create or replace function f_updateChanges_row_before()
returns trigger
as $$
begin
	raise info 'ROW AVANS';
	perform fGetOldNew(tg_op,new = null,old = null);
	return new ; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;


 
drop trigger t_updateChanges_row_before on emp;
CREATE TRIGGER t_updateChanges_row_before
BEFORE UPDATE or insert or delete ON emp
FOR each row
EXECUTE PROCEDURE f_updateChanges_row_before();

/*
drop trigger f_updateChanges_row_before on emp;
create or replace function f_updateChanges_row_before()
returns trigger
as $$
begin
	raise info 'ROW AVANS';
	perform fGetOldNew(tg_op);
	return new ; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;
*/
/*
drop trigger f_updateChanges_row_before on emp;
create or replace function f_updateChanges_row_before()
returns trigger
as $$
begin
	raise info 'ROW AVANS';
	if tg_op = 'INSERT' then
		raise info 'Nom i dept (new) nom:% , deptno: % ', new.ename,new.deptno;
	else if tg_op = 'DELETE' then
		raise info 'Nom i dept (old) nom:% , deptno: % ', old.ename,old.deptno;
	else
		raise info 'Nom i dept (new) nom:% , deptno: % . (old) nom:%, deptno: %', coalesce(new.ename,'No te nom(new)'),coalesce(new.deptno::varchar,'No te depnto(new)'),coalesce(old.ename,'No te nom(old)'),coalesce(old.deptno::varchar,'No te depnto(old)');
	end if;
	return new ; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;
 */
 






/*
-- row after
drop trigger updateChanges_row_after on emp;
create or replace function updateChanges_row_after()
returns trigger
as $$
begin
	raise info 'ROW DESPRES';
	if tg_op = 'INSERT' then
		raise info 'Nom i dept (new) nom:% , deptno: % ', new.ename,new.deptno;
	else if tg_op = 'DELETE' then
		raise info 'Nom i dept (old) nom:% , deptno: % ', old.ename,old.deptno;
	else
		raise info 'Nom i dept (new) nom:% , deptno: % . (old) nom:%, deptno: %', coalesce(new.ename,'No te nom(new)'),coalesce(new.deptno::varchar,'No te depnto(new)'),coalesce(old.ename,'No te nom(old)'),coalesce(old.deptno::varchar,'No te depnto(old)');
	end if;
	return new; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;

drop trigger t_updateChanges_row_after on emp;
CREATE TRIGGER t_updateChanges_row_after
after UPDATE or insert or delete ON emp
FOR each row
EXECUTE PROCEDURE updateChanges_row_after();


*/
