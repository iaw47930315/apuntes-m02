# Authentication methods in PostgreSQL.  
  
**trust**: remote connexion, allows the user to specify the user with which he wants to connect to the database.  
  
**Ident**: remote, no password. Failed to connect even after editing `/var/lib/pgsql/data/pg_ident.conf`  
  
**peer**: local, no password, requires user to have matching user in the database, doesn't allow user to connect as other user.   
```
^
[root@h03 ~]# psql template1
psql: FATAL:  role "root" does not exist

```
  
**md5**: remote or local, requires password. Password is encrypted and stored in a hash in the server.  
  
**password**: remote or local, requires password, password is sent to the server in clear text (it's not encrypted).  
  
=>
* reject /trust => permetre o bloquejar usuaris/ IP, etc
* peer / ident => OS user = DB user
* passwrod / md5 
  
Primera match predomina sobre el seguent en `pg_hba.conf `. Per tant s'ha d'aplicar primerament les regles restrictives avans que les permissives per que fagin efecte.  
```
host     all             all             10.200.242.210/32      reject
host     all             all             10.200.242.203/24      trust
```
^ Permet tots els usuaris de la red menys la ip ` 10.200.242.210/32 `.  
  
**connexió local**: `psql training`  
