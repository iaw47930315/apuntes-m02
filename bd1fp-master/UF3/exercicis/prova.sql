create or replace function reservarPista(p_idusuari integer,p_dia date, p_hora varchar(4), p_pista integer, p_durada numeric(2,1)) 
returns varchar
as $$
	declare
		v_abonat boolean;
		v_datareserva_in_range boolean;
		v_reserva_same_week boolean;
		v_hora int;
		v_minut int;
		v_pista_reservada boolean;
		v_message varchar(200) = 'Tot correcte';
		v_preu int;
	begin
		-- enmagatzema si l'usuari esta abonat (true false)
		-_abonat := usuariAbonat(p_idusuari);
		-- comprova si l'horari de reserva esta dins del rang (depenent si usuari es abonat ono)
		v_datareserva_in_range := horaReservaCheck(p_dia,p_hora,v_abonat);
		-- comprovar cas de reserva no consumida la mateixa setmana
		v_reserva_same_week := reservaSameweek(p_idusuari,p_dia);
		case 
			when to_char(p_dia,'d')::int between 2 and 6 and substr(p_hora,1,2):int >= 7 and substr(p_hora,1,2):int < 17 or (substr(p_hora,1,2):int = 17 and substr(p_hora,3,4):int = 0) then
				v_preu := 50;
			when to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2):int >= 14 and substr(p_hora,1,2):int < 21 or (substr(p_hora,1,2):int = 21 and substr(p_hora,3,4):int = 0) then
				v_preu := 50;
			when to_char(p_dia,'d')::int between 2 and 6 and substr(p_hora,1,2):int >= 17 and substr(p_hora,1,2):int < 21 or (substr(p_hora,1,2):int = 21 and substr(p_hora,3,4):int = 0) then
				v_preu := 100;
			when to_char(p_dia,'d')::int in (1,7) and substr(p_hora,1,2):int >= 7 and substr(p_hora,1,2):int < 14 or (substr(p_hora,1,2):int = 14 and substr(p_hora,3,4):int = 0) then
				v_preu := 100;
			else
				v_preu := 150;
		end case;
		case 
			when not v_datareserva_in_range then
				v_message := 'Error: la reserva es fora del termini maxim de reserva (48 horas per no abonats i 72 per abonats)';
			when not reservaSameweek then
				v_message:= 'Error: aquest usuari ja te una altra reserva activa aquesta setmana';
			else
				-- fer l'insert
				insert into reserva (idreserva,idpista,dia,horaini,durada,idusuari,datareserva,import)
					(nextval('reserva_idreserva_seq')::int,p_pista,p_dia,p_hora,p_durada,p_idusuari,current_timestamp,v_preu);
		end case;
	return v_message;
	end;
$$ language plpgsql;

