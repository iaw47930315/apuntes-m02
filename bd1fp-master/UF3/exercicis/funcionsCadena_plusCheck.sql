-- iaw14270791
-- funcions de cadenes
-- Jacint Iglesias
-- UF3
\c training
drop database if exists bdfuncions;
create database bdfuncions;
\c bdfuncions

-- 1. funció translate
\echo '1. funció translate'

create or replace function textPla(cadena varchar) -- cadena a traduir
returns varchar
as $$
	declare
		cadenaprocessada varchar;
	begin
		cadenaprocessada := translate(cadena, 'àáäÀÁÄèéëÈÉËìíïÌÍÏòóöÓÒÖúùüÚÙÜçÇñÑ', 'aaaAAAeeeEEEiiiIIIoooOOOuuuUUUcCnN');
		return cadenaprocessada;
	end;
$$ language plpgsql;
-- invoquem la funció
select textPla('àáäÀÁÄèéëÈÉËìíïÌÍÏòóöÓÒÖúùüÚÙÜçÇñÑ');
select textPla('pingüí africÀ al glaç');

-- 2. funció dni
\echo '2. funció dni'

create or replace function validaDNI(dni varchar) -- cadena a traduir
returns varchar
as $$
	declare
		lletra varchar;
		numeros int;
		cadenalletres varchar;
		modul int;
	begin
		cadenalletres := 'TRWAGMYFPDXBNJZSQVHLCKE';
		lletra := substr(dni, 9);
		numeros := substr(dni, 1 , 8)::int;
		modul := numeros % 23 + 1;
		if substr(lower(cadenalletres) , modul , 1) = lower(lletra) then
			return 'DNI correcte';
		end if;
		return 'DNI incorrecte';
	end;
$$ language plpgsql;
-- invoquem la funció amb DNI correcte
select validaDNI('14270791G');
-- DNI incorrecte
\echo 'V aquest DNI es incorrecte a posta'
select validaDNI('14270791F');




\c training
-- 3. repventa2
\echo '3. repventa2'

-- drop table if exists repventa2;

-- create table repventa2 as select *,
	-- lower(substr(nombre,  1, 1)|| 
	-- substr(regexp_replace(nombre, '[A-Z][a-z]* ', '') , 1, 8)) login 
-- from repventa ;

-- select * from repventa2;


-- usar strpos('substring', 'string') esta mas optimizado y es standard
-- SQLs

drop table if exists repventa2;

create table repventa2 as select *,
	lower(substr(nombre,  1, 1)|| 
	substr(nombre ,strpos(nombre, ' ' ) + 1, 7)) login 
from repventa ;

select * from repventa2;
