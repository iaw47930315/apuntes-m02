-- preparem entorn pel desenvolupament de les funcions

drop database if exists bdfuncions;
create database bdfuncions;
\c bdfuncions

-- 1. implementeu la funció `textPla`, que tindrà un paràmetre formal cadena
--  i retornarà la cadena sense cap tipus d'accentuació
create or replace function textPla (cadena varchar)
returns varchar
as $$
		begin
			return translate(cadena,
								'áàÁÀéèÈÉíïÍÏóòÓÒúüÚÜñÑçÇ',
								'aaAAeeEEiiIIooOOuuUUnNcC');
		end;
$$ language plpgsql;

-- 2. funció validaDNI

create or replace function validaDNI (dni varchar)
returns varchar
as $$
  declare
    lletra varchar(1);
    num_dni varchar(8);
    missatge varchar(50);
    posicio int;

  begin
    num_dni := substr(dni,1,8);
    lletra := substr(dni,9,1);
    posicio := mod( num_dni::int , 23) + 1;
    if upper(lletra) = substr('TRWAGMYFPDXBNJZSQVHLCKE',posicio , 1) then
        missatge:= dni ||' correcte';
    else
        missatge:= dni ||' incorrecte';
    end if;
    return missatge;
  end;
$$ language plpgsql;


-- preparem l'entorn fer l'exercici 3
-- i netegem si és el cas
\c training
drop table if exists repventa2;

-- 3. Exercici 3

-- A. Per a cada representant, mostrar el camp nombre i la posició on comença el cognom

select nombre, strpos(nombre, ' ') + 1 as posicioCognom
from repventa;

-- B.  Per cada representant, mostra el el camp nombre (tal com està a la base de dades)
-- i la columna Apellido

select nombre, substr(nombre, strpos(nombre, ' ')+1 ) as apellido
from repventa;


-- C. Per cada representant, mostra el camp nombre i el seu login

select nombre, lower(substr(nombre, 1, 1) || substr(nombre, strpos(nombre, ' ') + 1))
from repventa;

-- D. Ja podem fer l'exercici complet!


create table repventa2 as
	select *, lower(substr(nombre, 1, 1) || substr(nombre, strpos(nombre, ' ') + 1)) as login
	from repventa;
