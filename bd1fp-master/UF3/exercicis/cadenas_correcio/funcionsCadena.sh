#!/bin/bash
# Script: funcionsCadena.sh
# Description: script que aplica joc de proves per validar els exercicis de cadenes
# Date creation: 11/03/2020
# Use: ./funcionsCadena

NEUTRE='\033[0m'
VERD='\033[0;32m'
  # Carreguem les funcions a la BD bdfuncions
psql -d template1 -f funcionsCadena.sql
  # Apliquem un joc de proves per validar les funcions i taula
echo -e "\n$VERD TEST Exercici 1$NEUTRE\n"
psql -d bdfuncions -c "select textpla('pingüí africÀ al glaç')"
echo -e "\n$VERD TEST Exercici 2$NEUTRE\n"
psql -d bdfuncions -c "select validaDNI('12345678z')";
psql -d bdfuncions -c "select validaDNI('12345678a')";
echo -e "\n$VERD TEST Exercici 3$NEUTRE\n"
psql -d training -c "select repcod, nombre, login from repventa2"
