--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- funcions bibblioteca
-- Jacint Iglesias
-- UF3

-- function is available (disponible a exemplar)
--isAvailable(p_iddocument)
\c biblioteca

create or replace function isAvailable(p_iddocument document.iddocument%type)
returns boolean
as $$
declare
	v_exemplar exemplar%rowtype;
begin
	FOR v_exemplar IN 
			select *
			from exemplar
			where iddocument = p_iddocument
		LOOP
			-- can do some processing here
			if lower(v_exemplar.estat) = 'disponible' then
				return true;
			end if;
		END LOOP;
	return false;
exception
		when no_data_found then 
		return false;
end;
$$ language plpgsql;


-- test
-- update exemplar set estat= 'Exclòs de prèstec' where iddocument =1 and idexemplar =3;
-- select isAvailable(1);
-- end function is available

-- funció getExemplar
create or replace function getExemplar(p_iddocument document.iddocument%type)
returns integer
as $$
declare
	v_exemplar exemplar%rowtype;
begin
	FOR v_exemplar IN 
			select *
			from exemplar
			where iddocument = p_iddocument
		LOOP
			-- can do some processing here
			if lower(v_exemplar.estat) = 'disponible' then
				return v_exemplar.idexemplar;
			end if;
		END LOOP;
	return null;
exception
		when no_data_found then 
		return null;
end;
$$ language plpgsql;


-- test
-- update exemplar set estat= 'Exclòs de prèstec' where iddocument =1 and idexemplar =3;
-- update exemplar set estat= 'Disponible' where iddocument =1 and idexemplar =3;
-- select isAvailable(1);
-- end function is available


-- funció esta en lloguer o no

create or replace function isNotRented(p_idexemplar prestec.idexemplar%type)
returns boolean
as $$
declare
	p_basura varchar(1);
begin
	select '1'
	into strict p_basura
	from prestec 
	where idexemplar = p_idexemplar and
	datadev is null;
	return false;
exception
		when no_data_found then 
			return true;
end;
$$ language plpgsql;

-- end is not rented



-- funció comprova exemplar

create or replace function docuemntDisponible(p_iddocument document.iddocument%type, p_tipus varchar(50)) --  puentear smallint
returns integer
as $$
declare 
	v_iddocument document.iddocument%type;
	v_exemplar exemplar%rowtype;
	v_exemplarDisponible exemplar.idexemplar%type;
begin
	-- storing valid id
	case 
		when lower(p_tipus) = 'llibre' then
			select iddocument
			into strict v_iddocument
			from llibre
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'revista' then
			select iddocument
			into strict v_iddocument
			from revista
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'musica' then
			select iddocument
			into strict v_iddocument
			from musica
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'pelicula' then
			select iddocument
			into strict v_iddocument
			from pelicula
			where iddocument = p_iddocument;
		else
			return 0; -- es descarta taula falsa i id document f
	end case;
	-- function checks exemplar is 'Disponible'
	if isAvailable(p_iddocument) then
		v_exemplarDisponible := getExemplar(p_iddocument);
		-- check if it is rented or not
		if  isNotRented(v_exemplarDisponible) then
			return v_exemplarDisponible; 
		end if;
	else 
		return 0; -- es descarta si no esta disponible
	end if;
	return 0;
exception
		when no_data_found then 
			return 0;
end;
$$ language plpgsql;


-- corrección:  
=> titol no id, osea titol from document;
exemplar estat disponible AND not in (esta en prestec (datadev is null))

=> p_titol varchar
drop function codiExemplarDisponible(varchar);
create or replace function codiExemplarDisponible(p_titol varchar)
returns integer
as $$
declare
	v_idExemplar int;
begin
	-- s'obte el codi de l'exemplar
	select idExemplar
	into strict v_idExemplar
	from exemplar e
	inner join document d
	on e.idDocument = d.idDocument
	where lower(titol) = lower (p_titol)
	and lower(estat) = 'disponible'
	and idExemplar not in (	select idExemplar
							from prestec
							where datadev is null)
	limit 1; -- solo coje el primero
	-- ^ si el select falla se salta directamente a exception puenteando todo el codigo
	-- es comunica que s'ha trobat l'exemplar
	raise notice 'document troba amb id %', v_idExemplar;
	-- es retorna l'exemplar
	return v_idExemplar;
	-- exception cuan no es troba un exemplar 
	exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;


