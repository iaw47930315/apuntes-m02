--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca

-- exemplar disponible, joc de proves
\echo 'Joc de proves d''exemplar disponible';

\echo 'Exemplar amb alguns disponibles i altres en lloguer';
select codiExemplarDisponible('Highway to hell'); -- disponible 
select codiExemplarDisponible('Fundamentos de bases de datos'); -- disponible 
\echo 'Exemplar en lloguer'; 
select codiExemplarDisponible('La La Land'); -- no disponible  
\echo 'Exemplar mai en prestec i disponible'; 
select codiExemplarDisponible('Lonely Planet traveller');


-- prestecDocument joc de proves

\echo 'Joc de proves prestecDocument';

select prestecDocument(1,'highway to hell');
