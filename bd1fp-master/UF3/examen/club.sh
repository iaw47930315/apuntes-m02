#!/bin/bash
# Script: club.sh
# Description: script que gestiona base de dates club i relaccionats
# Date creation: 11/03/2020
# Use: ./club.sh

# iaw14270791
# club
# Jacint Iglesias
# UF3

NEUTRE='\033[0m'
VERD='\033[0;32m'
# resetejem la bd
echo -e "\n$VERD Reset de taules club$NEUTRE\n"
./examen.sql
# importem les funcions
echo -e "\n$VERD Funcions Club$NEUTRE\n"
./funcionsClub.sql
  # Apliquem un joc de proves per validar les funcions i taula
echo -e "\n$VERD Joc de proves$NEUTRE\n"
./jocProves.sql
