-- iaw14270791
-- funcions training
-- Jacint Iglesias
-- UF3
\c training

-- 0.1
-- crear sequencia

create  sequence cliecod_seq
start with 1 increment by 1;
select setval('cliecod_seq', (select max(cliecod) from cliente), true);

-- 1.

-- Funció		existeixClient
-- Paràmetres	p_cliecod
-- Tasca		comprova si existeix el client passat com argument
-- Retorna		booleà

create or replace function existeixclient(p_cliecod cliente.cliecod%type)
returns boolean
as $$
declare 
	v_clieccod cliente.cliecod%type;
begin
	select cliecod
	into strict v_clieccod
	from cliente
	where cliecod = p_cliecod;
	
	return true;
exception
		when no_data_found then 
			return  false;
end;
$$ language plpgsql;

select existeixclient(2111::smallint);


-- correcció


create or replace function existeixclient(p_cliecod cliente.cliecod%type)
returns boolean
as $$
declare 
	basura varchar(1);
begin
	select '1'
	into strict basura
	from cliente
	where cliecod = p_cliecod;
	
	return true;
exception
		when no_data_found then 
			return  false;
end;
$$ language plpgsql;

select existeixclient(2111::smallint);

-- correción sin into y con found
-- into + found
-- no hacer, no le gusta al profe, no equivvalente en oracle

create or replace function existeixclient(p_cliecod cliente.cliecod%type)
returns boolean
as $$
declare 
	basura varchar(1);
begin
	select '1'
	into basura
	from cliente
	where cliecod = p_cliecod;
	if found then
		--predicados: found
		return true;
	else
		return false;
	end if;
end;
$$ language plpgsql;

-- ^ en oracle no es equivalente, found y not found no equivalen


-- ^ simplificación de if y found v


create or replace function existeixclient(p_cliecod cliente.cliecod%type)
returns boolean
as $$
declare 
	basura varchar(1);
begin
	select '1'
	into basura
	from cliente
	where cliecod = p_cliecod;
	return found;
end;
$$ language plpgsql;




-- select existeixclient('2111'); -- funciona con comillas

-- .2
-- Funció		altaClient
-- Paràmetres	p_nombre ,p_repcod, p_limcred
-- Tasca		Donarà d’alta un client
-- Retorna		Missatge Client X s’ha donat d’alta correctament

-- Nota			si no està creada, creem una seqüència per donar valors 
-- 				a la clau primària. Començarà en el següent valor que hi 
--				hagi a la base de dades.


create or replace function altaClient(p_nombre cliente.nombre%type, p_repcod repventa.repcod%type, p_limcred cliente.limcred%type)
returns varchar(30)
as $$
begin
	insert into cliente (cliecod,nombre,repcod,limcred)
	values	(nextval('cliecod_seq'),p_nombre,p_repcod, p_limcred);	
	return 'Client '||p_nombre|| ' s’ha donat d’alta correctament';
end;
$$ language plpgsql;

select altaClient('Menganito', 105::smallint,  50000.00);


-- con excepcion

dup_val_on_index -- clau primaria repetido (oracle)


-- existeix repcod, faltaria añadir esta función para rizar el rizo









-- 3.
-- Funció		stockOk
-- Paràmetres	p_cant , p_fabcod,p_prodcod
-- Tasca		Comprova que hi ha prou existències del producte demanat.
-- Retorna		booleà

create or replace function stockOk(p_cant producto.exist%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type)
returns boolean
as $$
declare 
	v_exist producto.exist%type;
begin
	select exist
	into strict v_exist
	from producto
	where (fabcod||prodcod) = (p_fabcod||p_prodcod);
	if v_exist >= p_cant then
		return true;
	else
		return false;
	end if;
end;
$$ language plpgsql;

-- select stockOk(200,'rei','2a45c');
-- select stockOk(250,'rei','2a45c');



-- optimizada:
-- con found y exist = can EN el select

-- obliga a usar exceptions
create or replace function stockOk(p_cant producto.exist%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type)
returns boolean
as $$
declare 
	basura varchar(1);
begin
	select '1' 	
	into strict basura
	from producto
	where (fabcod||prodcod) = (p_fabcod||p_prodcod)
	and exist >= p_cant;
	return found;
	exception
		when no_data_found then 
			return  false;
end;
$$ language plpgsql;

-- 

-- sin into strict funciona por que no le importa la excepcion no la 
-- recoje
create or replace function stockOk(p_cant producto.exist%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type)
returns boolean
as $$
declare 
	basura varchar(1);
begin
	select '1' 	
	into  basura
	from producto
	where (fabcod||prodcod) = (p_fabcod||p_prodcod)
	and exist >= p_cant;
	return found;
end;
$$ language plpgsql;




-- 4.
-- Funció		altaComanda
-- Paràmetres	Segons els exercicis anteriors i segons necessitat, 
--				definiu vosaltres els paràmetres mínims que necessita la 
--				funció, tenint en compte que cal contemplar l'opció per 
--				defecte de no posar data, amb el què agafarà la data de 
--				sistema.
-- Tasca		Per poder donar d'alta una comanda es tindrà que 
--				comprovar que existeix el client i que hi ha prou 
--				existències. En aquesta funció heu d'utilitzar les 
--				funcions  existeixClient i stockOK (recordeu de no posar 
--				select function(... ). Evidentment, s'haura de calcular 
--				el preu de l'import en funció del preu unitari i de la 
--				quantitat d'unitats.


-- 4.0.1
--  create sequence

create  sequence pedido_seq
start with 1 increment by 1;
select setval('pedido_seq', (select max(pednum) from pedido), true);

create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
begin
	if stockOk(p_cant,p_fabcod,p_prodcod) AND existeixclient(p_cliecod) then
	
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		v_total_pedido := v_precio * p_cant;
		
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), current_date, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		return 'Pedido ejecutado con exito.'; 
		
	elsif stockOk(p_cant,p_fabcod,p_prodcod) then
		
		return 'El cliente con codigo ' ||p_cliecod|| ' no ha sido encontrado';
	
	elsif existeixclient(p_cliecod) then
		
		return 'No hay suficiente stock del producto de codigo ' ||p_fabcod||p_prodcod;
		
	end if;
	
	return 'No se ha podido completar el pedido';
	
end;
$$ language plpgsql;

-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint);



-- correccion current date p_data (hacer drop del anterior)

create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type, p_data date default current_date)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
begin
	if stockOk(p_cant,p_fabcod,p_prodcod) AND existeixclient(p_cliecod) then
	
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		v_total_pedido := v_precio * p_cant;
		
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), p_data, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		return 'Pedido ejecutado con exito.'; 
		
	elsif stockOk(p_cant,p_fabcod,p_prodcod) then
		
		return 'El cliente con codigo ' ||p_cliecod|| ' no ha sido encontrado';
	
	elsif existeixclient(p_cliecod) then
		
		return 'No hay suficiente stock del producto de codigo ' ||p_fabcod||p_prodcod;
		
	end if;
	
	return 'No se ha podido completar el pedido';
	
end;
$$ language plpgsql;

-- puliendo... 

--v_is_stock boolean;
--v_is_client boolean;
	

create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type, p_data date default current_date)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
begin
	if stockOk(p_cant,p_fabcod,p_prodcod) AND existeixclient(p_cliecod) then
		-- se captura el precio
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		-- precio totall por pedido
		v_total_pedido := v_precio * p_cant;
		
		-- se hace pedido
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), p_data, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		-- set actualiza stock
		update producto set exist = (exist - p_cant) where (p_fabcod||p_prodcod) = (fabcod||prodcod);
		
		--set actualizan las ventas de repcod
		update repventa set ventas = (ventas + 1) where repcod = p_repcod;
		
		
		-- se actualizan las ventas de oficina
		update oficina set ventas = ventas + v_total_pedido where ofinum = (select ofinum from repventa where repcod = p_repcod);
		
		
		return 'Pedido ejecutado con exito.'; 
		
	elsif stockOk(p_cant,p_fabcod,p_prodcod) then
		
		return 'El cliente con codigo ' ||p_cliecod|| ' no ha sido encontrado';
	
	elsif existeixclient(p_cliecod) then
		
		return 'No hay suficiente stock del producto de codigo ' ||p_fabcod||p_prodcod;
		
	end if;
	
	return 'No se ha podido completar el pedido.';
	
	exception
		when no_data_found then 
			return  'No se ha podido completar el pedido.';
end;
$$ language plpgsql;




-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint);

-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint,(current_date + interval '1 day')::date);

-- puliendo if


create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type, p_data date default current_date)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
	v_enough_stock boolean;
	v_client_existing boolean;
begin
	v_enough_stock := stockOk(p_cant,p_fabcod,p_prodcod);
	v_client_existing := existeixclient(p_cliecod);	if v_enough_stock AND v_client_existing then
		-- se captura el precio
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		-- precio totall por pedido
		v_total_pedido := v_precio * p_cant;
		
		-- se hace pedido
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), p_data, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		-- set actualiza stock
		update producto set exist = (exist - p_cant) where (p_fabcod||p_prodcod) = (fabcod||prodcod);
		
		--set actualizan las ventas de repcod
		update repventa set ventas = (ventas + 1) where repcod = p_repcod;
		
		
		-- se actualizan las ventas de oficina
		update oficina set ventas = ventas + v_total_pedido where ofinum = (select ofinum from repventa where repcod = p_repcod);
		
		
		return 'Pedido ejecutado con exito.'; 
	
	-- stock ok client false	
	elsif v_enough_stock then
		
		return 'El cliente con codigo % no ha sido encontrado',p_cliecod;
		
	end if;	
	
	-- client ok stock false
	if v_client_existing then
		
		return 'No hay suficiente stock del producto de codigo %', p_fabcod||p_prodcod;
		
	end if;
	
	return 'No se ha podido completar el pedido.';
	
	exception
		when no_data_found then 
			return  'No se ha podido completar el pedido.';
end;
$$ language plpgsql;


-- con raise exception


create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type, p_data date default current_date)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
	v_enough_stock boolean;
	v_client_existing boolean;
	v_fab_prod varchar(30);
begin
	v_enough_stock := stockOk(p_cant,p_fabcod,p_prodcod);
	v_client_existing := existeixclient(p_cliecod);
	if v_enough_stock AND v_client_existing then
		-- se captura el precio
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		-- precio totall por pedido
		v_total_pedido := v_precio * p_cant;
		
		-- se hace pedido
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), p_data, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		-- set actualiza stock
		update producto set exist = (exist - p_cant) where (p_fabcod||p_prodcod) = (fabcod||prodcod);
		
		--set actualizan las ventas de repcod
		update repventa set ventas = (ventas + 1) where repcod = p_repcod;
		
		
		-- se actualizan las ventas de oficina
		update oficina set ventas = ventas + v_total_pedido where ofinum = (select ofinum from repventa where repcod = p_repcod);
		
		
		return 'Pedido ejecutado con exito.'; 
	end if;
	-- stock ok client false	
	if not v_client_existing then
		
		raise notice 'El cliente con codigo % no ha sido encontrado',p_cliecod;
		
	end if;	
	
	-- client ok stock false
	if not v_enough_stock then
		v_fab_prod := p_fabcod||p_prodcod;
		raise notice 'No hay suficiente stock del producto de codigo %',v_fab_prod;
		
	end if;
	
	return 'No se ha podido completar el pedido.';
	
	exception
		when no_data_found then 
			return  'No se ha podido completar el pedido.';
end;
$$ language plpgsql;


select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint,(current_date + interval '1 day')::date);




-- message=""
-- if NOT v_enough_stock OR NOT v_client_existing then
    -- if not v_client_existing then
         -- message = no client
    -- if not  v_stock then
         -- message = message + no stock
-- return

 -- realizar alta

-- if optimitzat

create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type, p_data date default current_date)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
	v_enough_stock boolean;
	v_client_existing boolean;
	v_fab_prod varchar(30);
begin
	v_enough_stock := stockOk(p_cant,p_fabcod,p_prodcod);
	v_client_existing := existeixclient(p_cliecod);
	if not v_enough_stock OR not v_client_existing then
		
	-- condition messages
	
		-- stock ok client false	
		if not v_client_existing then
			
			raise notice 'El cliente con codigo % no ha sido encontrado',p_cliecod;
			
		end if;	
		
		-- client ok stock false
		if not v_enough_stock then
			v_fab_prod := p_fabcod||p_prodcod;
			raise notice 'No hay suficiente stock del producto de codigo %',v_fab_prod;
			
		end if;
		
		return 'No se ha podido completar el pedido.';
	
	
	end if;
	
	-- alternatively if all conditions are ok, execute order
	
	
	-- se captura el precio
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		-- precio totall por pedido
		v_total_pedido := v_precio * p_cant;
		
		-- se hace pedido
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), p_data, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		-- set actualiza stock
		update producto set exist = (exist - p_cant) where (p_fabcod||p_prodcod) = (fabcod||prodcod);
		
		--set actualizan las ventas de repcod
		update repventa set ventas = (ventas + v_total_pedido) where repcod = p_repcod;
		
		
		-- se actualizan las ventas de oficina
		update oficina set ventas = ventas + v_total_pedido where ofinum = (select ofinum from repventa where repcod = p_repcod);
		
		
		return 'Pedido ejecutado con exito.'; 
	
	
	
	
	
	exception
		when no_data_found then 
			return  'No se ha podido completar el pedido.';
			
			
			
			
end;
$$ language plpgsql;
-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint,(current_date + interval '1 day')::date); -- verything ok

-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',500::smallint,(current_date + interval '1 day')::date); -- fail stock

-- select altaComanda(21179::smallint,106::smallint,'rei','2a45c',1::smallint,(current_date + interval '1 day')::date); -- fail client

-- select altaComanda(21179::smallint,106::smallint,'rei','2a45c',500::smallint,(current_date + interval '1 day')::date); -- fail both



-- versión del profe  




if no client
	message
else
	if no stock
		message
	else
	 do 
	 message
return message;
