--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- funcions bibblioteca
-- Jacint Iglesias
-- UF3

-- correcció

-- funció codiExemplarDisponible
\c biblioteca
\echo 'codiExemplarDisponible5';

drop function codiExemplarDisponible5(varchar);
create or replace function codiExemplarDisponible5(p_titol varchar)
returns integer
as $$
declare
		cur_exemplars cursor for 
			select e.idexemplar miexemplar
			from exemplar e
			join document d
			on e.iddocument = d.iddocument
			join prestec p on e.idexemplar = p.idexemplar
			where lower(titol) = lower (p_titol)
			and lower(estat) = 'disponible'
			and idexemplar not in (select idexemplar -- aqui no se puede utilizar un join
									from prestec 	-- not in no tiene repemplazo
									where datadev is null)
			order by 1;
	v_exemplar int;
begin
	open cur_exemplars; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_exemplars INTO v_exemplar;
	close cur_exemplars;
	return coalesce(v_exemplar,0);
end;
$$ language plpgsql;



-- funcio get tipus
\echo 'getTipus';

drop function getTipus(varchar);
create or replace function getTipus(p_titol varchar)
returns varchar
as $$
declare
	v_tipusExemplar varchar;
begin	

if codiExemplarDisponible(p_titol) != 0 then
	select format
	into strict v_tipusExemplar
	from document d inner join
	exemplar e on e.iddocument = d.iddocument
	where lower(titol) = lower(p_titol)
	limit 1;
	return lower(v_tipusExemplar);
else
	return 0;
end if;
return 0;
exception
		when no_data_found then
			return 0;
end;
$$ language plpgsql;

-- funció prestecDocument

-- Per simplificació, considerarem que un usuari podrà tenir simultàniament un
-- màxim de 4 docs:

-- 2 docs del tipus llibre/revista
-- 1 DVD
-- 1 CD

-- er la funció prestecDocument, hem de tenir un exemplar disponible i 
-- evidentment un usuari que no tingui blocat el carnet degut a 
-- penalitzacions acumulades.

\echo 'prestecDocument2';




-- prestec document correcció
--
--

drop function prestecDocument2(integer,varchar);
create or replace function prestecDocument2(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
returns boolean
as $$
declare
	v_usuariBloquejat boolean;
	v_datadev date;
	v_totalLlibresIRevistes int = 0;
	v_totalCd int = 0;
	v_totalDvd int = 0;
	v_total_documents int = 0;
	v_totalLlibresIRevistes_prellogats int = 0;
	v_totalCd_prellogats int = 0;
	v_totalDvd_prellogats int = 0;
begin
	-- no esta blocat
	begin
		select bloquejat, datadesbloqueig
		into strict v_usuariBloquejat, v_datadev
		from usuari
		where idusuari = p_idusuari;
		if 	v_usuariBloquejat then
			raise notice 'Usuari %  esta bloquejat, data de desbloqueig %', p_idusuari,coalesce(v_datadev::varchar,'(data desconeguda)');
			return false;
		end if;
		-- codi per l'excepció (usuari no trobat o bloquejat)
		exception
			when no_data_found then
				raise notice 'Usuari % no esta donat d''alta', p_idusuari;
				return false;
	end;
	-- comprovem el tipus d'exemplar i que esta disponible
	case 
		when getTipus(p_titol) in ('revista','llibre') then
			v_totalLlibresIRevistes := v_totalLlibresIRevistes + 1;
		when getTipus(p_titol) = 'cd' then
			v_totalCd := v_totalCd + 1;
		when getTipus(p_titol) = 'dvd' then
			v_totalDvd := v_totalDvd + 1;
		else
			raise info 'Document % no disponible.',p_titol;
			return false;
	end case;
	
	-- control de documents previament llogats, quantitat
	-- llibres/revistes
	begin
		select
				sum(case when lower(format) in ('llibre','revista') then 1 else 0 end) llibres,
				sum(case when lower(format) = 'cd' then 1 else 0 end) cds,
				sum(case when lower(format) = 'dvd' then 1 else 0 end) pelis
		into strict v_totalLlibresIRevistes_prellogats,v_totalCd_prellogats,v_totalDvd_prellogats
		from document d
		join exemplar e on e.iddocument= d.iddocument
		join prestec p on p.idexemplar = e.idexemplar
		where idusuari = p_idusuari and
		datadev is null;
		exception
			when no_data_found then
				raise notice 'Error: problema amb el recompte de documents prellogats.';
	end;
	-- afegim prellogats a llibres a llogar
	v_totalLlibresIRevistes := 	v_totalLlibresIRevistes + v_totalLlibresIRevistes_prellogats;
	v_totalCd := v_totalCd + v_totalCd_prellogats;
	v_totalDvd := v_totalDvd + v_totalDvd_prellogats;
	-- comprovem la quantitat de numero de documents disponibles esta 
	-- d'acord amb el numero maxim de documents (2 llibre/revista,
	-- 1 DVD, 1 CD)
	-- de lo contrari retorna false i fa un raise notice informant
	if v_totalLlibresIRevistes > 2 OR v_totalCd > 1 OR v_totalDvd > 1 then
		raise notice 'Nomes es poden llogar 2 llibres/revistes, 1 dvd i 1 cd. Has tractat de llogar el següent:';
		raise notice '% llibres/revistes: ',v_totalLlibresIRevistes;
		raise notice '% cds: ',v_totalCd;
		raise notice '% dvds: ',v_totalDvd;
		raise notice 'Titols dels documents: %',p_titol;
		return false;
	end if;
	-- s'efectua el lloguer
	insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesRenovat)
	values	(codiExemplarDisponible(p_titol),current_timestamp,null,p_idusuari,0);
	-- informació
	raise notice 'En prestec el document %',p_titol;
	v_total_documents := v_totalLlibresIRevistes + 	v_totalCd + v_totalDvd;
	raise notice '% documents en prestec.',v_total_documents;
	raise notice '% llibres/revistes.',v_totalLlibresIRevistes;
	raise notice '% CDs.',v_totalCd;
	raise notice '% Pelicules.',v_totalDvd;
	return v_total_documents > 0;
end;
$$ language plpgsql;





-- ** funció marcarReserva **
-- Quan es retorni un exemplar, abans de que quedi disponible per prèstec per altres
-- usuaris, s'haurà de comprovar que no l'esperi cap usuari, és a dir, que altre usuari
-- el tingui reservat. Si fos el cas, l'estat de l'exemplar haurà de passar a reservat
-- (amb el que quedarà immediatament fora de la llista d'exemplars disponibles).
-- Com que l'exemplar ja està disponible per l'usuari que l'havia reservat, se marca
-- la data d'avís a la reserva.

-- ** funció marcarReserva **


\echo 'marcarReserva';

drop function marcarReserva(integer);
create or replace function marcarReserva(p_idexemplar integer) 
returns void
as $$
declare
	datos_reserva record;
begin
	begin
		select idexemplar,idusuari,datareserva
		into strict datos_reserva
		from reserva 
		where idexemplar = p_idexemplar
		order by 3
		limit 1;
		exception 
			when no_data_found then
				raise info 'Error: el document no esta en reserva.';
	end;
	
	-- realitzem la reserva del document
	update exemplar
	set estat = 'Reservat'
	where idexemplar = p_idexemplar;
	
	update reserva 
	set dataavis = currennt_date
	where idexemplar = p_idexemplar 
	and datareserva = datos_reserva.datareserva;
	raise notice 'Documento % reservado', datos_reserva.idexemplar;

end;
$$ language plpgsql;




-- Retorn de prèstecs i penalització (sistema de punts)
-- funció retornarDocument
-- Estudieu les taules usuari i penalització.
-- Per al bon funcionament del servei de préstec et recordem que has de ser puntual
-- a l'hora de retornar els documents. Per cada dia de retard i document en
-- préstec, el teu carnet rebrà 1 punt de penalització. Per cada 50 punts, el
-- carnet quedarà bloquejat durant 15 dies. Passat aquest temps, el carnet tornarà
-- a estar operatiu.
-- Quan un usuari superi els 50 punts de demèrit, és reiniciarà el comptador de
-- punts.
-- Hi ha llibres que no es presten (són  només de consulta a la biblioteca).
-- Si l'exemplar que es retorna està reservat, NO ha d'aparèixer com a disponible
-- pel públic en general.

-- 1 punt penalització per dia tard
-- 50 punts per bloqueig
-- 15 dies bloqueig
-- despres de 15 dies punts a 0

-- documents que no son de prestec

-- exemplat retornat que esta reservat => no disponible => Reservat

-- data devolució 30 dies, renovació 30 dies
\echo 'retornarDocument';

drop function retornarDocument(integer,varchar);
create or replace function retornarDocument(	p_idusuari usuari.idusuari%type, p_titol document.titol%type) -- parametre formal
returns boolean
as $$
declare
	v_titol boolean;
	v_days int;
	v_idexemplar int;
	v_punts int;
	v_extra_days int := 0;
	v_total_punts int;
	v_basura int;
begin
-- condició d'existencia d'usuari
begin
	select u.idusuari
	into strict v_basura
	from usuari u
	join prestec p on u.idusuari = p.idusuari
	where u.idusuari = p_idusuari
	and datadev is null
	limit 1;
	exception
		when no_data_found then
			raise info 'Usuari no existeix o no te documents en prestec.';
			return false;
end;
-- comprovar que el document esta en prestec
begin
select lower(d.titol) = lower(p_titol) 
into strict v_titol
from document d
join exemplar e on e.iddocument = d.iddocument
join prestec p on p.idexemplar = e.idexemplar
where datadev is null and
p.idusuari = p_idusuari;
-- condició de document no trovat en prestec
if v_titol is null then
		raise notice 'Aquest document no esta en prestec.';
		return false;
end if;
exception
	when too_many_rows then
		raise info 'Error: Mes d''un exemplar del mateix document en prestec. Retornant el primer';
end;
-- obtenim el numero d'exemplar
begin
select p.idexemplar
into strict v_idexemplar
from prestec p
join exemplar e on p.idexemplar = e.idexemplar 
join document d on d.iddocument = e.iddocument
where p.idusuari = p_idusuari and
lower(titol) = lower(p_titol) and
datadev is null
order by 1 limit 1;
-- control d'error per document en prestec no trobat
exception
	when no_data_found then
		raise notice 'Error, no s''ha trobat l''exemplar en prestec.';
		return false;
end;

-- comprovació de dies mes tard de datadev
-- obtenció de total dies de prestec
select sum(to_char(current_timestamp - datapres,'dd')::int)::int
into v_days
from prestec
where idusuari = p_idusuari
and datadev is null
and idexemplar = v_idexemplar;
-- control d'errors si data prestec es null
if v_days is null then
	raise notice 'Hi ha un error amb la data de prestec, no es troba al sistema.';
	return false;
end if;
--aplicació de penalitzacións
if v_days > 30 then
	v_extra_days := v_days - 30;
	-- control de punts actuals
	begin
		select punts::int
		into strict v_punts
		from usuari
		where idusuari = p_idusuari;
		exception
			when no_data_found then
			raise info 'Error amb els punts d''usuari, no trobats';
			return false;
	end;
	
	-- calcul de punts
	v_total_punts := v_punts + v_extra_days;
	case v_total_punts >= 50
		when true then
			v_extra_days := 50;
		when false then
			v_extra_days := v_total_punts;
		else
			raise info 'Error amb calcul de punts';
			return false;
	end case;
	-- insert/update a penalització de punts
	begin	
		select idusuari
		into strict v_basura
		from penalitzacio
		where idusuari = p_idusuari
		limit 1;
		-- si l'usuari ja esta definit a penalització es fa un update
		update penalització set demerit = v_extra_days
		where idusuari = p_idusuari and
		idexemplar = v_idexemplar;
		-- si no esta definit es fa un insert
		exception
			when no_data_found then
				insert into penalitzacio (idusuari, idexemplar, data, demerit)
				values (p_idusuari,v_idexemplar,current_timestamp,v_extra_days);
	end;
	-- bloquejar si es necesari
	if v_extra_days = 50 then
		update usuari 
		set bloquejat = true,
		datadesbloqueig = (current_date + interval '15 days')::date,
		punts = case when (punts + v_extra_days) > 50 then 50 else(punts + v_extra_days) end
		where idusuari = p_idusuari;
		raise info 'Usuari bloquejat, data desbloqueig en 15 dies des d''avui';
	end if;
end if;
-- s'efectua el retorn de document
update prestec
set datadev = current_timestamp
where idusuari = p_idusuari and
idexemplar = v_idexemplar;
raise info 'S''ha efectuat la devolució';
-- marcar com a reservat
-- v_basura2 := marcarReserva(v_idexemplar); <= hay que hacer return void
marcarReserva(v_idexemplar);
return true;
end;
$$ language plpgsql;


-- funció renovarDocument
-- Es poden renovar fins a tres vegades, la qual cosa es pot fer en persona,
-- telèfon o internet.
-- Els documents es poden renovar sempre i quan:

-- no s'hagin renovat ja tres vegades
-- no hagi expirat la data de retorn
-- no es tingui el carnet de préstec bloquejat per algun motiu
-- no estiguin reservats per un altre usuari

-- La renovació és de 30 dies naturals sobre la data de prèstec.

\echo 'renovarDocument';

drop function renovarDocument(integer);
create or replace function renovarDocument(	p_idexemplar exemplar.idexemplar%type) -- parametre formal
returns varchar
as $$
declare
	v_message varchar(300) := 'Document renovat amb exit.';
	v_vegadesrenovat int;
	v_dies varchar;
	v_bloquejat boolean;
	v_estat varchar;
	v_MAXDIES int := 35;
	v_idusuari int;
begin
	begin
	-- obtenció d'usuari
		select idusuari 
		into strict v_idusuari
		from prestec
		where idexemplar = p_idexemplar
		and datadev is null;
		exception
			when no_data_found then
				v_message := 'No s''ha trovat l''usuari.';
	end;
	begin
	-- recollida de variables pe'l control de renovació
		select vegadesrenovat, to_char((current_timestamp) - (datapres),'dd')::int, bloquejat, estat
		into strict v_vegadesrenovat, v_dies, v_bloquejat, v_estat 
		from prestec p join usuari u on p.idusuari = u.idusuari
		join  exemplar e on p.idexemplar = e.idexemplar
		where p.idexemplar = p_idexemplar
		and datadev is null
		and p.idusuari = v_idusuari;
		exception
			when no_data_found then
				v_message := 'Error: No s''ha trobat el document a retornar.';
				
	end;
	case
		-- opcions que eviten la renovació
		when v_vegadesrenovat > 2 then
			v_message := 'Error: El document s''ha renovat ja 3 vegades i no es pot renovar.';
		when v_dies::int >= v_MAXDIES then
			v_message := 'Error: El document no es pot renovar, fora de plaç';
		when v_bloquejat then
			v_message := 'Error: L''usuari esta bloquejat i no pot renovar.';
		when lower(v_estat) = 'reservat' then
			v_message := 'Error: El document esta reservat i no es pot renovar';
		else
			-- opció on s'efectua la renovació
			update prestec 
			set vegadesrenovat = vegadesrenovat +1,
				datapres = current_timestamp
			where idexemplar = p_idexemplar
			and idusuari = v_idusuari
			and datadev is null;
	end case;
	return v_message;
end;
$$ language plpgsql;


-- Reservar document
-- funció reservarDocument
-- Estudieu la taula reserva.
-- Quan es retorni un document que està reservat, l'usuari rebrà un avís per
-- telèfon o per correu electrònic per passar a recollir-lo en els propers
-- set dies a la biblioteca.

\echo 'reservarDocument';
drop function reservarDocument(integer,integer);
create or replace function reservarDocument(p_iddocument document.iddocument%type,p_idusuari integer) -- parametre formal
returns varchar
as $$
declare
	v_basura int;
	v_sameuser boolean;
	v_idexemplar int;
	v_message varchar(250) := 'Document reservat';
begin

	-- are all documments being rented?
	-- are all documments available
	-- usuari es nou => un usuari diferent al que esta retornant al que esta reservant [check]  
	
	-- 
	
	-- hacer un select en prestec i ver si esta en prestec
	--
	-- el tema es que esta funcion se ejecuta despues de llogarExemplar returne 0
	/*
	select idusuari
	into strict v_idusuari
	from prestec
	where idexemplar = p_idexemplar
	and datadev is null;
	exception
		when no_data_found then
			raise notice 'Error: no es pot reservar el coument per que hi han exemplars disponibles.';
			return false;
	*/
	
	
	/*
	case 
	when usuaribloquejat ...
	when v_idusuari = p_idusuari
		mateix usuari
	else
		hacerlo
	
	
	*/
	
	select idexemplar 
	into v_basura
	from exemplar 
	where iddocument = p_iddocument
	and lower(estat) in ('disponible','reservat')
	except 
	select p.idexemplar 
	from prestec p join exemplar e on e.idexemplar = p.idexemplar 
	where e.iddocument = p_iddocument
	and lower(estat) in ('disponible','reservat')
	limit 1;


	select p_idusuari in (	select idusuari
					from prestec p join exemplar e on p.idexemplar = e.idexemplar
					where datadev is null
					and iddocument = p_iddocument)
	into v_sameuser ;
	
	select p.idexemplar
	into v_idexemplar
	from prestec p join exemplar e on e.idexemplar = p.idexemplar
	where e.iddocument = p_iddocument
	and datadev is null
	limit 1;

	case 
		when v_basura is not null then
			v_message := 'Error: No es pot reservar el document per que hi han exemplars disponibles per a prestec.';
		when v_sameuser then
			v_message := 'Error: L''usuari no pot reservar un document que ell mateix te en prestec.';
		else
			-- efectuar la reserva
			insert into reserva(idexemplar,idusuari,datareserva)
				values (v_idexemplar,p_idusuari,current_date);
			v_message := v_message || '. Codi exemplar: ' || v_idexemplar;
	end case;
	return v_message;
end;
$$ language plpgsql;



-- Recollir la reserva de l'exemplar
-- funció recollirReserva
-- Aquesta funció eliminarà la reserva i la transformarà en un prèstec.


\echo 'recollirReserva';
drop function recollirReserva(integer,integer);
create or replace function recollirReserva(p_idexemplar integer,p_idusuari integer) -- parametre formal
returns varchar
as $$
declare
	v_idexemplar int;
	v_idusuari int;
	v_estat varchar(35);
	v_datadev timestamp;
	v_encarareservat int;
	v_message varchar(250) := 'Exemplar reservat recoolit. Exemplar numero: ' || p_idexemplar || ', usuari: '|| p_idusuari;
begin
	-- comprovar dataavis is not null <= 
	
	begin
	--comrpovar si existeix l'usuari
	select idusuari
	from usuari
	where idusuari = p_idusuari;
	exception
		when no_data_found then
			return 'L''usuari ' || p_idusuari || ' no existeix.';
	end;
	begin
	--comrpovar si el document existeix
	select idexemplar
	from exemplar
	where idexemplar = p_idexemplar;
	exception
		when no_data_found then
			return 'El document ' || p_idexemplar || ' no existeix.';
	end;
	-- veure si el document esta reservat i no esta prestat
	select r.idexemplar,r.idusuari, estat, datadev
	into v_idexemplar,v_idusuari, v_estat, v_datadev
	from reserva r join exemplar e on e.idexemplar = r.idexemplar
	join prestec p on e.idexemplar = p.idexemplar
	where r.idexemplar = p_idexemplar
	and r.idusuari = p_idusuari
	and r.idexemplar not in (	select idexemplar
								from prestec 
								where datadev is null
								and idexemplar = p_idexemplar);
	
	case 
		when v_idexemplar is null then
			v_message := 'El document es trova en prestec.';
		when lower(v_estat) != 'reservat' then
			v_message := 'El document no es trova reservat.';
		else
			--'sefectua el cambi de reserva a prestec
			delete from reserva 
			where idexemplar = p_idexemplar
			and idusuari = p_idusuari;
			-- s'efectua el prestec
			insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesrenovat)
				values(p_idexemplar,current_timestamp,null,p_idusuari,0);
			-- es revisa que no continui en resrva per altre usuari
			select idexemplar 
			into v_encarareservat
			from reserva 
			where idexemplar = p_idexemplar
			limit 1;
			if v_encarareservat is null then
				update exemplar
					set estat = 'Disponible'
				where idexemplar = p_idexemplar;
			end if;
		end case;
	return v_message;
end;
$$ language plpgsql;
