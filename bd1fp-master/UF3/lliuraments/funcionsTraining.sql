-- Jacint Iglesias
-- iaw14270791
-- funcions training
-- UF3
\c training

-- 0.1
-- crear sequencia

create  sequence cliecod_seq
start with 1 increment by 1;
select setval('cliecod_seq', (select max(cliecod) from cliente), true);

-- 1.

-- Funció		existeixClient
-- Paràmetres	p_cliecod
-- Tasca		comprova si existeix el client passat com argument
-- Retorna		booleà

create or replace function existeixclient(p_cliecod cliente.cliecod%type)
returns boolean
as $$
declare 
	v_clieccod cliente.cliecod%type;
begin
	select cliecod
	into strict v_clieccod
	from cliente
	where cliecod = p_cliecod;
	
	return true;
exception
		when no_data_found then 
			return  false;
end;
$$ language plpgsql;

-- select existeixclient(2111::smallint);
-- select existeixclient('2111'); -- funciona con comillas

-- .2
-- Funció		altaClient
-- Paràmetres	p_nombre ,p_repcod, p_limcred
-- Tasca		Donarà d’alta un client
-- Retorna		Missatge Client X s’ha donat d’alta correctament

-- Nota			si no està creada, creem una seqüència per donar valors 
-- 				a la clau primària. Començarà en el següent valor que hi 
--				hagi a la base de dades.


create or replace function altaClient(p_nombre cliente.nombre%type, p_repcod repventa.repcod%type, p_limcred cliente.limcred%type)
returns varchar(30)
as $$
begin
	insert into cliente (cliecod,nombre,repcod,limcred)
	values	(nextval('cliecod_seq'),p_nombre,p_repcod, p_limcred);	
	return 'Client '||p_nombre|| ' s’ha donat d’alta correctament';
end;
$$ language plpgsql;

-- select altaClient('Menganito', 105::smallint,  50000.00);

-- 3.
-- Funció		stockOk
-- Paràmetres	p_cant , p_fabcod,p_prodcod
-- Tasca		Comprova que hi ha prou existències del producte demanat.
-- Retorna		booleà

create or replace function stockOk(p_cant producto.exist%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type)
returns boolean
as $$
declare 
	v_exist producto.exist%type;
begin
	select exist
	into strict v_exist
	from producto
	where (fabcod||prodcod) = (p_fabcod||p_prodcod);
	if v_exist >= p_cant then
		return true;
	else
		return false;
	end if;
end;
$$ language plpgsql;

-- select stockOk(200,'rei','2a45c');
-- select stockOk(250,'rei','2a45c');

-- 4.
-- Funció		altaComanda
-- Paràmetres	Segons els exercicis anteriors i segons necessitat, 
--				definiu vosaltres els paràmetres mínims que necessita la 
--				funció, tenint en compte que cal contemplar l'opció per 
--				defecte de no posar data, amb el què agafarà la data de 
--				sistema.
-- Tasca		Per poder donar d'alta una comanda es tindrà que 
--				comprovar que existeix el client i que hi ha prou 
--				existències. En aquesta funció heu d'utilitzar les 
--				funcions  existeixClient i stockOK (recordeu de no posar 
--				select function(... ). Evidentment, s'haura de calcular 
--				el preu de l'import en funció del preu unitari i de la 
--				quantitat d'unitats.


-- 4.0.1
--  create sequence

create  sequence pedido_seq
start with 1 increment by 1;
select setval('pedido_seq', (select max(pednum) from pedido), true);

create or replace function altaComanda(p_cliecod cliente.cliecod%type, p_repcod repventa.repcod%type, p_fabcod producto.fabcod%type, p_prodcod producto.prodcod%type, p_cant producto.exist%type)
returns varchar(50)
as $$
declare 
	v_precio producto.precio%type;
	v_total_pedido producto.precio%type;
begin
	if stockOk(p_cant,p_fabcod,p_prodcod) AND existeixclient(p_cliecod) then
	
		select precio
		into strict v_precio
		from producto
		where (fabcod||prodcod) = (p_fabcod||p_prodcod);
		
		v_total_pedido := v_precio * p_cant;
		
		insert into pedido (pednum,fecha,cliecod,repcod,fabcod,prodcod,cant,importe)
		values (nextval('pedido_seq'), current_date, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_total_pedido);
		
		return 'Pedido ejecutado con exito.'; 
		
	elsif stockOk(p_cant,p_fabcod,p_prodcod) then
		
		return 'El cliente con codigo ' ||p_cliecod|| ' no ha sido encontrado';
	
	elsif existeixclient(p_cliecod) then
		
		return 'No hay suficiente stock del producto de codigo ' ||p_fabcod||p_prodcod;
		
	end if;
	
	return 'No se ha podido completar el pedido';
	
end;
$$ language plpgsql;

-- select altaComanda(2117::smallint,106::smallint,'rei','2a45c',1::smallint);

-- 5.
-- Funció			preuSenseIVA
-- Paràmetres		p_precio (preu amb IVA)
-- Tasca			Donat un preu amb IVA, es calcularà el preu sense 
--					IVA (es considera un 21 % d'IVA) i serà retornat.


create or replace function preuSenseIVA(p_preu_amb_iva numeric(9,2))
returns numeric(9,2)
as $$
begin
	return round(p_preu_amb_iva / 1.21,2);
end;
$$ language plpgsql;

-- 6.
-- Funció			preuAmbIVA
-- Paràmetres		p_precio (preu sense IVA)
-- Tasca			Donat un preu sense IVA, es calcularà el preu amb 
--					IVA (es considera un 21 % d'IVA) i serà retornat.

create or replace function preuAmbIVA(p_preu_sense_iva numeric(9,2))
returns numeric(9,2)
as $$
begin
	return round(p_preu_sense_iva * 1.21,2);
end;
$$ language plpgsql;
