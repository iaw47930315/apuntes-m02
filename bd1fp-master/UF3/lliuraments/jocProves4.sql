--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3

\c biblioteca
\echo '---------------------------------';
\echo 'Joc de proves de reservarDocument';
\echo '---------------------------------';
-- reservarDocument joc de proves

-- actualment reservat
-- data reserva fa menys de 29 dies
update reserva 
set datareserva = current_date -29 
where idexemplar = 2 and idusuari  = 1;

insert into reserva (idexemplar, idusuari,datareserva,dataavis)
values(2,3,current_date -29,null),(3,4,current_date -29,null);

\echo '< 30 dies desde la reserva';

select reservarDocument(1,1);

\echo '> 30 dies desde la reserva';

select reservarDocument(1,2,current_date + 2);
select * from reserva;
