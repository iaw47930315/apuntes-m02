-- iaw14270791
-- funcions dates
-- Jacint Iglesias
-- UF3
\c bdfuncions


-- 1. funció myEpoch()

create or replace function myEpoch()
returns varchar
as $$
	declare
		interval_ts varchar;
		result_time varchar;
	begin
		interval_ts := (current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::text;
		result_time := (date_part('days',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 * 60 * 24 + date_part('hours',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 * 60 + date_part('minutes',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric * 60 + date_part('seconds',current_timestamp - to_date('01/01/1970','dd/mm/yyyy'))::numeric - 3600)::text;
		return result_time;
	end;
$$ language plpgsql;



-- correcció

create or replace function myEpoch()
returns bigint
as $$
	begin
		
		return 	to_char(current_timestamp - timestamp '1970-01-01 00:00:00','dd')::int * 60 * 60 * 24 +
				to_char(current_timestamp,'hh24')::int * 60 * 60 +
				to_char(current_timestamp,'mi')::int * 60 + 
				to_char(current_timestamp,'ss')::int -
				(60 * 60); 
	end;
$$ language plpgsql;

-- en oracle to_date('1970-01-01 00:00:00'...)






-- 2. funció darrerDiaMes()

create or replace function darrerDiaMes()
returns varchar
as $$
	begin
		return to_char((current_date + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes();



-- correcció


create or replace function darrerDiaMes(p_mes integer DEFAULT to_char(current_date,'mm')::int)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
	begin
	myYear := '/'||to_char(current_date,'yyyy');
	myDay := to_char(current_date,'dd')||'/';
	return to_char((to_date(myDay||p_mes::varchar||myYear,'dd/mm/yyyy') + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes();


-- problems with multiple functions with the same name


-- month + 1 (pero controlando si el mes es 12, etc)









-- 3. funció darrerDiaMes2()

create or replace function darrerDiaMes2(myMonth numeric)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
	begin
	myYear := '/'||to_char(current_date,'yyyy');
	myDay := to_char(current_date,'dd')||'/';
	return to_char((to_date(myDay||myMonth::text||myYear,'dd/mm/yyyy') + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes2(2);


-- 4. funció darrerDiaMes3()

create or replace function darrerDiaMes3(myMonth varchar)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
		myMonthParsed int;
	begin
	if lower(myMonth) in ('enero','january') then
		myMonthParsed := 1;
	elsif lower(myMonth) in ('febrero','february') then
		myMonthParsed := 2;
	elsif lower(myMonth) in ('marzo','march') then
		myMonthParsed := 3;
	elsif lower(myMonth) in ('abril','april') then
		myMonthParsed := 4;
	elsif lower(myMonth) in ('mayo','may') then
		myMonthParsed := 5;
	elsif lower(myMonth) in ('junio','june') then
		myMonthParsed := 6;
	elsif lower(myMonth) in ('julio','july') then
		myMonthParsed := 7;
	elsif lower(myMonth) in ('agosto','august') then
		myMonthParsed := 8;
	elsif lower(myMonth) in ('setiembre','september') then
		myMonthParsed := 9;
	elsif lower(myMonth) in ('octubre','october') then
		myMonthParsed := 10;
	elsif lower(myMonth) in ('noviembre','november') then
		myMonthParsed := 11;
	elsif lower(myMonth) in ('diciembre','december') then
		myMonthParsed := 12;
	else
		return 'Wrong input for month, valid months are spelled in english or spanish.';
	end if;
	myYear := '/'||to_char(current_date,'yyyy');
	myDay := to_char(current_date,'dd')||'/';
	return to_char((to_date(myDay||myMonthParsed||myYear,'dd/mm/yyyy') + interval '1 month')::date - (to_char(current_date,'dd'))::int,'dd');
	end;
$$ language plpgsql;

select darrerDiaMes3('March');



-- correcció



create or replace function darrerDiaMes3(myMonth varchar)
returns varchar
as $$
	declare
		myYear varchar;
		myDay varchar;
		myMonthParsed int;
	begin
		CASE 
			WHEN lower(myMonth) in ('january','enero') THEN
					myMonthParsed := 1;
			WHEN lower(myMonth) in ('febrero','february') THEN
					myMonthParsed := 2;
			WHEN lower(myMonth) in ('marzo','march') THEN
					myMonthParsed := 3;
			WHEN lower(myMonth) in ('abril','april') THEN
					myMonthParsed := 4;
			WHEN lower(myMonth) in ('mayo','may') THEN
					myMonthParsed := 5;
			WHEN lower(myMonth) in ('junio','june') THEN
					myMonthParsed := 6;
			WHEN lower(myMonth) in ('julio','july') THEN
					myMonthParsed := 7;
			WHEN lower(myMonth) in ('agosto','august') THEN
					myMonthParsed := 8;
			WHEN lower(myMonth) in ('setiembre','september') THEN
					myMonthParsed := 9;
			WHEN lower(myMonth) in ('octubre','october') THEN
					myMonthParsed := 10;
			WHEN lower(myMonth) in ('noviembre','november') THEN
					myMonthParsed := 11;
			WHEN lower(myMonth) in ('diciembre','december') THEN
					myMonthParsed := 12;
			ELSE
				return 'Out of range';
			END CASE;
	return darrerDiaMes2(myMonthParsed);
	end;
$$ language plpgsql;

select darrerDiaMes3('March');




-- ^ utilizar la función anterior, utilizar un case en vez de un if, es
-- mas sintacticamente correcto



-- 4. funció darrerDiaMesV2()
-- v corrección (eliminar parametro de entrada y incluir un parametro 
-- en la llamada de la función)
create or replace function darrerDiaMesV2()
returns varchar
as $$
	begin
		return darrerDiaMes2(to_char(current_date,'mm')::numeric);
	end;
$$ language plpgsql;

select darrerDiaMesV2(2);





