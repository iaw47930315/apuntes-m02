#!/bin/bash
# Script: funcionsCadena.sh
# Description: script que aplica joc de proves per validar els exercicis de cadenes
# Date creation: 11/03/2020
# Use: ./funcionsCadena

# iaw14270791
# funcions de cadenes
# Jacint Iglesias
# UF3

NEUTRE='\033[0m'
VERD='\033[0;32m'
  # Carreguem les funcions a la BD bdfuncions
psql -d template1 -f funcionsDates.sql
  # Apliquem un joc de proves per validar les funcions i taula
echo -e "\n$VERD TEST Exercici 1$NEUTRE\n"
psql -d bdfuncions -c "select myEpoch()"
psql -d bdfuncions -c "SELECT EXTRACT(EPOCH FROM current_timestamp) epoch"

echo -e "\n$VERD TEST Exercici 2$NEUTRE\n"
psql -d bdfuncions -c "select darrerDiaMes()"

echo -e "\n$VERD TEST Exercici 3$NEUTRE\n"
psql -d bdfuncions -c "select darrerDiaMes2(2)"

echo -e "\n$VERD TEST Exercici 4$NEUTRE\n"
psql -d bdfuncions -c "select darrerDiaMes3('March')"

echo -e "\n$VERD TEST Exercici 5$NEUTRE\n"
psql -d bdfuncions -c "select darrerDiaMesV2(2)"
