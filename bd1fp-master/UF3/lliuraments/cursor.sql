--() { :; }; exec psql biblioteca -f "$0"

-- iaw14270791
-- funcions cursors
-- Jacint Iglesias
-- UF3

-- Ejercicios. A la bd biblioteca. Mostrar id ejemplar, id documento, 
-- titulo, (nombre y apellidos en una unica columna). Provar while, 
-- loop y for. Un solo cursor, un solo codigo, 3 loops.  

\c biblioteca

create or replace function mostrarInfo()
returns varchar
as $$ 
	declare
		cur_usuari cursor for 
			select 	e.idexemplar miexemplar,
					e.iddocument midocument,
					titol,
					nom||' '||cognoms nom 
			from prestec p
			join exemplar e on p.idexemplar = e.idexemplar
			join usuari u on p.idusuari = u.idusuari
			join document d on d.iddocument = e.iddocument
			order by 1; 
	v_prestec record;
begin
	open cur_usuari; -- executa la consulta del cursor (el cursor es carregara de files)
	fetch cur_usuari INTO v_prestec;
	-- found / not found
	raise info 'while loop:';
	while found -- en oracle => cur_empleats found
		loop
			raise info 'idexemplar: %,iddocument: %,titol: %,nom: %',v_prestec.miexemplar,v_prestec.midocument,v_prestec.titol,v_prestec.nom;
			-- falta avanzar el cursor una posición V
			fetch cur_usuari INTO v_prestec;
		end loop;
	close cur_usuari;
	raise info '';
	raise info 'do while loop:';
	-- do while
	open cur_usuari;
	 LOOP
    -- fetch row into the film
      FETCH cur_usuari INTO v_prestec;
     
    -- exit when no more row to fetch
      EXIT WHEN NOT FOUND;
      raise info 'idexemplar: %,iddocument: %,titol: %,nom: %',v_prestec.miexemplar,v_prestec.midocument,v_prestec.titol,v_prestec.nom;
   END LOOP;
   close cur_usuari;
   raise info '';
   raise info 'for loop';
   -- for
   for v_prestec in cur_usuari
	   LOOP
			raise info 'idexemplar: %,iddocument: %,titol: %,nom: %',v_prestec.miexemplar,v_prestec.midocument,v_prestec.titol,v_prestec.nom;
		end loop;
	return 'ok';
end;
$$ language plpgsql;

select mostrarInfo();
