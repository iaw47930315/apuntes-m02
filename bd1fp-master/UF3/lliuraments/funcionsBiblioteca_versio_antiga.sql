-- iaw14270791
-- funcions bibblioteca
-- Jacint Iglesias
-- UF3

-- funcio docuemntDisponible

create or replace function docuemntDisponible(p_iddocument document.iddocument%type, p_tipus varchar(50)) --  puentear smallint
returns boolean
as $$
declare 
	v_iddocument  document.iddocument%type;
begin
	-- storing valid id
	case 
		when lower(p_tipus) = 'llibre' then
			select iddocument
			into strict v_iddocument
			from llibre
			where iddocument = p_iddocument;
			return  true;
		when lower(p_tipus) = 'revista' then
			select iddocument
			into strict v_iddocument
			from revista
			where iddocument = p_iddocument;
			return  true;
		when lower(p_tipus) = 'musica' then
			select iddocument
			into strict v_iddocument
			from musica
			where iddocument = p_iddocument;
			return  true;
		when lower(p_tipus) = 'pelicula' then
			select iddocument
			into strict v_iddocument
			from pelicula
			where iddocument = p_iddocument;
			return  true;
		else
			return false; -- es descarta taula falsa i id document f
	end case;
exception
		when no_data_found then 
		return false;
end;
$$ language plpgsql;

-- select docuemntDisponible(1,'llibre');
-- select docuemntDisponible(4,'revista');

-- select docuemntDisponible(2,'llibre');

-- select docuemntDisponible(1,'nave espacial');


-- versió amb taula temporal ???

-- funció prestecDocument
-- La normativa de les biblioteques de la Diputació de Barcelona és que es poden
-- tenir en prèstec d'un màxim 30 documents simultànis, concretament:

-- 15 llibres i revistes
-- 6 dvds (pelis, documentals)
-- 9 altres docs (cd, cd-rom) cançons, contes narrats, etc

-- Per simplificació, considerarem que un usuari podrà tenir simultàniament un
-- màxim de 4 docs:

-- 2 docs del tipus llibre/revista
-- 1 DVD
-- 1 CD

-- D'altra banda, un exemplar d'un document apareixerà com a disponible si
-- actualment no està en prèstec i a més a més el seu estat és Disponible.
-- Qualsevol altre estat (Exclòs de prèstec, En exposició, Reservat) farà que no
-- apareixi l'exemplar com apte per agafar en prèstec. Evidentment, si que es
-- podran consultar a la pròpia biblioteca.
-- Evidentment, un usuari podrà afagar documents en prèstec si i només sí no
-- té el carnet blocat.


--, p_iddocument2 document.iddocument%type default null, p_tipus2 varchar(50) default null, p_iddocument3 document.iddocument%type default null, p_tipus3 varchar(50) default null, p_iddocument4 document.iddocument%type default null, p_tipus4 varchar(50) default '',) --  puentear smallint



create or replace function prestecDocument(p_iddocument1 document.iddocument%type, p_tipus1 varchar(50), p_usuari usuari.idusuari%type)
returns boolean
as $$

end;
$$ language plpgsql;


--


-- amb validació d'exemplar

create or replace function docuemntDisponible(p_iddocument document.iddocument%type, p_tipus varchar(50)) --  puentear smallint
returns boolean
as $$
declare 
	v_iddocument  document.iddocument%type;
	v_exemplar exemplar%rowtype;
begin
	-- storing valid id
	case 
		when lower(p_tipus) = 'llibre' then
			select iddocument
			into strict v_iddocument
			from llibre
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'revista' then
			select iddocument
			into strict v_iddocument
			from revista
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'musica' then
			select iddocument
			into strict v_iddocument
			from musica
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'pelicula' then
			select iddocument
			into strict v_iddocument
			from pelicula
			where iddocument = p_iddocument;
		else
			return false; -- es descarta taula falsa i id document f
	end case;
		-- loop through exemplars till disponible is found
	 FOR v_exemplar IN 
        select *
		from exemplar
		where iddocument = p_iddocument
    LOOP
        -- can do some processing here
        if lower(v_exemplar.estat) = 'disponible' then
			return true;
        end if;
    END LOOP;
    return false;
exception
		when no_data_found then 
			return false;
end;
$$ language plpgsql;
-- select docuemntDisponible(1,'llibre');
--update exemplar set estat = 'Exclòs de prèstec' where iddocument = 1; 
-- select docuemntDisponible(1,'llibre');
-- update exemplar set estat = 'Disponible' where idexemplar=3;
-- select docuemntDisponible(1,'llibre');
-- comprovar que no esta prestat
-- insert into prestec (idexemplar,datapres,datadev,idusuari,vegadesrenovat) values(3,current_date,current_date,1,0);


create or replace function docuemntDisponible(p_iddocument document.iddocument%type, p_tipus varchar(50)) --  puentear smallint
returns boolean
as $$
declare 
	v_iddocument  document.iddocument%type;
	v_exemplar exemplar%rowtype;
begin
	-- storing valid id
	case 
		when lower(p_tipus) = 'llibre' then
			select iddocument
			into strict v_iddocument
			from llibre
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'revista' then
			select iddocument
			into strict v_iddocument
			from revista
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'musica' then
			select iddocument
			into strict v_iddocument
			from musica
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'pelicula' then
			select iddocument
			into strict v_iddocument
			from pelicula
			where iddocument = p_iddocument;
		else
			return false; -- es descarta taula falsa i id document f
	end case;
		-- loop through exemplars till disponible is found
	 FOR v_exemplar IN 
        select *
		from exemplar
		where iddocument = p_iddocument
    LOOP
        -- can do some processing here
        if lower(v_exemplar.estat) = 'disponible' then
			select idexemplar
			from prestec 
			where idexemplar = v_exemplar.idexemplar and datadev is null;
			if not found then
				return true;
			end if;
        end if;
    END LOOP;
    return false;
exception
		when no_data_found then 
			return false;
end;
$$ language plpgsql;
-- vamos desglosando


-- function is available (disponible a exemplar)
--isAvailable(p_iddocument)

create or replace function isAvailable(p_iddocument document.iddocument%type)
returns boolean
as $$
declare
	v_exemplar exemplar%rowtype;
begin
	FOR v_exemplar IN 
			select *
			from exemplar
			where iddocument = p_iddocument
		LOOP
			-- can do some processing here
			if lower(v_exemplar.estat) = 'disponible' then
				return true;
			end if;
		END LOOP;
	return false;
exception
		when no_data_found then 
		return false;
end;
$$ language plpgsql;


-- test
-- update exemplar set estat= 'Exclòs de prèstec' where iddocument =1 and idexemplar =3;
-- select isAvailable(1);
-- end function is available

-- funció getExemplar
create or replace function getExemplar(p_iddocument document.iddocument%type)
returns integer
as $$
declare
	v_exemplar exemplar%rowtype;
begin
	FOR v_exemplar IN 
			select *
			from exemplar
			where iddocument = p_iddocument
		LOOP
			-- can do some processing here
			if lower(v_exemplar.estat) = 'disponible' then
				return v_exemplar.idexemplar;
			end if;
		END LOOP;
	return null;
exception
		when no_data_found then 
		return null;
end;
$$ language plpgsql;


-- test
-- update exemplar set estat= 'Exclòs de prèstec' where iddocument =1 and idexemplar =3;
-- update exemplar set estat= 'Disponible' where iddocument =1 and idexemplar =3;
-- select isAvailable(1);
-- end function is available


-- funció esta en lloguer o no

create or replace function isNotRented(p_idexemplar prestec.idexemplar%type)
returns boolean
as $$
declare
	p_basura varchar(1);
begin
	select '1'
	into strict p_basura
	from prestec 
	where idexemplar = p_idexemplar and
	datadev is null;
	return false;
exception
		when no_data_found then 
			return true;
end;
$$ language plpgsql;

-- end is not rented



-- funció comprova exemplar

create or replace function docuemntDisponible(p_iddocument document.iddocument%type, p_tipus varchar(50)) --  puentear smallint
returns integer
as $$
declare 
	v_iddocument document.iddocument%type;
	v_exemplar exemplar%rowtype;
	v_exemplarDisponible exemplar.idexemplar%type;
begin
	-- storing valid id
	case 
		when lower(p_tipus) = 'llibre' then
			select iddocument
			into strict v_iddocument
			from llibre
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'revista' then
			select iddocument
			into strict v_iddocument
			from revista
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'musica' then
			select iddocument
			into strict v_iddocument
			from musica
			where iddocument = p_iddocument;
		when lower(p_tipus) = 'pelicula' then
			select iddocument
			into strict v_iddocument
			from pelicula
			where iddocument = p_iddocument;
		else
			return 0; -- es descarta taula falsa i id document f
	end case;
	-- function checks exemplar is 'Disponible'
	if isAvailable(p_iddocument) then
		v_exemplarDisponible := getExemplar(p_iddocument);
		-- check if it is rented or not
		if  isNotRented(v_exemplarDisponible) then
			return v_exemplarDisponible; 
		end if;
	else 
		return 0; -- es descarta si no esta disponible
	end if;
	return 0;
exception
		when no_data_found then 
			return 0;
end;
$$ language plpgsql;


-- select docuemntdisponible(1,'llibre');
-- update prestec set datadev = null where idexemplar = 3;
-- update prestec set datadev = current_date where idexemplar = 3;
  
