--() { :; }; exec psql training -f "$0"

-- iaw14270791
-- joc de proves funcions biblioteca 
-- Jacint Iglesias
-- UF3
\c training


-- training

-- trigger actualitzarVendes
drop trigger actualitzarVendes on pedido;
create or replace function actualitzarVendes()
returns trigger
as $$
begin
	update repventa set ventas = ventas + new.importe -- da error en el intro
	where repcod = new.repcod;
	
	update oficina
	set ventas = ventas + new.importe
	where ofinum = (select ofinum
					from repventa
					where repcod = new.repcod);
	
	return new; -- <= esto es nuevo triggers a nivel de row
end;
$$ language plpgsql;


-- cabecera
drop trigger tactualitzarVendes on pedido;
CREATE TRIGGER tactualitzarVendes
after INSERT ON pedido 
for each row -- si hem de fer servir old i new es row
EXECUTE PROCEDURE actualitzarVendes();  


