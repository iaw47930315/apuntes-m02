#!/bin/bash
# Script: funcionsCadena.sh
# Description: script que aplica joc de proves per validar els exercicis de cadenes
# Date creation: 11/03/2020
# Use: ./funcionsCadena

# iaw14270791
# funcions de cadenes
# Jacint Iglesias
# UF3

NEUTRE='\033[0m'
VERD='\033[0;32m'
# resetejem la bd
psql -d template1 -f ../db_scripts/taulesBiblioteca.sql
# importem les funcions
psql -d template1 -f funcionsBiblioteca.sql
  # Apliquem un joc de proves per validar les funcions i taula
echo -e "\n$VERD Joc de proves$NEUTRE\n"
psql -d template1 -f jocProvesfuncBiblioteca.sql
