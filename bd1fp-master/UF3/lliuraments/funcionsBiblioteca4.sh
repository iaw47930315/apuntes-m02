#!/bin/bash
# Script: funcionsCadena.sh
# Description: script que aplica joc de proves per validar funció reserva document
# Date creation: 11/03/2020
# Use: ./funcionsBiblioteca4.sh

# iaw14270791
# funcions de cadenes
# Jacint Iglesias
# UF3

NEUTRE='\033[0m'
VERD='\033[0;32m'
# resetejem la bd
echo -e "\n$VERD Reset de taula$NEUTRE\n"
./taulesBiblioteca.sql
# importem les funcions
echo -e "\n$VERD Funcions Biblioteca4$NEUTRE\n"
./funcionsBiblioteca4_correcio.sql
  # Apliquem un joc de proves per validar les funcions i taula
echo -e "\n$VERD Joc de proves$NEUTRE\n"
./jocProves.sql
