create or replace function testcaseEq(p_mes varchar)
returns varchar
as $$
declare
        v_mes varchar;
begin
        case
                when p_mes in ('enero','january') then
                        v_mes := 1;
                else 
                        v_mes := 0;
        end case;
        return v_mes;
end;
$$ language plpgsql;
