--Alejandro Carreño Rubio
--iaw46487690
--7/2/2019

--1 Insereix (si s'escau) el/s lloguer/s necessari/s per validar les seguents preguntes DML
insert into lloguer(codsoci, coddvd, datapres, datadev, import)
values (1, 1, current_date-15, current_date-8, 1);
--2 Esborra els lloguers de fa més d'una setmana
delete from lloguer
where datadev < current_date-7;

--3 Actualiza la reserva de la peli 'Juana la Loca' del soci Manel Capdevila, el seu 
--import amb el seu preu corresponent
update lloguer
set import = (select preu
				from pelicula
				where lower(titol)='juana la loca')
where codsoci = (select codsoci
					from soci
					where lower(nom)='manel')
		and coddvd = (select codpeli
						from pelicula
						where lower(titol)='juana la loca');

--4 Mostra per cada peli el seu codi, titol, el número de vegades que s'ha prestada i 
--els guanys derivats del seu prèstec (utilitzeu el camp preu, no import). El 3r i 4t camp 
--es diran Prestecs i Guanys respectivament. Ordena la llista de pelis de mes vegades 
--prestades a menys, i s'hi hi ha coincidència per ordre alfabètic
select codpeli, titol,

--Correcció
select p.codpeli, titol, count(*)"prestecs", count(*)*preu"guanys", sum(coalesce(preu,0))
from pelicula p left join dvd d 
	on p.codpeli=d.codpeli left join lloguer l
		on d.coddvd=l.coddvd
group by p.codpeli, titol;

--5 Mostrar per a cada gènere, quina és la peli més cara. Mostreu el nom del gènere el 
--títol de la pelicula i el preu. Ordeneu per gènere i títol
select genere, titol, preu
from genere g, pelicula p
where g.codgen=p.codgen and preu in (select max(preu)
									from pelicula
									group by codgen)
group by genere, titol, preu
order by 1 asc, 2 asc;
--Correcció
select g.codgen, genere, max(coalesce(preu,0)) max
from pelicula p right join genere g
	on p.codgen=g.codgen
group by g.codgen, genere;
									
--6 Mostra els gèneres dels quals mai s'ha prestat una peli. Ordena'ls alfabèticament
select genere
from genere
where codgen not in (select codgen
					from pelicula)
order by 1 asc;

--Correcció
select genere
from genere
where codgen not in (select p.codgen
					from lloguer l,dvd d,pelicula p
					where l.coddvd=d.coddvd and d.codpeli=p.codpeli)
order by 1;
--7 Mostra el titol de la pelicula (o pelicules) que té més gent esperant per ella.
--Ordena alfabéticament
select titol 
from pelicula
where codpeli <any (select count(codpeli)
						from llistaespera
						group by codpeli);
						
--Correcció
select p.codpeli, titol
from llistaespera l join pelicula p
	on l.codpeli=p.codpeli
group by p.codpeli
having count(*)=(select max(numSocis)
				from (select codpeli, count(*) numSocis
						from llistaespera
						group by codpeli)taula
);
