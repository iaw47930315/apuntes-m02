--() { :; }; exec psql scott -f "$0"
--Alejandro Carreño Rubio
--iaw46487690
--funcionsMatricula.sql

\c matricula

--Retorna true en el caso que dado una asignatura y un grupo en el que 
--se quiera matricular.

drop function if exists grupoCompleto();

create or replace function grupoCompleto(p_codasig varchar(5), p_codgrupo int)
returns boolean
as $$
	declare
		v_codasig varchar(5);
		v_codgrupo int;
	begin
		select idmodulo, codgrupo
		into strict v_codasig, v_codgrupo
		from modulo, grupo
		where lower(p_codasig)=lower(idmodulo) and p_codgrupo=codgrupo;
			return true; 
		exception
		when NO_DATA_FOUND then
            return false;
	end; 
$$ language plpgsql;

--Retorna verdadero en el caso de que el alumno con dni p_dni tenga aprobada la
--asignatura p_codasig.

drop function if exists estaAprobada();

create or replace function estaAprobada(p_codasig varchar(5), p_dni varchar(9))
returns boolean
as $$
	declare
		v_codasig varchar(5);
	begin
		select idmodulo, dni
		into strict v_codasig
		from expediente
		where lower(p_codasig)=lower(idmodulo) and lower(p_dni)=lower(dni)
				and nota >= 5;
			return true; 
		exception
		when NO_DATA_FOUND then
            return false;
	end; 
$$ language plpgsql;

--Retorna verdadero en el caso que un alumno que se quiera matricular de
--una asignatura y o no tiene las asignaturas prerrequisito aprobadas 
--(en el caso de estar matriculado) o directamente ni tan siquiera está 
--matriculado de ellas. Un ejemplo de prerrequisitos lo encontramos en
--DAW: para poder matricularse de M02-UF4, es necesario haber estado 
--matriculado de M02-UF2 y de M02-UF3, y además tenerlas aprobadas.
--En la tabla Prerrequisitos habrian pues, dos filas con M02-UF4: una 
--para M02-UF2 y otra para M02-UF3.

drop function if exists asignaturasPendientes();

create or replace function asignaturasPendientes(p_codasig varchar(5), p_dni varchar(9))
returns boolean
as $$
	declare
		v_prerre varchar(5);
	begin
		select idprerre
		into strict v_prerre
		from prerrequisito
		where idmodulo=p_codasig and v_prerre = (select idmodulo
													from expediente e, prerrequisito p
													where lower(p_dni)=lower(dni) 
													and nota >= 5);
			return true; 
		exception
		when NO_DATA_FOUND then
            return false;
	end; 
$$ language plpgsql;

--Crea la fila correspondiente en la tabla matricula solo cuando el alumno 
--se matricula del primer módulo en un curso académico (tabla expediente).
--Cada vez que se añada una nueva asignatura al expediente se tiene que 
--actualizar el importe dela matricula con el precio del módulo.

drop function fMatricula();
create or replace function fMatricula()
returns trigger
as $$
declare
 v_precio numeric(4,2);
begin
	select precio
	into strict v_precio
	from modulo
	where idmodulo=new.idmodulo;

	update matricula 
	set importe = (importe + v_precio)
	where dni = new.dni;
	return null;
end;
$$ LANGUAGE plpgsql;


drop trigger on matricula;

create trigger tMatricula
after insert on expediente
for each row
execute procedure fMatricula();

--No funciona be


















