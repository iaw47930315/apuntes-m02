--Alejandro Carreño Rubio 
--iaw46487690
--18/12/2018
--exE.sql

--Exercicis amb operadors d'àlgebra relacional

--Intersección ,unión y diferencia

--1. Obtener una lista de todos los productos cuyo precio exceda de 3500 y de los 
--cuales hay algún pedido con un importe superior a 35000.
select p.fabcod, p.prodcod
from producto pr, pedido p
where pr.fabcod=p.fabcod and pr.prodcod=p.prodcod and precio > 3500 and importe > 35000;

select fabcod, prodcod
from producto
where precio > 3500
intersect
select fabcod, prodcod
from pedido
where importe > 35000;

--2. Obtener una lista de todos los productos cuyo precio más IVA exceda de 3.500 o 
--bien haya algún pedido cuyo importe más IVA exceda de 30.000.
select fabcod, prodcod, precio*1.21
from producto
where precio*1.21>3500
union
select fabcod, prodcod, importe*1.21
from pedido
where importe*1.21>30000
order by 1, 2;


--3. Obtener los códigos de los representantes que son directores de oficina y que 
--no han tomado ningún pedido.
select director
from oficina
except
select repcod 
from pedido;


UNION -> or
INTERSECT -> and
EXCEPT ->


--Saber si el codigo de la tabla A(x) y B(x) son iguales.
select x
from A
except
select x
from B;

select x
from B
except
select x
from A;

--Mostrar el nom del client, la data de la comanda, el import de les comandes de 
--cada client
select nombre, fecha, importe
from cliente c, pedido p
where c.cliecod=p.cliecod;

select nombre
from cliente c
where exists( select *
				from pedido p
				where p.cliecod=c.cliecod);
				
--Representants que no han fet cap comanda
select repcod, nombre
from repventa r
where repcod not in (select repcod
						from pedido);

select repcod, nombre
from repventa r
where not exists (select *
					from pedido p
					where r.repcod=p.repcod);
					
select repcod
from repventa
except
select repcod
from pedido;

select *
from repventa r,
	(select repcod
	from repventa
	except
	select repcod
	from pedido) rep
where r.repcod=rep.repcod;
--rep es el alias de la tabla del except y es obligatorio ponerlo

--Clients que nomès hagin fet comandes d'un producte en particular(training)(1punto)

					
--Incrementa el salario un 20% al trabajador mas antiguo(scott)
select ename, hiredate, round(sal+sal/100*20,2) "salari"
from emp
where hiredate = (select min(hiredate)
					from emp);
--Para cambiar la base de datos
update emp
set sal = sal * 1.2
where hiredate = (select min(hiredate)
					from emp);




--current_timestamp
insert into lloguer
values (1,1, current_timestamp-interval'1 day',current_timestamp, 1);

INSERT INTO LLOGUER(Codsoci, coddvd,datapres, datadev, import) 
VALUES (1,1, current_date-10, current_date-9, 1);



--Cambiar tots el empleats que tenen com a cap a King ya no tendran jefe

delete from emp
where empno not in (select mgr
					from emp
					where mgr is not null);
					
update emp
set mgr = null
where mgr = (select empno
				from emp
				where lower(ename)='king');
				
delete from emp
where sal = (select max(sal)
--Els que depenien de king ahora depenenen de scott
update emp
set mgr = (select empno
			from emp
			where lower(ename)='scott')
where mgr = (select empno
				from emp
				where lower(ename)='king');

--Crear la taula TOP REPVENTA amb els 5 millors vendedors
create table TOP_REPVENTA
as select * from repventa;

create table TOP_REPVENTA
as select * from repventa
order by ventas desc
limit 5;

--Insertar els venedors que venen mes de la mitjana
insert into TOP_REPVENTA
select *
from repventa
where ventas > (select avg(ventas)
                    from repventa)
order by ventas desc;

--varios insert
insert into emp(empno, ename)
values (1235,'alejandro'),
		(2345,'pablo'),
		(1234,'jordi');
		
create sequence deptno_seq
start with 50
increment by 10;

select nextval('deptno_seq');
select currval('deptno_seq');

drop sequence deptno_seq;

--Crear el nou departament contabilitat que esta en barcelona(BDscott)
insert into dept
values (nextval('deptno_seq'), 'CONTABILITAT', 'BARCELONA');


