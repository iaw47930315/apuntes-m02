--Alejandro Carreño Rubio 
--iaw46487690
--18/12/2018
--exD.sql

--Exercicis de subconsultes

--0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho con self join,
--ahora con subconsultas)
select nombre, puesto
from repventa
where repcod in (select jefe
				from repventa);
				
--1. Obtener una lista de los representantes cuyas cuotas son iguales ó superiores 
--al objetivo de la oficina de Atlanta.
select nombre
from repventa
where cuota >= (select objetivo
				from oficina
				where lower(ciudad)='atlanta'); 

--2. Obtener una lista de todos los clientes (nombre) que fueron contactados 
--por primera vez por Bill Adams.
select nombre
from cliente
where repcod = (select repcod
				from repventa
				where lower(nombre)='bill adams');

--3. Obtener una lista de todos los productos del fabricante ACI cuyas existencias 
--superan a las existencias del producto 41004 del mismo fabricante.
select *
from producto
where lower(fabcod)='aci' and exist > (select exist
										from producto
										where prodcod='41004');

--4. Obtener una lista de los representantes que trabajan en las oficinas que han 
--logrado superar su objetivo de ventas.
select *
from repventa 
where ofinum in (select ofinum
				from oficina
				where objetivo < ventas);

--5. Obtener una lista de los representantes que no trabajan en las oficinas 
--dirigidas por Larry Fitch.
select *
from repventa
where ofinum not in (select ofinum
					from repventa
					where lower(nombre)='larry fitch');

--6. Obtener una lista de todos los clientes que han solicitado pedidos del 
--fabricante ACI entre enero y junio de 2003.
select *
from cliente
where cliecod in (select cliecod
					from pedido
					where lower(fabcod)='aci' 
						and to_char(fecha,'yyyy mm')>='1990 01' 
						and to_char(fecha,'yyyy mm')<='1990 06');

--7. Obtener una lista de los productos de los que se ha tomado un pedido de 
--150 euros ó mas.
select *
from producto
where (fabcod,prodcod) in (select fabcod,prodcod
							from pedido
							where importe >= 150);

--8. Obtener una lista de los clientes contactados por Sue Smith que no han 
--solicitado pedidos con importes superiores a 18 euros.
select *
from cliente
where repcod in (select repcod
				from repventa
				where lower(nombre)='sue smith' and repcod in (select repcod
																from pedido
																where importe > 18));

--9. Obtener una lista de las oficinas en donde haya algún representante cuya cuota 
--sea más del 55% del objetivo de la oficina.
select *
from oficina
where ofinum in (select o.ofinum
				from repventa r, oficina o
				where r.ofinum=o.ofinum and cuota > objetivo/100*55);

--9.bis. Muestra empleados que trabajan en el mismo departamento y en el mismo puesto.
--(base de datos scott).
select *
from emp e
where (deptno,job) in (select deptno,job
						from emp
						where empno!=e.empno); 

--10. Obtener una lista de los representantes que han tomado algún pedido cuyo 
--importe sea más del 10% de de su cuota.
select *
from repventa
where repcod in (select repcod
				from pedido
				where importe > cuota/100*10);

--11. Obtener una lista de las oficinas en las cuales el total de ventas de sus 
--representantes han alcanzado un importe de ventas que supera el 50% del objetivo 
--de la oficina. Mostrar también el objetivo de cada oficina (suponed que el 
--campo ventas de oficina no existe).
select *
from oficina
where ofinum in (select r.ofinum
					from repventa r, oficina o
					where o.ofinum=r.ofinum and r.ventas > o.objetivo/100*50);

--12. Mostrar el nombre y ventas del representante/s contratado más recientemente.
select nombre, ventas
from repventa
where fcontrato = (select max(fcontrato)
						from repventa);
						
--13.0 Mostrar la oficina que vende más
select *
from oficina
where ventas = (select max(ventas)
				from oficina);

--13.1 Mostrar el director de la oficina que vende más.
select nombre"Director oficina"
from oficina o, repventa r
where r.repcod=o.director and o.ventas = (select max(ventas)
											from oficina);

--13.2 Mostrar el director que vende más.
select repcod, nombre, ventas
from repventa
where ventas = (select max(r.ventas)
					from repventa r, oficina
					where director=r.repcod);

--14. Lista de representantes que nunca han realizado un pedido (el campo ventas
--no lo tengais en cuenta).
select *
from repventa
where repcod not in (select repcod
						from pedido);

--15 Lista de productos que nunca han sido solicitados.
select *
from producto
where (fabcod,prodcod) not in (select fabcod,prodcod
								from pedido);

--16.1. Mostrar por cliente, su gasto en la empresa (funcion de grupo). 
--Mostar nombre y el montante de la facturacion, ordenado de mejor a peor cliente 
--(no subconsulta).
select nombre, sum(importe)"Montante de facturación"
from cliente cl, pedido p
where cl.cliecod=p.cliecod
group by cl.nombre
order by 2 desc;

--16.2 Mostrar los 10 mejores clientes (no subconsulta).
select nombre, sum(importe)"Montante de facturación"
from cliente cl, pedido p
where cl.cliecod=p.cliecod
group by cl.nombre
order by 2 desc
limit 10;

--16.3 Clientes que gastan más de la media.
select nombre, sum(importe)"Gasto"
from cliente cl, pedido p
where p.cliecod=cl.cliecod
group by cl.nombre, cl.cliecod, p.cliecod
having sum(importe) > (select sum(importe)/count(distinct(cliecod))
						from pedido)
order by 2 desc;

select round(avg(Gasto),2) "Media de gasto"
from (select sum(importe) Gasto
		from pedido p, cliente cl
		where p.cliecod=cl.cliecod
		group by nombre) TablaGasto;

--17. Mostrar por cada oficina cuál és su representante estrella (el que vende más). 
--Mostrar código de oficina, ciudad, Nombre del representan
select o.ofinum, ciudad, nombre
from oficina o, repventa r
where r.ofinum=o.ofinum and (r.ofinum,r.ventas) in (select ofinum,max(ventas)
													from repventa
													group by ofinum);

--18. Clientes que nunca han hecho un pedido
select *
from cliente
where cliecod not in (select cliecod
						from pedido);

