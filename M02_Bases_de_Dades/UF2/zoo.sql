--Zoos iaw46487690
drop table animal;
drop table EspecieAnimal;
drop table zoo;
create table zoo (
codigo int,
nombre varchar(20) constraint zoo_nombre_nn not null,
ciudad varchar(15),
pais varchar(15),
tamaño varchar(10),
presupuestoAnual numeric(8,2),
Constraint zoo_codigo_pk primary key(codigo)
);
create table EspecieAnimal (
idEspecie int,
nombreBulgar varchar(15) constraint EspecieAnimal_nombreBulgar_nn not null,
nombreCientifico varchar(20),
familia varchar(15),
peligroDeExtincion varchar(2),
constraint EspecieAnimal_idEspecie_pk primary key(idEspecie)
);
create table animal (
codigo int,
codEspecie int,
sexe varchar(6),
especie varchar(15),
añoNacimiento int,
pais varchar(15),
continente varchar(10),
idEspecie int,
Constraint animal_codigo_fk foreign key(codigo) references zoo(codigo),
constraint animal_codEspecie_pk primary key(codEspecie),
constraint animal_idEspecie_fk foreign key(idEspecie) references EspecieAnimal(idEspecie)
);
insert into zoo
values (1, 'Zoo Barcelona', 'Barcelona', 'España', '100.000 m2', 900000);
insert into EspecieAnimal
values (123, 'Elefante', 'Elephantus Gigantus', 'Elephants', 'No');
insert into animal
values (1, 10, 'Macho', 'Mamifero', 2000, 'Senegal', 'Africa', 123);

insert into zoo
values (2, 'Zoo Madrid', 'Madrid', 'España', '100.000 m2', 90000050); 
insert into EspecieAnimal
values (456, 'Lince Iberico', 'Linzus Ibericus', 'Linces', 'Si');
insert into animal
values (2, 20, 'Hembra', 'Mamifero', 2010, 'España', 'Europa', 456);
