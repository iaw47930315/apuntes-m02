--Alejandro Carreño Rubio 
--iaw46487690
--26/11/18

--EJERCICIOS scott recomanables

--1.Seleccionar el código de empleado, salario, comisión, nº de departamento y fecha 
--de la tabla EMP.
select empno, sal, comm, deptno, hiredate
from emp;

--2.Mostrar toda la información de la tabla departamentos.
select *
from dept;

--3.Seleccionar el nombre y el empleo de aquellos  que sean salesman (utiliza la 
--función lower)
select ename, job
from emp
where lower(job)='salesman';

--4.Seleccionar el nombre y el código de departamento de aquellos empleados que 
--no trabajen en el departamento 30.
select ename, deptno
from emp
where deptno != 30;

--5.Mostrad el nombre y salario de aquellos empleados que ganen más de 2000.
select ename, sal
from emp
where sal > 2000;

--6.Mostrad el nombre y la fecha de contratación de aquellos empleados que 
--hayan entrado antes del 1/1/82.
select ename, hiredate
from emp
where to_char (hiredate,'yyyy')<'1982';

--7.Seleccionar el nombre de los vendedores que ganen más de 1500.
select ename
from emp
where sal > 1500;

--8.Seleccionar el nombre de aquellos que sean ‘CLERK’ o trabajen en el departamento 30.
select ename
from emp
where lower(job)='clerk' or deptno=30;

--9.Seleccionar aquellos que se llamen ‘SMITH’, ‘ALLEN’ o ‘SCOTT ‘.
select *
from emp
where lower(ename) in ('smith', 'allen', 'scott');

--10.Seleccionar aquellos que no se llamen ‘SMITH’, ‘ALLEN’ o ‘SCOTT ‘.
select *
from emp
where lower(ename) not in ('smith', 'allen', 'scott');

--11.Seleccionar aquellos cuyo salario esté entre 2000 y 3000.
select *
from emp
where sal between 2000 and 3000;

--12.Seleccionar aquellos empleados que trabajen en el departamento 10 o en el 20.
select *
from emp
where deptno = 10 or deptno = 20;

--13.Seleccionar aquellos empleados cuyo nombre empiece por ‘A’.
select *
from emp
where lower(ename) like 'a%';

--14.Seleccionar aquellos empleados cuyo nombre tenga como segunda letra una ‘D’.
select *
from emp
where lower(ename) like '_d%';

--15.Seleccionar los distintos departamentos que existen en la tabla EMP.
select distinct deptno
from emp;

--16.Seleccionar el departamento y el trabajo de los empleados (evitando repeticiones).
select distinct deptno, job
from emp;

--17.Seleccionar aquellos empleados que hayan entrado en 1981.
select *
from emp
where to_char (hiredate,'yyyy')='1981';

--18.Seleccionar aquellos empleados que tienen comisión,mostrando nombre y comisión.
select ename, comm
from emp
where comm is not null;

--19.Seleccionar aquellos empleados que ganen más de 1500, ordenados por empleo.
select *
from emp
where sal > 1500
order by job;

--20.Calcular el salario anual a percibir por cada empleado (tened en cuenta 14 pagas).
select ename, sal*14 "Salario Anual"
from emp;

--21.Mostrar el nombre del empleado, el salario y el incremento del 15 % del salario. 
select ename, sal, round(sal/100*15,2) "Incremento del 15% del salario"
from emp;

--22.Mostrar el nombre del empleado, el salario, el incremento del 15 % del salario 
--y el salario aumentado un 15%.
select ename, sal, round(sal/100*15,2) "Incremento del 15% del salario", 
		sal+round(sal/100*15,2) "Salario aumentado un 15%"
from emp;


--FUNCIONES DE GRUPOS

--26.Calcular el salario total mensual.
select sum(sal*4)"Salario Mensual"
from emp;

--27.Calcular el número de empleados que tienen comisión y la media. Queremos mostrar todos 
--los empleados, con lo que tened en cuenta que el campo comm puede tener nulos.
select count(comm)"Empleats amb comisió", round(sum(comm)/count(*),2)"Comisió Mitja"
from emp;

--28.Seleccionar el salario, mínimo y máximo de los empleados, agrupados por empleo.
select job, min(sal)"Salario Mínimo", max(sal)"Salario Máximo"
from emp
group by job;

--29.Siguiendo lo explicado en el ej. 27, Seleccionar por cada departamento, el numero de 
--empleados que tienen comision, la suma y la media.
select dname, count(comm)"Empleats amb comisió", round(sum(coalesce(comm,0)),2)"Suma de comisions", 
		round(sum(coalesce(comm,0))/count(*),2)"Comisió Mitja"
from emp e right join dept d
	on d.deptno=e.deptno 
group by d.deptno
order by 2 desc;

--30.Idem que el 4, pero mostrando además el nombre de departamento.
select dname, ename, d.deptno
from emp e, dept d
where e.deptno=d.deptno and d.deptno != 30;

--31.Seleccionar el salario mínimo, máximo y medio de los empleados agrupados por empleo, 
--pero sólo de aquellos cuya media sea superior a 4000.
select job, min(sal)"Salario Mínimo", max(sal)"Salario Máximo", round(avg(sal),2)"Salario Medio"
from emp
group by job
having avg(sal) > 4000;

--32.Visualice el número y el nombre de los departamentos que tengan más de tres empleados asignados.
select d.deptno, dname
from dept d, emp e
where d.deptno=e.deptno
group by d.deptno
having count(*) >= 2;

--SUBCONSULTES

--33.Seleccionar los empleados que trabajan en el mismo departamento que Clark.
select ename
from emp
where deptno = (select deptno
				from emp
				where lower(ename)='clark');

--34.Calcular el número de empleados por departamento que tienen un salario superior 
--a la media.
select count(*)"Empleados con salario mayor a la media", deptno
from emp
where sal > (select avg(sal)
				from emp)
group by deptno;

--35.Seleccionar los empleados cuyo salario sea superior al de Adams.
select ename
from emp
where sal > (select sal
			from emp
			where lower(ename)='adams');

--36.Seleccionar el nombre y fecha de ingreso del empleado que lleva menos tiempo.
select ename, hiredate
from emp
where hiredate = (select max(hiredate)
					from emp);

--37.Seleccionar el nombre de los empleados que ganen más que todos los que trabajan 
--de salesman.
select ename
from emp
where sal > (select max(sal)
			from emp
			where lower(job)='salesman'); 

--38.Seleccionar los empleados que ganen más que alguno de los que trabajan de salesman.
select ename
from emp
where sal not in (select sal
					from emp
					where lower(job)='salesman');
		
-- MÁS EJERCICIOS

--39.Mostrar el trabajo, el nombre y el salario de los empleados ordenados por el 
--tipo de trabajo y por salario descendente.

--40.Seleccionar el nombre de cada empleado, y el número y nombre de su jefe.

--41.Mostrar el nombre del empleado y su fecha de alta en la empresa de los empleados 
--que son analyst.

--42.Mostrar el nombre del empleado y una columna que contenga el salario multiplicado 
--por la comisión cuya cabecera sea ‘BONO’.

--43.Encontrar el salario medio de aquellos empleados cuyo trabajo sea el de analista 
--(analyst).

--44.Encontrar el salario más alto, el más bajo y la diferencia entre ambos.

--45.Hallar el numero de trabajos distintos que existen en el departamento 30.

--46.Mostrar el nombre del empleado, su trabajo, el nombre y el código del departamento 
--en el que trabaja.

--47.Mostrar el nombre, el trabajo y el salario de todos los empleados que tienen 
--un salario superior al salario más bajo del departamento 30.

--48.Encontrar a los empleados cuyo jefe es ‘BLAKE’.

--49.Encontrar el nº de trabajadores diferentes en el departamento 30 para aquellos 
--empleados cuyo salario pertenezca al intervalo [1000, 1800].

--50.Encontrar el ename, dname, job y sal de los empleados que trabajen en el mismo 
--departamento que ‘TURNER’ y su salario sea mayor que la media del salario del 
--departamento 10.


--DML

--57.Insertar en la tabla DEPT la información correspondiente a un nuevo departamento 
--de consultoría, cuyo número sea 50 y que esté ubicado en SANTANDER.
insert int dept


--58.Dar de alta a un nuevo empleado de nombre BELTRAN, que desempeñará el puesto de analyst en el departamento de SALES y cuyo número de empleado sea 8200. Por el momento se desconoce su salario y quién será su jefe.

--59.Cambiar la fecha del empleado SCOTT por la de hoy.

--60.El empleado MILLER, debido a sus éxitos es ascendido al puesto de analyst,aumentándole su salario en un 20%, cambiándole al departamento ‘SALES’ e integrándole en el grupo que supervisa el empleado 7566. Haced los cambios oportunos .

--61.A raíz de la firma del convenio anual de la empresa, se ha determinado incrementar el salario de todos los empleados en un 6%. Incorporar esta cambio a la base de datos.

--62.El empleado JAMES causa baja en la empresa. Borrad la información correspondiente

--63.Se contrata a SANZ, con número 1657, para el departamento 30 y con sueldo 3000.

--64.SANZ cambia al departamento 40.

--65.SANZ trabaja de vendedor, con una comisión de 4000.

--66.Se decide aumentar las comisiones. Aumentan todas las comisiones  en un 20% del salario.

--67.Se decide aumentar un 35% el salario a los empleados que ganen menos que SANZ.

--68.Se despide a SANZ.

--69.Se despide a los que trabajan en el departamento ‘SALES’.

