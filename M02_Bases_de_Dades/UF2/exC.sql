--Alejandro Carreño Rubio 
--iaw46487690
--18/12/2018
--exC.sql

--Exercicis de Funciones de Grupo

--1.Mostrar la suma de las cuotas y la suma de las ventas totales de todos 
--los representantes.
select sum(cuota) cuota, sum(ventas) ventas
from repventa;

--2.¿Cuál es el importe total de los pedidos tomados por Bill Adams?
select sum(importe) importe
from pedido p, repventa r
where lower(nombre)='bill adams' and p.repcod=r.repcod;

--3.Calcula el precio medio de los productos del fabricante ACI.
select round(avg(precio),2) "Precio Medio"
from producto
where lower(fabcod)='aci'; 

--4.¿Cuál es el importe medio de los pedidos solicitados por el cliente 2103
select round(avg(importe),2) "Importe Medio"
from pedido
where cliecod=2103;

--4.bis.¿Cuál es el importe medio de los pedidos solicitados por el cliente (Acme Mfg.).
select round(avg(importe),2) "Importe Medio"
from pedido p, cliente cl
where p.cliecod=cl.cliecod and lower(nombre)='acme mfg.';

--5.Mostrar la cuota máxima y la cuota mínima de las cuotas de los representantes.
select max(cuota), min(cuota)
from repventa;

--6.¿Cuál es la fecha del pedido más antiguo que se tiene registrado?
select min(fecha)"Fecha del pedido más antiguo"
from pedido;

--7.¿Cuál es el mejor rendimiento de ventas de todos los representantes? 
--(considerarlo como el porcentaje de ventas sobre la cuota).
select round(max(ventas/cuota),2) "Porcentaje máximo de ventas"
from repventa;

--8.¿Cuántos clientes tiene la empresa?
select count(*) "Numero de Clientes"
from cliente;

--9.¿Cuántos representantes han obtenido un importe de ventas superior a su propia cuota?
select count(*) "Representantes"
from repventa
where ventas > cuota;

--9.bis.Idem pero el camp ventas no existeix


--10.¿Cuántos pedidos se han tomado de más de 150 euros?
select count(*) "Pedidos"
from pedido p, producto pr
where p.fabcod=pr.fabcod and p.prodcod=pr.prodcod and precio > 150; 

--11.Halla el número total de pedidos, el importe medio, el importe total de los mismos.
select count(*) "Pedidos", round(avg(importe),2) "Importe Medio", sum(importe) "Importe Total"
from pedido;

--12.¿Cuántos puestos de trabajo diferentes hay en la empresa?
select count(distinct puesto) 
from repventa;

--13.¿Cuántas oficinas de ventas tienen representantes que superan sus propias cuotas?
select count(distinct o.ofinum) "Ofinas que superan sus cuotas"
from oficina o, repventa r
where o.ofinum=r.ofinum and r.ventas > cuota;

--14.¿Cuál es el importe medio de los pedidos tomados por cada representante?
select nombre, round(avg(importe),2) "Importe Medio"
from repventa r, pedido p
where r.repcod=p.repcod
group by r.repcod, nombre;

--15.¿Cuál es el rango de las cuotas de los representantes asignados a cada oficina 
--(mínimo y máximo)?
select nombre, min(cuota), max(cuota)
from repventa r, oficina o
where r.ofinum=o.ofinum
group by r.nombre;

select r.ofinum 

--16.¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad y número 
--de representantes.
select count(*)"Numero de representantes", ciudad
from repventa r, oficina o
where r.ofinum=o.ofinum
group by o.ofinum;

--16.bis. Idem però nomès de les ofcines que tenen almenys 2 representants
select count(*)"Numero de representantes", ciudad
from repventa r, oficina o
where r.ofinum=o.ofinum
group by o.ofinum
having count(*) >= 2;

--17.¿Cuántos clientes ha contactado por primera vez cada representante? Mostrar 
--el código de representante, nombre y número de clientes.
select r.repcod"Codigo", r.nombre"Representante", count(*)"Numero de Clientes" 
from repventa r, cliente cl
where r.repcod=cl.repcod
group by r.repcod, r.nombre;

--18.Calcula el total del importe de los pedidos solicitados por cada cliente 
--a cada representante.
select sum(importe)"Importe Total"
from repventa r, cliente cl, pedido p
where r.repcod=cl.repcod and p.cliecod=cl.cliecod;

--19.Lista el importe total de los pedidos tomados por cada representante.
select r.repcod, nombre, coalesce(sum(importe),0)"Importe Total"
from pedido p right join repventa r
	on r.repcod=p.repcod
group by r.repcod, nombre;

--20.Para cada oficina con dos o más representantes, calcular el total de las cuotas 
--y el total de las ventas de todos sus representantes.
select ciudad, region, sum(cuota) "Cuota Total", sum(r.ventas) "Venta Total"
from oficina o, repventa r
where o.ofinum=r.ofinum
group by o.ofinum, ciudad, region
having count(*) >= 2;

--21.Muestra el número de pedidos que superan el 75% de las existencias.
select (exist/100*cant) > 75 "Pedidos"
from pedido p, producto pr
where p.fabcod=pr.fabcod and p.prodcod=pr.prodcod;

-- Tot camp que no sigui funció de grup i que estigui al select s'ha de posar al group by
--1.from-2.where-3.group by-4(calcular funció de gup)-5.having-6.order by-7.limit-8.select

--cada vegada que es crea una clau primaria en cualsevol sistema gestor el sistema crea un index 

