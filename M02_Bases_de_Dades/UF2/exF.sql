--Alejandro Carreño Rubio 
--iaw46487690
--18/12/2018
--exF.sql

--Exercicis Videoclub:

--1.Mostra el titol de la pel.lícula, el nom del seu director així com el
--seu gènere (no el codi).
select titol, nom, genere
from pelicula p, director d, genere g
where p.coddir=d.coddir and p.codgen=g.codgen;

--2.Es vol saber quins dvds hi ha per cada peli. Es demana el codi de
--cada pel.lícula, el títol, i el codi del dvd.
select distinct(p.codpeli), titol, coddvd
from pelicula p, dvd d
where p.codpeli=d.codpeli
group by p.codpeli, coddvd
order by 1;

--3.Es vol saber les pel.lícules que actualment estan en préstec. Mostrar
--el nom del soci, el nom de la peli, la data de préstec i la devolució.
select nom, titol, datapres, datadev
from soci s, pelicula p, lloguer l, dvd d
where s.codsoci=l.codsoci and p.codpeli=d.codpeli and d.coddvd=l.coddvd;

--4.Es vol saber les pel.lícules que en el passat van estar llogades.
--Mostrar el nom del soci, el nom de la peli, la data de préstec i la
--devolució.
select nom, titol, datapres, datadev
from soci s, pelicula p, lloguer l, dvd d
where s.codsoci=l.codsoci and p.codpeli=d.codpeli and d.coddvd=l.coddvd
	and datapres>current_date;

--5.Es vol mostrar la llista d’espera de socis i pelis, ordenada per codi
--de peli i per data ascendentment. Mostreu nom de la peli, nom del
--soci i data de reserva.
select titol, nom, datapres
from soci s, pelicula p, lloguer l, dvd d
where s.codsoci=l.codsoci and p.codpeli=d.codpeli and d.coddvd=l.coddvd;


--6.Mostra el repartiments de totes les pelis. Mostra el títol de la peli i
--el nom dels actors.
select titol, nom
from pelicula p, repartiment r, actor a
where p.codpeli=r.codpeli and r.codactor=a.codactor;

--1.­ Obtener los nombres de los socios que tienen actualmente prestada una película que
--ya tuvieron prestada con anterioridad.


--2.­ Obtener el título de la película o películas que han sido prestadas más veces.


--3.­ Obtener el título de las películas que han sido prestadas a todos los socios del
--videoclub.


--4.­ Obtener el nombre y la dirección del socio o socios más peliculeros, es decir, los que
--han tomado el mayor número de películas distintas.


--5.­ Obtener los títulos de las películas que nunca han sido prestadas.


--6.­ Obtener el título de la película o películas cuya lista de espera es la más larga.


--7.­ Obtener los nombres de los socios que han tomado prestada la película Blancanieves
--alguna vez o que están esperando para tomarla prestada.


--8.­ Obtener los nombres de los socios que han tomado prestada la película Blancanieves
--alguna vez y que además están en su lista de espera.


--9.­ Obtener una lista de los géneros cinematográficos de los que se han realizado más de
--3  prestamos.


--10.­ Nombre y teléfono de las personas que llevan más tiempo en la lista de espera para
--la película “Lo que el viento se llevó”


--11.­ Obtener los datos del socio que ha tomado prestadas más películas del actor Alex
--Bonnin.
select codsoci, count(*) "Prestecs"
from taules
where joins
group by
having count(*) = (select max(x)
					from tt

