--Alejandro Carreño Rubio 
--iaw46487690

--Exercicis de joins

--1.Muestra un listado con todos los pedidos: su número, importe, nombre de cliente y 
--límite de crédito.
select pednum, importe, nombre, limcred
from cliente cl join pedido p
	on cl.cliecod=p.cliecod;


--2.Lista cada uno de los representantes, mostrando el nombre, la ciudad y región en la 
--que trabajan ¿Cuántas filas salen? ¿No debería salir una más? Explica el motivo.
select nombre, ciudad, region
from repventa r left join oficina o
	on r.ofinum=o.ofinum;

--2.bis.Ha de sortir "---" on hi ha nulls.
select nombre, coalesce(ciudad,'---')"ciudad", coalesce(region,'---')"region"
from repventa r left join oficina o
	on r.ofinum=o.ofinum;

--3.Muestra un listado de oficinas donde aparezca el codigo de oficina, la ciudad, 
--el nombre del director y su puesto de trabajo.
select o.ofinum, ciudad, nombre, puesto
from oficina o join repventa r
	on o.director=r.repcod;

--4.Como la consulta 3 pero añadiendo que el objetivo ha de ser superior a 600.000
select o.ofinum, ciudad, nombre, puesto, objetivo
from oficina o join repventa r
	on o.director=r.repcod
where objetivo > 600000;

--5.Mostrar el nombre del cliente y nombre del representante que contactó con el por
--primera vez.
select cl.nombre cliente, r.nombre "representante que lo fichó"
from cliente cl join repventa r
	on cl.repcod=r.repcod;
	
--5.bis.Mostrar per cada representant quins clients han fitchat, ordenat per representant
--i desprès per client.
select r.nombre "representante que lo fichó", cl.nombre cliente
from cliente cl join repventa r
	on cl.repcod=r.repcod
order by r.nombre, cl.nombre;


--6.Listado de pedidos, mostrando el importe, fecha, descripcion del producto, cantidad, 
--precio unitario e importe del pedido.
select importe, fecha, descrip, cant, precio
from pedido p inner join producto pr
on p.fabcod=pr.fabcod and p.prodcod=pr.prodcod;

--El inner es opcional

--7.Como la consulta anterior pero añadiendo el nombre del representante que tomó el 
--pedido y que además el importe del pedido sea superior a 25.000.
select importe, fecha, descrip, cant, precio, nombre
from pedido p, producto pr, repventa r
where p.fabcod=pr.fabcod and 
p.prodcod=pr.prodcod and p.repcod=r.repcod and importe > 25000;

--8.Mostrar un listado de los representantes con su jefe, es decir,  código y nombre de 
--representante y lo mismo para su jefe.
select r.repcod representant,r.nombre, r.jefe, jefe.nombre
from repventa r left outer join repventa jefe
	on r.jefe=jefe.repcod;
	
--9.Mostrar todos los representantes (incluyendo los que no tienen asignada una oficina). 
--Nombre del representante, puesto, ventas, código de oficina y ciudad.
select nombre, puesto, r.ventas, o.ofinum, ciudad
from repventa r left join oficina o
	on r.ofinum=o.ofinum;

--10.Como la 8 pero mostrando todos los representantes.
select r.repcod representant,r.nombre, coalesce(r.jefe::text,'---'), 
		coalesce(jefe.nombre::text,'---')
from repventa r left outer join repventa jefe
	on r.jefe=jefe.repcod;

--11.Obtener una lista de los pedidos con importes superiores a 150 euros, mostrando 
--el código del pedido, el importe, el nombre del cliente que lo solicitó, el nombre del 
--representante que contactó con él por primera vez y la ciudad de la oficina donde el 
--representante trabaja.
select pednum, importe, cl.nombre, r.nombre, ciudad
from pedido p, cliente cl, repventa r, oficina o
where cl.repcod=r.repcod and p.repcod=r.repcod and o.ofinum=r.ofinum;

--12.Lista los pedidos tomados durante el mes de octubre del año 2003 , mostrando 
--solamente el número del pedido, su importe, el nombre del cliente que lo realizó, 
--la fecha y la descripción del producto solicitado.
select pednum, importe, nombre, fecha, descrip
from pedido p, cliente cl, producto pr
where to_char (fecha,'yyyy mm')='2003 10' and cl.cliecod=p.cliecod 
		and p.fabcod=pr.fabcod and p.prodcod=pr.prodcod;

--13.Obtener una lista de todos los pedidos tomados por representantes de oficinas de 
--la región Este, mostrando solamente el número del pedido, la descripción del producto 
--y el nombre del representante que lo tomó.
select pednum, descrip, nombre
from pedido p, oficina o, producto pr, repventa r
where lower(region)='este' and p.fabcod=pr.fabcod and p.prodcod=pr.prodcod
		and p.repcod=r.repcod and o.ofinum=r.ofinum;

--14.Obtener los pedidos tomados en los mismos días en que un nuevo representante fue 
--contratado. Mostrar número de pedido, importe, fecha pedido.
select pednum, importe, fecha
from pedido p, repventa r
where fecha=fcontrato;

--15.Obtener una lista con parejas de representantes y oficinas en donde la cuota del 
--representante es mayor o igual que el objetivo de la oficina, sea o no la oficina en 
--la que trabaja. Mostrar Nombre del representante, cuota del mismo, Ciudad de la oficina, 
--objetivo de la misma.
select nombre, cuota, ciudad, objetivo
from repventa r, oficina o
where r.ofinum=o.ofinum and r.cuota >= o.objetivo;

--16.Muestra el nombre, las ventas y la ciudad de la oficina de cada representante de 
--la empresa.
select nombre, r.ventas, ciudad
from repventa r, oficina o
where r.ofinum=o.ofinum;

--17.Obtener una lista de la descripción de los productos para los que existe algún 
--pedido en el que se solicita una cantidad mayor a las existencias de dicho producto.
select descrip
from producto pr, pedido p
where p.fabcod=pr.fabcod and p.prodcod=pr.prodcod and cant > exist;

--18.Lista los nombres de los representantes que tienen una cuota superior a la de 
--su jefe. Mostrar nombre y cuota del representante y nombre y cuota de su jefe.
select jefe.nombre, jefe.cuota, r.nombre, r.cuota
from repventa r right join repventa jefe
	on r.repcod=jefe.jefe and r.cuota > jefe.cuota;

--19.Obtener una lista de los representantes que trabajan en una oficina distinta de 
--la oficina en la que trabaja su jefe, mostrando también el nombre del director y el 
--código de la oficina donde trabaja cada uno de ellos.
select r.nombre, r.ofinum, dir.nombre, dir.ofinum
from repventa r, repventa jefe, repventa dir, oficina o
where r.jefe=jefe.repcod and r.ofinum!=jefe.ofinum 
		and r.ofinum=o.ofinum and o.director=dir.repcod;

--20.El mismo ejercicio anterior, mostrando en lugar de su código de la oficina la 
--ciudad del representante y su jefe.


--21.Mostrar todos los datos de los representantes que son jefe.
select *
from repventa
where jefe is not null;
     
--22.Inseriu una nova oficina (el codi el teniu a sota). D'aquesta manera tindreu 
--un representant sense oficina i una oficina sense representant.  Mostreu de cada 
--representant, el seu codi, el seu nom, la seva oficina i la ciutat. Proveu els 4 
--joins: inner i els 3 outer (left, rigth i full join). La sintaxi del  join es:
--T1 { [INNER] | { LEFT | RIGHT | FULL } [OUTER] } JOIN T2 ON cond.
--insert into oficina
--values (10, 'Barcelona','Este',null,0,0);
