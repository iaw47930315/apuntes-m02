--Gestión De Proyectos iaw46487690
drop table proyecto;
drop table empleado;
drop table departamento;
create table departamento (
NumDepartamento int,
nombre varchar(20) constraint departamento_nombre_nn not null,
tareas varchar(100),
constraint departamento_NumDepartamento_pk primary key(NumDepartamento)
);
create table empleado (
DNI varchar(9),
nombre varchar(20) constraint empleado_nombre_nn not null,
direccion varchar(40),
telefono varchar(13),
proyecto varchar(30),
NumDepartamento int,
jefe varchar(9),
codigo int,
constraint empleado_DNI_pk primary key(DNI),
constraint empleado_jefe_fk foreign key(jefe) references empleado(DNI),
constraint empleado_NumDepartamento_fk foreign key(NumDepartamento) references departamento(NumDepartamento)
);
create table proyecto (
codigo int,
titulo varchar(20),
dEstimada date,
dReal date,
presupuesto numeric(8,2),
DNI varchar(9),
constraint proyecto_codigo_pk primary key(codigo),
constraint proyecto_DNI_fk foreign key(DNI) references empleado(DNI)
);
alter table proyecto add constraint empleado_codigo_fk foreign key(codigo) references proyecto(codigo);
alter table proyecto delete subtitulo varchar(20);

insert into departamento
values (111,'Programación', 'Hacer 50 programas');
insert into empleado
values ('12345678Q', 'Sara', 'Av.Sal', '+34 123456789', 'Programa 1', 111, '12345678Q');
insert into proyecto
values (123, 'Programas Bla', current_date, to_date('30-11-18', 'DD-MM-YY'), 400000, '12345678Q');

insert into departamento
values (222, 'Diseño', 'Diseñar 3 bases de datos');
insert into empleado
values ('12345678T', 'Maria', 'Av.Azúcar', '+34 987654321', 'Diseño 1', 222, '12345678T');
insert into proyecto
values (456, 'Diseños Bla', current_date, to_date('30-11-18', 'DD-MM-YY'), 500000, '12345678T');
