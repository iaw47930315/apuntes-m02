--Museus iaw46487690
\c template1
drop database museus;
create database museus;
\c museus
drop table Obra;
drop table autor;
drop table SalaMuseo;
drop table museo;
drop table Ciudad;
create table Ciudad (
IdCiudad varchar(20),
Ciudad varchar(20),
Pais varchar(20),
constraint Ciudad_IdCiudad_pk primary key (IdCiudad)
);
create table museo (
IdMuseo int,
nombre varchar(20),
direccion varchar(40),
IdCiudad varchar(20),
constraint museo_IdMuseo_pk primary key (IdMuseo),
constraint museo_IdCiudad_fk foreign key (IdCiudad) references Ciudad(IdCiudad)
);
create table SalaMuseo (
IdMuseo int,
idSala int,
constraint SalaMuseo_IdMuseo_pk primary key (IdMuseo,idSala),
constraint museo_IdMuseo_fk foreign key (IdMuseo) references museo(IdMuseo)
);
create table autor (
codAutor int,
nombre varchar(20),
nacionalidad varchar(20),
constraint autor_codAutor_pk primary key (codAutor)
);
create table Obra (
codObra int,
titulo varchar(20),
tipo varchar(20),
IdMuseo int,
idSala int,
codAutor int,
constraint Obra_codObra_pk primary key (codObra),
constraint Obra_codAutor_fk foreign key (codAutor) references autor(codAutor),
constraint Obra_IdMuseo_fk foreign key (IdMuseo,idSala) references SalaMuseo(IdMuseo,idSala),
constraint Obra_tipo_ck check(tipo='pintura' or tipo='escultura')
);
