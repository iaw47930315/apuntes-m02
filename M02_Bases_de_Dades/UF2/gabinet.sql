--Gabinete de Abogados iaw46487690 
drop table asuntoxProcurador;
drop table procurador;
drop table asunto;
drop table client;
CREATE TABLE client (
DNI VARCHAR(9),
nom VARCHAR(20) constraint client_nom_nn not null,
adreça VARCHAR(40),
telefon VARCHAR(13),
CONSTRAINT client_DNI_pk PRIMARY KEY(DNI)
);
CREATE TABLE asunto (
NumExpediente VARCHAR(20),
FechaInicio date constraint asunto_FechaInicio_nn not null,
FechaArchivo date,
Estado VARCHAR(20),
DNI VARCHAR(9),
CONSTRAINT asunto_NumExpediente_pk Primary KEY(NumExpediente),
CONSTRAINT asunto_DNI_fk FOREIGN KEY(DNI) REFERENCES client (DNI),
constraint asunto_Estado_ck check(Estado='O'or Estado='T')
);
CREATE TABLE procurador (
DNI VARCHAR(9),
nom VARCHAR(20) constraint procurador_nom_nn not null,
adreça VARCHAR(40),
telefon VARCHAR(13),
CONSTRAINT procurador_DNI_pk PRIMARY KEY(DNI),
CONSTRAINT procurador_nom_uk UNIQUE(nom)
);
CREATE TABLE asuntoxProcurador (
NumExpediente VARCHAR(20),
DNI VARCHAR(9),
CONSTRAINT asuntoxProcurador_NumExpediente_fk foreign KEY(NumExpediente) REFERENCES asunto(NumExpediente),
constraint asuntoxProcurador_DNI_pk foreign key(DNI) REFERENCES procurador (DNI),
constraint asuntoxProcurador_pk primary key (NumExpediente,DNI)
);
insert into client
values ('12345678K', 'Paco', 'Av.Marina', '+34 123456789');
insert into asunto
values (12345, to_date('25-09-07', 'DD-MM-YY'), current_date, 'O', '12345678K');
insert into procurador
values ('23456789L', 'Rafa', 'Av.Mar', '+34 789456123');
insert into asuntoxProcurador
values (12345, '23456789L');

insert into client
values ('87654321P', 'Daniel', 'Av.Masnou', '+34 987654321');
insert into asunto
values (54321, to_date('25-09-07', 'DD-MM-YY'), current_date, 'O', '87654321P');
insert into procurador
values ('98765432M', 'Raquel', 'Av.Meridiana', '+34 789456000');
insert into asuntoxProcurador
values (54321, '98765432M');
