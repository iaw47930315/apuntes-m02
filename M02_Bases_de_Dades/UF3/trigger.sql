--Exemple 1
CREATE TRIGGER trigger_cliente_historico
AFTER INSERT ON cliente
FOR EACH ROW
BEGIN
	INSERT INTO cliente_historico(nombre, dni, direccion)
	VALUES (NEW.nombre, NEW.dni, NEW.direccion, CURRENT DATE());
END;

--Exemple 2
CREATE TRIGGER trigger_usuario_identificador
AFTER UPDATE ON usuario
FOR EACH ROW
BEGIN
	UPDATE usuario SET identificador
END;

--Exemple 3
select * from soci;
select * from historic_reg;

CREATE OR REPLACE FUNCTION insertar_trigger 
RETURNS TRIGGER AS $insertar$ 
DECLARE
--Inserta cos(dispara)
BEGIN
INSERT INTO historial_reg () VALUES (OLD.codi, OLD.nom, OLD.date)
RETURN NULL
END;
$insertar$ LANGUAGE plpgsql;

--Exemple 3(cont.)
CREATE TRIGGER insertar_historial_reg AFTER DELETE
ON socis FOR EACH ROW
EXECUTE PROCEDURE insertar_trigger();

CREATE TRIGGER actualitzar AFTER UPDATE
ON soci FOR EACH ROW
EXECUTE PROCEDURE insertar_trigger();

DELETE FROM SOCI WHERE codi = 3;

UPDATE soci SET 

--Trigger tRaiseTrashUpdate que impedeix fer un update sobre el camp salari de la taula emp
--si no n'hi ha un canvi de valor
CREATE TRIGGER tRaiseTrashUpdate
AFTER UPDATE ON emp 
FOR EACH ROW
WHEN (OLD.sal IS DISTINCT FROM NEW.sal)
EXECUTE PROCEDURE sal_update();

create function sal_update()
RETURNS TRIGGER AS $actualitzar$
BEGIN
update emp set new.sal
END;
$actualitzar$ LANGUAGE plpgsql;


--Crear un trigger que només deixi baixar preus(update), sino que provoqui una excepció
create trigger baix_preu 
after update on emp
FOR EACH ROW
WHEN (OLD.* IS DISTINCT FROM NEW.*)
begin
if new.precio < precio then
update producto set precio;
end if;
end;
update producto set precio = 700 where prodcod = '4100y';
--1.Fer un trigger per actualitzar el camp vendes de la taula repventa quan 
--es faci una nova comanda. Comproveu que funciona.


--2.Modifiqueu-lo per actualitzar també el camp ventas de la taula oficina





--Database triggers
create table control (
	id serial primary key,
	usuari varchar(50),
	data timestamp,
	tipusOp varchar(20)
);

drop function fControl();
create or replace function fControl()
returns trigger
as $$
begin
	insert into control
	values(default, current_user, current_timestamp, tg_op);
	return null;
end;
$$ LANGUAGE plpgsql;

drop trigger on emp;
create trigger tControl
after update or insert or delete on emp
for each row
execute procedure fControl();
--for each row, devuelve la comprovacion por cada fila afectada
--for each statement, devuleva una comprovacion para todas las filas afectadas
update emp set sal=1000;
select * from control;
/* id |   usuari    |            data            | tipusop 
----+-------------+----------------------------+---------
  1 | iaw46487690 | 2019-05-21 13:32:20.285923 | UPDATE
(1 row)*/


/*     |   OLD   |   NEW   |
-------+---------+---------|
insert |    NO   |    SI   |
update |    SI   |    SI   |
delete |    SI   |    NO   |
*/

--Trigger que insereix un nou empleat amb totes les dades
create trigger tControl
instead of update or insert or delete on emp
for each row
execute procedure fControl();
