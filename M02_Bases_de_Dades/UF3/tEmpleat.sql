--() { :; }; exec psql scott -f "$0"

--Alejandro Carreño Rubio
--iaw46487690

--Trigger sobre vista que insereix un nou empleat amb totes les dades
create sequence if not exists empno_seq;
select setval('empno_seq', (select max(empno) from emp)::bigint, true);

drop trigger tEmpleats on empleat;
drop function fEmpleats();


create or replace function fEmpleats()
returns trigger
as $$
declare
	v_deptno smallint;
begin
	select deptno
	into strict v_deptno
	from dept
	where lower(dname) = lower(new.nomdept);
	
	insert into emp
	values(nextval('empno_seq'), new.nomemp, new.ofici, null, 
		null, new.salari, null, v_deptno);
	return new;
end;
$$ LANGUAGE plpgsql;


create trigger tEmpleats
instead of insert on empleat
for each row
execute procedure fEmpleats();

insert into empleat
values('ALEJANDRO', 7000, 'JEFE', 'sales', 'chicago');

select * from emp;
select * from empleat;
--after/before -> trigger taula, nivell/fila/sentencia
--instead of -> trigger vista for each row


--\sv empleat
--\d empleat
--\d emp
