--select		(camps)
--into strict	(variables/parametresformals de sortida)
--from			(taula)

--BDscott. Crear la funció getEmpById que en donar com argument el codi 
--del empleat, retorna el seu nom

create or replace function getEmpById(p_empno emp.empno%type)
returns varchar
as $$
	declare
		v_ename varchar(100);
	begin
		select ename 
		into strict v_ename
		from emp
		where empno=p_empno;
		return v_ename;
		exception
			when NO_DATA_FOUND THEN
            return ('El empleado '|| p_empno ||' no existe');
	end; 
$$ language plpgsql;


--en el execption se puede poner un return o un raise exception
