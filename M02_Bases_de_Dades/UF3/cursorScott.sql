--() { :; }; exec psql scott -f "$0"
--Alejandro Carreño Rubio
--iaw46487690
--cursorScott.sql

--Adapteu l'exemple del cursor per a la BBDD d'Scott.

--psql scott -f '/home/users/inf/hiaw1/iaw46487690/Desktop/M02_Bases_de_Dades/cursorScott.sql'

drop function if exists get_dept_name();
drop function if exists get_dept_name2();

CREATE OR REPLACE FUNCTION get_dept_name()
RETURNS text
AS $$
DECLARE
 dept_names TEXT DEFAULT '';
    -- declarem una variable fila anomenada rec_dept, la qual reculirà cada fila
    -- que es llegeix (fetch) del cursor
    -- Oracle permet %rowtype per taules i cursors, postgreSQL per taules només
 rec_dept   RECORD;
    -- Els cursors poden admetre paràmetres.
    -- Aquest és un exemple de cursor bàsic, sense paràmetres
 cur_depts CURSOR FOR          -- postgreSQL usa habitualment "FOR"
     SELECT deptno, dname      -- pero permet "IS" com a gest cap  a Oracle
     FROM dept;
BEGIN
OPEN cur_depts;
   LOOP
      -- fetch row into the dept
      FETCH cur_depts INTO rec_dept;
      -- exit when no more row to fetch
      -- La sintaxi de postgreSQL difereix una
      -- mica d'Oracle, que sería així:
      -- EXIT WHEN cur_depts%NOTFOUND;
      EXIT WHEN NOT FOUND;
      -- build the output
      IF rec_dept.deptno = '40' or rec_dept.deptno = '10' THEN
         dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
         || rec_dept.deptno;
      END IF;
   END LOOP;
   -- Close the cursor
   CLOSE cur_depts;

--WHILE   
OPEN cur_depts;
FETCH cur_depts INTO rec_dept;
WHILE FOUND
	LOOP
		IF rec_dept.deptno = '20' or rec_dept.deptno = '30' THEN
         dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
         || rec_dept.deptno;
		END IF;
		FETCH cur_depts INTO rec_dept;
	END LOOP;
CLOSE cur_depts;

--FOR
FOR rec_dept IN cur_depts
	LOOP
		IF rec_dept.deptno = '20' or rec_dept.deptno = '30' THEN
        dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
        || rec_dept.deptno;
		END IF;
	END LOOP;

   RETURN dept_names;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION get_dept_name2()
RETURNS text
AS $$
DECLARE
 dept_names TEXT DEFAULT '';
 rec_dept   RECORD;
 cur_depts CURSOR
 FOR SELECT deptno, dname
     FROM dept
     WHERE deptno = 10;
BEGIN
OPEN cur_depts;
	LOOP
	FETCH cur_depts into rec_dept;
	EXIT WHEN NOT FOUND;
        dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
        || rec_dept.deptno;
	END LOOP;
CLOSE cur_depts;

--WHILE
OPEN cur_depts;
FETCH cur_depts INTO rec_dept;
WHILE FOUND
	LOOP
         dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
         || rec_dept.deptno;
		FETCH cur_depts INTO rec_dept;
	END LOOP;
CLOSE cur_depts;

--FOR
FOR rec_dept IN cur_depts
	LOOP
        dept_names := dept_names || E'\n' || rpad(rec_dept.dname,15,' ') 
        || rec_dept.deptno;
	END LOOP;

   RETURN dept_names;
END;
$$ LANGUAGE plpgsql;


-- Comprovació
select get_dept_name();
select get_dept_name2();


/*
-- Open the cursor
   OPEN cur_depts
FETCH cur_depts INTO rec_dept;
WHILE FOUND
LOOP
	 IF rec_dept.deptno = '40' or rec_dept.deptno = '10' THEN
         RAISE NOTICE 'Departament: % Numero: %', rec_dept.dname, rec_dept.deptno;
     END IF;
END LOOP;
CLOSE cur_depts;
*/


