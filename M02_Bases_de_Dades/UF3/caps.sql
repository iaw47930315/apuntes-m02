--Creeu sobre la BBDD training la vista cap. La seva estructura serà codi, nom, 
--oficina i representants (que hi contendrà el nombre d'empleats al seu càrrec)
--En lloc de crear alies en la consulta, poseu-los a la definició de la vista.
create or replace view cap
as select repcod "Codi", nombre "Nom", r.ofinum "Oficina", 
	(select count(repcod) "Representants"
		from repventa re
		where re.jefe=r.repcod)
from repventa r join oficina o
	on r.ofinum=o.ofinum
where r.repcod = ANY (select jefe from repventa)
group by r.repcod, r.nombre, o.ciudad;
									
									
									
									
--BDD scott									
select deptno, count(*) 
from emp
where deptno=10 
group by deptno; 

select count(*) 
from emp
where deptno=10;

select count(*) 
from emp
group by deptno; 


-- Mostrar el nom dels empleas y el seu treball en castella
select job,
	case when job='CLERK' then 'EMPLEADO'
		when job='SALESMAN' then 'VENDEDOR'
		when job='MANAGER' then 'DIRECTOR'
		when job='ANALYST' then 'ANALISTA'
		when job='PRESIDENT' then 'PRESIDENTE'
	end
from emp;

    job    |    case    
-----------+------------
 CLERK     | EMPLEADO
 SALESMAN  | VENDEDOR
 SALESMAN  | VENDEDOR
 MANAGER   | DIRECTOR
 SALESMAN  | VENDEDOR
 MANAGER   | DIRECTOR
 MANAGER   | DIRECTOR
 ANALYST   | ANALISTA
 PRESIDENT | PRESIDENTE
 SALESMAN  | VENDEDOR
 CLERK     | EMPLEADO
 CLERK     | EMPLEADO
 ANALYST   | ANALISTA
 CLERK     | EMPLEADO 
(17 rows)
