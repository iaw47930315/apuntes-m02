--() { :; }; exec psql biblioteca -f "$0"

/* ************************************************************
documentDisponible

Tasca:
	Donat un títol de document com a argument, torna el primer idExemplar disponible
	Ha de retornar un codi 0 si no hi ha cap exemplar disponible

	IMPORTANT: cal tenir en compte l'estat!!

Entrada:
		 p_titol (varchar): Títol del document a cercar
Retorna:
		 v_idExemplar: id d'exemplar disponible o 0 en cas què no n'hagi cap
************************************************************ */
create or replace function documentDisponible(p_titol varchar)
returns int
as $$
declare
	 v_exemplar record;
	 c_exemplar CURSOR IS
	  	 select idExemplar
		   from exemplar e, document d
		   where e.idDocument = d.idDocument and lower(titol) = lower (p_titol) and
			    estat != 'Exclòs de prèstec' and 	idExemplar not in
																												(select idExemplar
																												from prestec
																												where datadev is null);
begin
	-- Utilitzem CURSOR per obtenir una fila, perquè sinó dóna l'error:
	-- 													ERROR:  query returned more than one row
  open c_exemplar;
	fetch c_exemplar into v_exemplar;
  close c_exemplar;
	return coalesce(v_exemplar.idExemplar,0);
end;
$$ language plpgsql;

-- Us poso el joc de proves dintre d'aquest script, però la idea és que tingueu
-- un script anomenat joc.sql i que tingueu un .sh que executi tots els scripts
-- que vulgueu. Exemple del que pot incloure:
-- 			Creació BD del cas pràctic
-- 			Funcions d'usuari del cas pràctic
-- 			Joc de proves, el qual podrà incloure inserts o qualsevol op. DML si s'escau

\t
select E'--------------\nJOC DE PROVES\n--------------';
select '(1) Del llibre "Fundamentos de Bases de datos" ha de sortir l''exemplar 2 ó 3';
select documentDisponible('Fundamentos de Bases de datos');
select '(2) El CD "Highway to hell" té dos exemplars, el 4 prestat i el 5 disponible';
select '    Ha de sortir l''exemplar 5';
select documentDisponible('Highway to hell');
select ('(3) La peli "la la land" té un exemplar per a prestar (prestat actualment)');
select ('    i altre exemplar Exclòs de prèstec, amb el què la funció ha de retornar un 0');
select documentDisponible('la La Land');
select ('(4) El document "Aquest document no existeix" no existeix');
select ('    Ha de retornar un id d''exemplar 0, que vol dir cap exemplar disponible');
select documentDisponible('Aquest document no existeix');
