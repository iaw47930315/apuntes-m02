--Exercici DCL: grant i revoke

--1.Creeu el rol rol_repventa
create role rol_repventa;

--2.Assigneu privilegis de lectura sobre la taula pedido de la BBDD training
grant select on table pedido to rol_repventa;

--3.Creeu el rol rol_scott
create role rol_scott;

--4.Assigneu tots els privilegis (select, insert, update, delete) sobre totes les 
--taules de la BBDD scott.
grant all on database scott to rol_scott;

--5.Assignar els rols al company.
grant rol_scott to iaw47999217;
grant rol_repventa to iaw47999217;
grant rol_scott to iaw47930315;
grant rol_repventa to iaw47930315;

--6.Per canvia de rol, heu de fer "set role nou_rol;"
set role iaw47999217;

--7.Comproveu que podeu fer només el que el rol us marca
psql -U iaw47999217 -h h11 -d template1;
\c training
select * from pedido;
select current_user, session_user;
set role rol_repventa;
select current_user, session_user;
select * from pedido;
--ok
select * from cliente;
delete from cliente;

--8.Per tornar al vostre rol, "reset role;"
reset role;

--9.Al rol rol_scott treieu els privilegis d'update i delete.
revoke update, delete on dept, emp, salgrade from rol_scott;
--10.Comprova TOTES les operacions (es demana totes les consultes per verificar-ho)

--Nota: si no deixa esborrar un role, executeu primer DROP OWNED BY nom_role; 
--A continuació, torneu a esborrar el role.
