--Alejandro Carreño Rubio
--iaw46487690
--myEpoch4.sql

--Igual que el 3 pero pot acceptar o no arguments

drop function if exists myEpoch4(timestamp);

create or replace function myEpoch4(date timestamp default current_timestamp)
returns varchar
    as $$
        declare year integer;
        declare month integer;
        begin
            year := to_char(current_date,'yyyy');
            month := to_char(current_date,'mm');
            return to_char(to_date(year || '-' || month || '-01', 'yyyy-mm-dd') 
            + interval '1 month' - interval '1 days','yyyy-mm-dd');
        end; 
$$ language plpgsql;

select myEpoch4('02-03-1970 00:00:00');
select myEpoch4();
