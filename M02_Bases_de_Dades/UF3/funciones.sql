--Alejandro Carreño Rubio 
--iaw46487690

--Funcions predefinides del sistema

--1.BBDD Scott: Fes una consulta per treure del camp ename sense cap accentuació. 
--S'han de contemplar totes les possibilitats : accents, dièresi, caràcters especials 
--ç, ñ, majúscules i minúscules.
select translate (ename, 'áàéèíóòúçñüïÁÀÉÈÍÓÒÚÇÑÜÏ', 'aaeeiooucnuiAAEEIOOUCNUI')
from emp;

--2.BBDD TRAINING: Es demana una sentència SQL per a crear i carregar una taula que es 
--dirà emp2 on, a més a més dels camps propis de la taula, tindrem el camp login 
--(que serà la inicial del nom i el cognom.)

--L'exercici desglosat seria així:

--A.Per a cada representant, mostrar el camp  nombre i la posició on comença el cognom
select nombre, position(' ' in nombre)+1 posicion
from repventa;
--B.Per cada representant, mostra el el camp nombre (tal com està a la base de dades) i
--la columna Apellido
select nombre, substr(nombre, strpos(nombre,' ')+1) "Apellido"
from repventa;

--C.Per cada representant, mostra el camp nombre i el seu login
select substr(lower(nombre),1,1) || split_part(lower(nombre),' ',2) "login"
from repventa;

--D.Ja podem fer l'exercici complet!
create table repventa2 as
select *, lower(substr((nombre),1,1) || split_part((nombre),' ',2)) "login"
from repventa;





create or replace function helloWorld()
returns varchar as $$
begin
return 'Hello World';
end;
$$ language plpgsql;
--No cridar select desde otra funcion
select helloWorld();


