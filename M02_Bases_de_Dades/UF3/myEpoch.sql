--Alejandro Carreño Rubio 
--iaw46487690

drop function if exists myEpoch();

create or replace function myEpoch()
returns bigint as $$
        declare seconds bigint;
        begin
                seconds = date_part('days', current_timestamp-to_timestamp('1970-01-01',
                'yyyy-mm-dd')) * 86400 + date_part('hour', current_timestamp-to_timestamp
                ('1970-01-01','yyyy-mm-dd')) * 3600 + date_part('minutes', 
                current_timestamp-to_timestamp('1970-01-01','yyyy-mm-dd')) * 60 + 
                date_part('seconds', current_timestamp-to_timestamp
                ('1970-01-01','yyyy-mm-dd'));
                return seconds;
        end;
$$ language plpgsql;

select myEpoch();


--psql training -f Desktop/M02_Bases_de_Dades/myEpoch.sql 
--psql training -c 'select myEpoch()'
