--Alejandro Carreño Rubio 
--iaw46487690

drop databse if exists fechas;
create database fechas;
\c fechas
create or replace function darrerDiaMes()
returns varchar
	as $$
		declare year integer;
		declare month integer;
		begin
			year = date_part('year', current_timestamp);
			month = date_part('month', current_timestamp);
			return to_char(to_timestamp(year || '-' || month || '-01', 'yyyy-mm-dd') + 
			interval '1 month' - interval '1 days', 'yyyy-mm-dd');
		end;
$$ language plpgsql;
			
select darrerDiaMes();
