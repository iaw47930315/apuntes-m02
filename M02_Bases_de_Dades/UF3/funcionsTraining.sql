--Alejandro Carreño Rubio
--iaw46487690
--funcionsTraining.sql

\c training;

--Funció: existeixClient
--Paràmetres: p_cliecod
--Tasca: comprova si existeix el client passat com argument
--Retorna: booleà
drop function if exists existeixClient();

create or replace function existeixClient(p_cliecod int)
returns boolean
as $$
	declare
		v_cliecod int;
	begin
		select cliecod into strict v_cliecod from cliente where cliecod = p_cliecod;
			return true; 
		exception
		when NO_DATA_FOUND then
            return false;
	end;
$$ language plpgsql;

select existeixClient(2111);

--Funció: altaClient
--Paràmetres: p_nombre ,p_repcod, p_limcred  
--Tasca: Donarà d’alta un client.  
--Retorna: Missatge on diu que el client X s’ha donat d’alta correctament
--Nota: si no està creada, creem una seqüència per donar valors a la clau primària. 
--Començarà en el següent valor que hi hagi a la base de dades.
drop function if exists altaClient();

create sequence if not exists cliecod_seq;

select setval('cliecod_seq', (select max(cliecod) from cliente), true);

create or replace function altaClient(p_nombre varchar,p_repcod int, p_limcred numeric(7,2))
returns varchar
as $$
	begin
		insert into cliente(cliecod, nombre, repcod, limcred) 
			values (nextval('cliecod_seq'), p_nombre, p_repcod, p_limcred); 
			return 'Client ' || (select last_value from cliecod_seq) || ' afegit amb exit';
	end;
$$ language plpgsql;

select altaClient('EA SPORTS', 101, 40750);
select altaClient('Rockstar', 102, 30500);
select * from cliente where cliecod=2125;

--Funció: stockOk
--Paràmetres: p_cant, p_fabcod, p_prodcod
--Tasca: Comprova que hi ha prou existències del producte demanat.
--Retorna: booleà
drop function if exists stockOk();

create or replace function stockOk(p_cant int, p_fabcod varchar, p_prodcod varchar)
returns boolean
as $$
	declare
		v_cant varchar;
	begin
		select fabcod, prodcod, exist 
		into strict v_cant 
		from producto 
		where fabcod=p_fabcod and prodcod=p_prodcod and exist >= p_cant;
			return true; 
		exception
		when NO_DATA_FOUND then
            return false;
	end;
$$ language plpgsql;

select stockOk(200, 'rei', '2a45c'); --t
select stockOk(30, 'aci', '4100y'); --f
--select * from producto where fabcod='rei' and prodcod='2a45c' and exists >= 1;

--Funció: altaComanda
--Paràmetres: Segons els exercicis anteriors i segons necessitat, definiu vosaltres 
--els paràmetres mínims que necessita la funció, tenint en compte que cal contemplar 
--l'opció per defecte de no posar data, amb el què agafarà la data de sistema.
--Tasca: Per poder donar d'alta una comanda es tindrà que comprovar que existeix el 
--client i que hi ha prou existències. En aquesta funció heu d'utilitzar les funcions 
--existeixClient i stockOK (recordeu de no posar select function(... ). Evidentment, 
--s'haura de calcular el preu de l'import en funció del preu unitari i de la quantitat 
--d'unitats.

drop function if exists altaComanda();

create sequence if not exists pednum_seq;

select setval('pednum_seq', (select max(pednum) from pedido), true);

create or replace function altaComanda(p_fecha date, p_cliecod int, p_repcod int, 
										p_fabcod varchar, p_prodcod varchar, p_cant int)
returns varchar
as $$
	declare v_importe numeric;
	begin
		--Comprovar si el client existeix
		if not existeixClient(p_cliecod) then
			raise exception using 
				message=('Cliente con cliecod=' || p_cliecod || ' no existeix');
		else
		--Comprovar si n'hi han prou existencies
		if not stockOk(p_cant, p_fabcod, p_prodcod) then
			raise exception using 
				message=('No hi ha suficients existencies del producte');
		else
		--Calcular import de la comanda
		select precio * p_cant
		into strict v_importe
		from producto
		where (fabcod, prodcod) = (p_fabcod, p_prodcod);
		--Insertar nova fila			
		insert into pedido(pednum, fecha, cliecod, repcod, fabcod, prodcod, cant, importe) 
			values (nextval('pednum_seq'), p_fecha, p_cliecod, p_repcod, p_fabcod, 
					p_prodcod, p_cant, v_importe);
		--Actualitzar el producte retirant les unitats de stock
		update producto
		set exist = (exist - p_cant)
		where (fabcod, prodcod) = (p_fabcod, p_prodcod);
		--Actualitzar les ventes dels representants
		update repventa
		set ventas = (ventas + v_importe)
		where repcod = p_repcod;
		--Actualitzar les ventes de les oficines
		update oficina
		set ventas = (ventas + v_importe)
		where ofinum = (select ofinum
						from repventa
						where repcod = p_repcod);
		
		return 'Comanda ' || (select last_value from pednum_seq) || ' afegida amb exit';
	end if;
	end if;	
end;
$$ language plpgsql;

select altaComanda(current_timestamp::date, 1, 105, 'rei', '2a45c', 5);--No existeix client
select altaComanda(current_timestamp::date, 2111, 105, 'rei', '2a45c', 5);



--El representant amb codi 105 fa una comanda de 1300. Sentencies per 
--actualitzar les vendes seves i les de la seva oficina.

update repventa
set ventas = (ventas + 1300)
where repcod = 105;

update oficina
set ventas = (ventas + 1300)
where ofinum = (select ofinum
				from repventa
				where repcod = 105);


--psql training -c 'select * from cliente where cliecod=2125'
/*	
no_data_fonud
to_many_rows
dup_valon_index
*/
/*
into strict + exception
into + found
*/

