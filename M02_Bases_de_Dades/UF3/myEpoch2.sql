--Alejandro Carreño Rubio 
--iaw46487690

--myEpoch2()
--pasar cualsevol data (un argument)

drop function if exists myEpoch2();

create or replace function myEpoch2(timestamp)
returns integer as $$
        declare segons integer;
        declare minuts integer;
        declare hores integer;
        declare dies integer;
        begin
                segons = date_part('seconds', data-to_date
                ('1970-01-01','yyyy-mm-dd'));
                minuts = date_part('minutes', data-to_date
                ('1970-01-01','yyyy-mm-dd'));
                hores = date_part('hour', data-to_date
                ('1970-01-01','yyyy-mm-dd')); 
                dies = date_part('days', data-to_date
                ('1970-01-01','yyyy-mm-dd'));
                return segons + minuts + hores + dies;
        end;
$$ language plpgsql;

select myEpoch2();

