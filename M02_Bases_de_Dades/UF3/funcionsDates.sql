--Alejandro Carreño Rubio
--iaw46487690

--1. Calcular l'ultim dia del mes amb to_char i current_date
drop function if exists darrerDiaMes();

create or replace function darrerDiaMes()
returns varchar
    as $$
        declare year integer;
        declare month integer;
        begin
            year := to_char(current_date,'yyyy');
            month := to_char(current_date,'mm');
            return to_char(to_date(year || '-' || month || '-01', 'yyyy-mm-dd') 
            + interval '1 month' - interval '1 days','yyyy-mm-dd');
        end; 
$$ language plpgsql;

select darrerDiaMes();

--psql training -f Desktop/M02_Bases_de_Dades/funcionsDates.sql 


--2. Calcular l'ultim dia del mes amb to_char i current_date, donant el mes(int)
create or replace function darrerDiaMes2(mes integer)
returns varchar
    as $$
        declare year integer;
        begin
            year := to_char(current_date,'yyyy');
            return to_char(to_date(year || '-' || mes || '-01', 'yyyy-mm-dd') 
            + interval '1 month' - interval '1 days','yyyy-mm-dd');
        end; 
$$ language plpgsql;

select darrerDiaMes2(2);

--3. Calcular l'ultim dia del mes amb to_char i current_date, donant el mes(text)
create or replace function darrerDiaMes3(mes varchar)
returns varchar
    as $$
        declare year integer;
        begin
            year := to_char(current_date,'yyyy');
            return to_char(to_date(year || '-' || mes || '-01', 'yyyy-mes-dd') 
            + interval '1 month' - interval '1 days','yyyy-mm-dd');
        end; 
$$ language plpgsql;

select darrerDiaMes3('may');
--No funciona
