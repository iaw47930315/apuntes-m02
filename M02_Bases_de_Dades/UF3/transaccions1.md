
# Funcions de transaccions
-- --------------------------------------------

Carlos Esteban, Alejandro Carreño i Pol Pastalle

iaw47999217, iaw46487690 i iaw47930315

**Ctrl + Shift + M**  
### Ejecutar una base de datos desde fuera  
**psql -f /home/users/inf/hiaw1/iaw46487690/Desktop/M02_Bases_de_Dades/scott.sql -d scott
**

#### Exercici 1
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```
- A la primera línea fem un insert de les dades, després fa un begin, indicant així que s'inicia la `transacció`.  
Seguidament fa un UPDATE i canvía les dades, però al fer rollback torna a recuperar les dades del principi. Per tant el select mostrará la mateixa informació del INSERT, és a dir **5**.



#### Exercici 2
 Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
- Després de fer l'insert s'inicia la transacció amb BEGIN, es realitza la transacció i es valida amb el COMMIT. Per tant el select mostrará la informació introduïda per l'UPDATE, és a dir sortirà **4**.


#### Exercici 3
 Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
- Després de fer l'INSERT s'inicia la transacció i es fa un UPDATE. Es crea un SAVEPOINT per si falla el códi més endevant.  
Es torna a fer un insert i al acabar la transacció es fa un rollback.  
Com que és un `rollback` i no un `rollback TO a;`, al fer el SELECT ens tornarà a mostrar la informació anterior a la transacció, és a dir sortirá **5**.

#### Exercici 4
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
- Primer borra totes les files de la taula punts, Seguidament torna a fer un INSERT i s'inicía una transacció.  
S'executa un UPDATE i es crea un SAVEPOINT. Es torna a fer un INSERT però es fa un `ROLLBACK TO a`, per tant si fem un `SELECT COUNT(*)` només sortirà una fila.

#### Exercici 5
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
- Seguidament es realitza un INSERT i s'inicía la transacció. El primer SELECT donará ERROR ja que falta un FROM enlloc de un WHERE.
Com que la operació anterior ha fallat, el UPDATE també fallarà i per tant si fem COMMIT, el sistema farà un ROLLBACK automàtic.
Per tant a l'últim SELECT, el valor serà **5**

#### Exercici 6
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
- El script borra totes les files de la taula punts per després fer un INSERT i iniciar la transacció.  
Un cop iniciada la transacció fa un UPDATE de la única fila que hi ha a la taula i genera un primer SAVEPOINT a. Després fa un segon INSERT i genera un altre SAVEPOINT b, seguidament executa un tercer
INSERT però falla ja que el id ja existeix. Per tornar a un punt segur, fa un ROLLBACK al SAVEPOINT b
i després valida la transacció amb un COMMIT.
El valor final del **sum()** serà `12`.

#### Exercici 7
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```
- Com que la connexió 1 no ha fet el COMMIT, la conexió 2 encara pot veure el contingut de la taula. Si la connexió 1 fa un COMMIT, la Connexió 2 ja no veuría les files.

#### Exercici 8
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```
- La connexió 1 inicia la transacció abans que la connexió 2, per tant els canvis que faci la connexió 2 seràn invàlids si es creua informació amb els canvis de la connexió 1.

```
ERROR:  deadlock detected
DETAIL:  Process 4207 waits for ShareLock on transaction 2520; blocked by process 4195.
	Process 4195 waits for ShareLock on transaction 2521; blocked by process 4207.
	Process 4207: UPDATE punts SET valor = 6 WHERE id = 80;
	Process 4195: UPDATE punts SET valor = 10 WHERE id = 81;
HINT:  See server log for query details.
CONTEXT:  while updating tuple (0,26) in relation "punts"
STATEMENT:  UPDATE punts SET valor = 6 WHERE id = 80;
```

#### Exercici 9
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```
- El SELECT final dóna com resultat `9`. La connexió 1 fa un DELETE de totes les files, però la connexió 2 fa un INSERT després i tanca la transacció amb un COMMIT. Per tant encara que la connexió 1 faci un COMMIT més tard, l'INSERT de la connexió 2 serà vàlid.

#### Exercici 10
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```
- La connexió 1 i la connexió 2 fan el mateix UPDATE però amb diferents valors. L'UPDATE de la connexió 2 es queda bloquejada fins que la connexió 1 no fa COMMIT. Un cop fet el COMMIT s'executa l'UPDATE de la connexió 2 i al fer el SELECT ens mostrarà l'UPDATE de la connexió 2, és a dir `7`;

#### Exercici 11
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```
- La connexio 2 es queda bloquejada al primer UPDATE, per tant encara que la connexió 2 executi totes les operacions, fins que la connexió 1 no fa el COMMIT la connexió 2 no executa del tot les operacions.
El SELECT final és `7`, que és l'UPDATE que ha fet la connexió 2.

#### Exercici 12
Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
- Al fer la connexió 2 el seu UPDATE, s'ha quedat bloquejada fins que la connexió 1 no ha fet el `ROLLBACK TO a;`. La connexió 1 només donarà per bó el primer UPDATE de la transacció, la resta d'operacions les desfarà al fer el `ROLLBACK TO a;`. En el cas de la connexió 2 passa el mateix, només serà vàlida la primera operació UPDATE, la resta no es guardarà ja que fa un altre `ROLLBACK TO a;`. El SELECT final donarà com a resultat `6`. En el cas de que el id de la consulta hagués sigut `id=120`, el resultat hagués sigut `7`.
