--Alejandro Carreño Rubio
--iaw46487690
--myEpoch3.sql

--El mateix que el myEpoch2 simplificant el codi

drop function if exists myEpoch3(timestamp);

create or replace function myEpoch3(data timestamp) 
        returns bigint as $$
                declare segons bigint;
                begin
                        return myEpoch2(data);
                end;
$$ language plpgsql;

select myEpoch3('02-03-1970 00:00:00');
