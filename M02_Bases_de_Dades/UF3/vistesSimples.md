# VISTES SIMPLES
```
create view emp30
as select empno, ename, sal, deptno
from emp
where deptno=30;
```
```
select * from emp30;
 empno | ename  |   sal   | deptno
-------+--------+---------+--------
  7499 | ALLEN  | 1600.00 |     30
  7521 | WARD   | 1250.00 |     30
  7654 | MARTIN | 1250.00 |     30
  7698 | BLAKE  | 2850.00 |     30
  7844 | TURNER | 1500.00 |     30
  7900 | JAMES  |  950.00 |     30
(6 rows)

INSERT INTO emp30  
  VALUES (1234, 'YAS', 3000, 10);

UPDATE emp30 SET sal = 2500 WHERE empno = 1234;

Delete from emp30 where empno = 1234;
```
- Si es fa un insert al departament 10 no surtira en la vista pero si en la
taula emp perque en la vista nomes es veuen els departaments 30
- Si es fa un insert al departament 30 es veura tant a la vista com a la taula emp
- Si es fa un insert al deaprtament 50 surtira un missatge d'error dient que el
departament 50 no esta present en la taula dept

**On s'emmagatzema la informació?**  
A la taula

**Com puc veure la definició de la vista?**  
\sv vista o \sv+ vista o \d+ vista

**Com puc veure les vistes de l'usuari?**

## Definició Vistes Simples
Vistes simples es aquella que nomes esta feta sobre una taula y no conte  
ni funcions de grup, ni joins, ni camps calculats
