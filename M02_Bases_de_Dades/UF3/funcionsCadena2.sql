--Alejandro Carreño Rubio 
--iaw46487690

drop function if exists instr(cadenaOrigen varchar, patro varchar);
drop function if exists strcat(cadena1 varchar, cadena2 varchar);

--*instr
--Paràmetres formals: cadena origen, cadena patró a cercar
--Tasca: retornarà la posició on es troba el patró
--retorna: cadena

create or replace function instr(cadenaOrigen varchar, patro varchar)
returns varchar as $$
	begin
		return strpos(cadenaOrigen, patro);
	end;
$$ language plpgsql;

select instr('hello world','wo');
 instr 
-------
 7
(1 row)

--* strcat
--Paràmetres formals: cadena1, cadena2
--Tasca: retornarà la concatenació de les dues cadenes.
--Retorna: cadena

create or replace function strcat(cadena1 varchar, cadena2 varchar)
returns varchar as $$
	begin
		return cadena1 || cadena2;
	end;
$$ language plpgsql;

select strcat('hola', ' que tal');
    strcat    
--------------
 hola que tal
(1 row)







