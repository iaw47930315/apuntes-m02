--Alejandro Carreño Rubio
--iaw46487690

--Valida un dni. Recordeu que el DNI és una cadena alfanumèrica on hi 
--ha 8 dígits i una lletra, tal i com es descriu a sota.
drop function if exists dniCorrecte();

create or replace function dniCorrecte(dni varchar)
returns boolean
	as $$
		declare 
			lletra varchar(10);
			numero integer;
		begin
			lletra := upper(substr(dni,9,1));
			numero := substr(dni,1,8);
			return lletra = substr('TRWAGMYFPDXBNJZSQVHLCKE', mod(numero,23)+1, 1);
		end;
$$ language plpgsql;
 
select dniCorrecte('46487690K');

--psql fechas -f Desktop/M02_Bases_de_Dades/dniCorrecte.sql 
