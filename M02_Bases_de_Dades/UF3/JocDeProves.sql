--Alejandro Carreño Rubio
--iaw46487690
--JocDeProves.sql

--Crear una funcio que donat el id de empleat mostri el nom, el salari i el treball

drop function if exists empById();

create or replace function empById(p_empno int)
returns varchar
as $$
	declare
		v_ename varchar(100);
		v_sal numeric(7,2);
		v_job varchar(100);
	begin
		select ename, sal, job
		into strict v_ename, v_sal, v_job
		from emp
		where empno=p_empno;
		return (v_ename, v_sal, v_job);
		exception
			when NO_DATA_FOUND THEN
            return ('El empleado '|| p_empno ||' no existe');
	end; 
$$ language plpgsql;

select empById(7369);
select empById(73690);--No existeix l'empleat

--return (v_ename, v_sal, v_job);
--return ('El empleado '|| v_ename ||' con un salario de '|| v_sal ||' trabaja de '|| v_job);


--Crear una funcio que donat el id de empleat mostri tota l'informacio de l'empleat

drop function if exists allEmpById();

create or replace function allEmpById(p_empno int)
returns varchar
as $$
	declare
		v_empleat emp% rowtype;
	begin
		select *
		into strict v_empleat 
		from emp
		where empno=p_empno;
		--return v_empleat.ename || ' ' || v_empleat.sal || ' ' || v_empleat.job;
		return v_empleat;
		exception
			when NO_DATA_FOUND THEN
            return ('El empleado '|| p_empno ||' no existe');
	end; 
$$ language plpgsql;

select allEmpById(7369);
select allEmpById(73690);--No existeix l'empleat

--\df -> listar las funciones de la base de datos
--\sf function name -> veure el codi de la funcio

--shebang
--() { :; }; exec psql template1 -f "$0"
