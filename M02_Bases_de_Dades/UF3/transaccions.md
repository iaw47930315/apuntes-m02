-- --------------------------------------------
    Funcions de transaccions
-- --------------------------------------------

**1. Analitzant les següents sentències explica quins canvis es realitzen  
on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

INSERT INTO punts (id, valor) VALUES (10,5);  
BEGIN;  
UPDATE punts SET valor = 4 WHERE id = 10;  
ROLLBACK;  
SELECT valor FROM punts WHERE id = 10;

- Inserta un 10 al id i un 5 al valor, desprès cambia el valor a 4 on el id  
valgui 10. Torna enrere on el valor era 5 i el mostra.

**2. Analitzant les següents sentències explica quins canvis es realitzen i on es  
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

INSERT INTO punts (id, valor) VALUES (20,5);  
BEGIN;  
UPDATE punts SET valor = 4 WHERE id = 20;  
COMMIT;  
SELECT valor FROM punts WHERE id = 20;

- Inserta un 20 al id i un 5 al valor, desprès cambia el valor a 4 on el id  
valgui 20. Guarda, i mostra el valor nou que es el 4.

**3. Analitzant les següents sentències explica quins canvis es realitzen i on es  
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

INSERT INTO punts (id, valor) VALUES (30,5);  
BEGIN;  
UPDATE punts SET valor = 4 WHERE id = 30;  
SAVEPOINT a;  
INSERT INTO punts (id, valor) VALUES (31,7);  
ROLLBACK;  
SELECT valor FROM punts WHERE id = 30;

- Inserta un 30 al id i un 5 al valor, desprès cambia el valor a 4 on el id  
valgui 30. Es guarda amb un savepoint anomenat a i inserta un nova fila  
on l'id es 31 i el valor 7. Torna enrere i mostra el valor del id 30 que sera 5.

**4. Analitzant les següents sentències explica quins canvis es realitzen i on es  
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

DELETE FROM punts;  
INSERT INTO punts (id, valor) VALUES (40,5);  
BEGIN;  
UPDATE punts SET valor = 4 WHERE id = 40;  
SAVEPOINT a;  
INSERT INTO punts (id, valor) VALUES (41,7);  
ROLLBACK TO a;  
SELECT COUNT(*) FROM punts;  

- Esborra totes les files de la taula punts, inserta una fila on l'id es 40  
i el valor 5. Cambia el valor a 4 on l'id sigui 40. Es guarda amb un savepoint  
anomenat a i inserta un nova fila on l'id es 41 i el valor 7. Torna enrere  
al savepoint a i mostra cuantes files n'hi han, en aquest cas només 1.

**5. Analitzant les següents sentències explica quins canvis es realitzen i on es  
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

INSERT INTO punts (id, valor) VALUES (50,5);  
BEGIN;  
SELECT id, valor WHERE punts;  
UPDATE punts SET valor = 4 WHERE id = 50;  
COMMIT;  
SELECT valor FROM punts WHERE id = 50;  

- Falla l'operació del block, i per tant mostra el valor original que es 5.

**6. Analitzant les següents sentències explica quins canvis es realitzen i on es  
realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.**

DELETE FROM punts;  
INSERT INTO punts (id, valor) VALUES (60,5);  
BEGIN;  
UPDATE punts SET valor = 4 WHERE id = 60;  
SAVEPOINT a;  
INSERT INTO punts (id, valor) VALUES (61,8);  
SAVEPOINT b;  
INSERT INTO punts (id, valor) VALUES (61,9);  
ROLLBACK TO b;  
COMMIT;  
SELECT SUM(valor) FROM punts;

- Esborra totes les files de la taula punts, inserta una fila on l'id es 60  
i el valor 5. Cambia el valor a 4 on l'id sigui 60. Es guarda amb un savepoint  
anomenat a i inserta un nova fila on l'id es 61 i el valor 8. Es guarda amb  
un savepoint anomenat b i inserta un nova fila on l'id es 61 i el valor 9.  
Torna enrere al savepoint b guarda i mostra la suma de tots els valors que dona 12;

**7. Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

DELETE FROM punts; -- Connexió 0  
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1  
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2

- El resultat es 1 perque la connexió 1 encara no ha fet el commit i no  
ha esborrat l'insert

**8.- Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0  
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1  
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2  
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2  
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0

- Com que hi ha 2 transaccions a la vegada nomes es guarda la primera y  
la segona farà un ROLLBACK quan es faci el commit. El resultat sera 4.

**9.- Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1  
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2  
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2  
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0

- La connexió 2 no fa ROLLBACK perque en mig de la seva transacció no hi  
ha cap ordre de la connexió 1. El valor donarà 9.

**10.- Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1  
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2  
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2  
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0

- La transacció de la connexió 2 es queda penjada fins que la connexió 1  
fa el commit i despres es fa la transacció de la connexió 2. El resultat sera 7.

**11.- Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0  
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1  
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2  
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2  
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2  
SAVEPOINT a; -- Connexió 2  
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2  
ROLLBACK TO a; -- Connexió 2  
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0

- Les ordres de la connexió 2 es queden penjades fins que la connexió 1 fa  
commit, i despres s'executen totes. El valor del id 111 sera 7.

**12.- Analitzant les següents sentències explica quins canvis es realitzen  
i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
Tenint en compte que cada sentència s'executa en una connexió determinada.**

INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0  
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1  
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1  
SAVEPOINT a;  
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2  
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2  
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2  
ROLLBACK TO a; -- Connexió 2  
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0

- Fins que la connexió 1 no fa ROLLBACK to a no es fa el update de la connexió 2. al fer l'ultim rollback torna al ultim savepoint. El valor sera 6 del primer update de la connexió 1.
