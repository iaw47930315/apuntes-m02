--() { :; }; exec psql scott -f "$0"
--Alejandro Carreño Rubio
--iaw46487690
--funcionsBiblioteca.sql

\c biblioteca;
\x;

--Donat un titol em diu els codis de documents disponibles
--Ha de retornar el codi d'exemplar(idexemplar)
--NOTA: Si hi ha mes d'un cas retornará el primer que trobi
--Si no troba retorna un null, si troba retorna l'ultim idexemplar
drop function if exists documentDisponible();

create or replace function documentDisponible(p_titol varchar)
returns int
as $$
	declare
		v_exemplar int;
	begin
		select idexemplar
		into strict v_exemplar
		from exemplar e, document d
		where e.iddocument=d.iddocument and lower(titol)=lower(p_titol)
		except
		select idexemplar
		from prestec
		where datadev is null
		limit 1;
		return v_exemplar;
		exception
			when NO_DATA_FOUND THEN
            return null;
	end; 
$$ language plpgsql;

--Es pot posar limit 1, max() o min(), depenent el que posem sortira 2 o 3 que son
--els exemplars que estan disponibles
select documentDisponible('Fundamentos de bases de datos');

select documentDisponible('Highway to hell');
select documentDisponible('La La Land [enregistrament vídeo] : la ciudad de las estrellas / escrita y dirigida por Damien Chazelle');
--En vez de limit 1 puede ser max(idexemplar) o min(idexemplar)

--jocDeProves
select max(idexemplar)
from exemplar
where iddocument = (select iddocument
					from llibre
					where titol ='Fundamentos de bases de datos');

--Sortira tots els exemplars que te el document amb aquest titol			
select l.iddocument, idexemplar
from llibre l,exemplar e
where l.iddocument=e.iddocument and
	titol='Fundamentos de bases de datos';

--Sortiran els exemplars que estan actualment en prestec
select * 
from prestec
where idexemplar in (1,2,3);


/*Prèstec de documents
funció prestecDocument
Considerarem que un usuari podrà tenir simultàniament un màxim de 4 docs:
-2 docs del tipus llibre/revista 
-1 DVD 
-1 CD
D’altra banda, un exemplar d’un document apareixerà com a disponible si
actualment no està en prèstec i a més a més el seu estat és "Disponible".

Qualsevol altre estat (Exclòs de prèstec, En exposició, Reservat) farà que no
apareixi l’exemplar com apte per agafar en prèstec. Evidentment, si que es
podran consultar a la pròpia biblioteca.
Evidentment, un usuari podrà afagar documents en prèstec només sí no té el 
carnet blocat.*/

select idUsuari,
	sum(case when format='CD' then 1
			else 0 end) numCDs,
	sum(case when format in ('llibre','revista') then 1
			else 0 end) llibreRevista,
	sum(case when format='DVD' then 1
			else 0 end) numDVDs,
	count(case when format='DVD' then 1
			else 0 end) totalPrestecs
from exemplar e, document d, prestec p
where d.idDocument = e.idDocument and p.idExemplar = e.idExemplar
	and datadev is null
group by idUsuari;

if v_bloquejat then
	RAISE EXCEPTION 'Ho sentim el document esta bloquejat'
end if;

select format
into strict v_format
from document
where lower(titol)=lower(p_titol);
if v_format='CD' and v_CDs=1 then
	raise exception '';
else 
	if v_fromar='DVD' and v_DVDs=1 then
		raise exception '';
	else if 
	

drop function if exists prestecDocument();

create or replace function prestecDocument(p_titol varchar)
returns varchar
as $$
	declare
		v_exemplar int;
	begin
		select idexemplar
		into strict v_exemplar
		from exemplar e, document d, repartiment r
		where e.iddocument=d.iddocument and r.iddocument!=e.iddocument 
				and e.estat='Disponible' and lower(titol)=lower(p_titol)
		except
		select idexemplar
		from prestec
		where datadev is null
		limit 1;
		return 'El exemplar '||v_exemplar||' esta actualmente disponible';
		exception
			when NO_DATA_FOUND THEN
            return 'El exemplar '||v_exemplar||' no esta actualmente disponible';
	end; 
$$ language plpgsql;

select prestecDocument('Fundamentos de bases de datos');
select prestecDocument('La La Land');

/*
drop function if exists prestecDocument();

create or replace function prestecDocument(p_login varchar)
returns boolean
as $$
	declare
		v_bloquejat boolean;
		v_idexemplar int;
		v_contador int = 0;
	begin
		select bloquejat
		into strict v_bloquejat
		from usuari where login = p_login;
		if v_bloquejat
		--el false seria: if not bloquejat
		then
			return false;
			RAISE EXCEPTION 'Ho sentim el document esta bloquejat'
		else
			idexemplar := documentDisponible(p_titol);
			if idexemplar = 0 then
				RAISE EXCEPTION 'Ho sentim pero el document no esta disponible'
			return false;
			if idexemplar > 0 and v_contador < 2 then
				RAISE EXCEPTION 'Disponible'
			return true;
	end;
$$ language plpgsql;

*/

/*Per al bon funcionament del servei de préstec et recordem que has de ser puntual
a l’hora de retornar els documents. Per cada dia de retard i document en préstec,
el teu carnet rebrà 1 punt de penalització. Per cada 50 punts, el carnet quedarà
bloquejat durant 15 dies. Passat aquest temps, el carnet tornarà a estar operatiu.
Quan un usuari superi els 50 punts de demèrit, és reiniciarà el comptador de
punts.
Hi ha llibres que no es presten (són només de consulta a la biblioteca).
Si l’exemplar que es retorna està reservat, NO ha d’aparèixer com a disponible
pel públic en general.*/

p_idExemplar
v_prestec prestec%rowtype
...
select *
into strict v_prestec
from prestec
where p_idExemplar = idExemplar
	and datadev is null;

 
drop function if exists retornarDocument();

create or replace function retornarDocument(p_titol varchar, p_idUsuari)
returns boolean
as $$
	declare
		v_punts int = 0;
		v_bloquejat boolean;
		v_data date;
	begin
		select bloquejat
		into strict v_bloquejat
		from usuari where login = p_login;
		select data
		into strict v_data
		from penalitzacio p, usuari u 
		where p.idusuari=u.idusuari;
		if v_bloquejat
		then
			return false;
			RAISE EXCEPTION 'Has arribat a 50 punts de penalitzacio i el 
								teu carnet quedara bloquejat 15 dies';
		else
			v_punts = v_punts + (current_date-v_data);
			if v_punts >= 50
			then
				return false;
				RAISE EXCEPTION 'Has arribat a 50 punts de penalitzacio i el 
									teu carnet quedara bloquejat 15 dies';
			end if;
		return true;
		end if;
	end;
$$ language plpgsql;


/*Es poden renovar fins a tres vegades, la qual cosa es pot fer en persona, telèfon
o internet.
Els documents es poden renovar sempre i quan:
no s’hagin renovat ja tres vegades
no hagi expirat la data de retorn
no es tingui el carnet de préstec bloquejat per algun motiu
no estiguin reservats per un altre usuari
La renovació és de 30 dies naturals sobre la data de prèstec.*/

drop function if exists renovarDocument();

create or replace function renovarDocument(p_login varchar)
returns varchar
as $$
	declare
		v_renovacio int;
	begin
		if not retornarDocument(p_login) then
			raise exception 'No es pot renovar el carnet';
		else
			if v_renovacio == 3 then
			raise exception 'Carnet renovat';
			v_renovacio = v_renovacio + 1;
			end if;
		end if;
	end;
$$ language plpgsql;		








