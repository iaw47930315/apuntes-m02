--() { :; }; exec psql biblioteca -f "$0"

create or replace function pelis()
returns varchar
as $$

declare
 c_peli cursor for
    select e.idExemplar, e.idDocument, titol, Nom ||' '||COGNOMS Nom
    from prestec p, exemplar e, usuari u, document d
    where p.idExemplar=e.idExemplar and p.idUsuari=u.idUsuari and
          e.idDocument=d.idDocument;
  -- %rowtype no funciona per a cursors, a diferència d'Oracle
  -- v_peli c_peli%rowtype;
  v_peli record; --declaro la variable de tipus record per al cas del LOOP i del WHILE
begin

    -- amb FOR
  RAISE NOTICE E'\n\nFOR\n-----\n';
  for v_peli in c_peli
  loop
       raise notice 'Document % exemplar % titol %', v_peli.idDocument,v_peli.idexemplar,v_peli.titol;
  --procesar cada una de las filas
  end loop;

  -- amb LOOP
 RAISE NOTICE E'\n\nLOOP\n-----\n';
 OPEN c_peli;
 LOOP
      FETCH c_peli INTO v_peli;
   EXIT WHEN NOT FOUND;
      raise notice E'Document % exemplar % titol %', v_peli.idDocument,v_peli.idexemplar,v_peli.titol;
 END LOOP;
 close c_peli;

 -- amb WHILE
 RAISE NOTICE E'\n\nWHILE\n-----\n';
 OPEN c_peli;
 FETCH c_peli INTO v_peli;
 WHILE FOUND
 LOOP
      RAISE NOTICE E'Document % Exemplar: % Titol:%', v_peli.idDocument,v_peli.idexemplar,v_peli.titol;
      FETCH c_peli INTO v_peli;
 END LOOP;
 close c_peli;
 return 'Fi de cursor';
end;
$$ language plpgsql;


select pelis();
